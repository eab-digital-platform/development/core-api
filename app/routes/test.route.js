import express from 'express';

import testController from '../controllers/test.controller';

const router = express.Router();

router.get('/', (req, res) => {
  testController.test1(req, res);
});

router.get('/test2', (req, res) => {
  testController.test2(req, res);
});

export default router;
