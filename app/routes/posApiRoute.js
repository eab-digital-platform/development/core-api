var express = require('express');
var router = express.Router();
var $posapiController = require('../controller/posApiController');
var $searchEngine = require('../controller/searchEngineController');
var $FNAController = require('../controller/FNAController');
var $quoteController = require('../controller/quoteContoller');
var $eAppController = require('../controller/eAppController');

//common
router.delete('/delete', $searchEngine.api.delete);
router.get('/start_signature/:docType/:docId',$eAppController.api.startSignature);
// router.get('/start_signature/:docType/:docId',$eAppController.api.startSignature);
router.get('/get_single_doc_view/:docType/:docId',$eAppController.api.getPDFForPreview);
router.post('/save_signed_eform/:docType/:docId/:eSignSessionID',$eAppController.api.saveSignedForm);
router.get('/getSignedPDFForPreview/:docType/:docId',$eAppController.api.getSignedPDFForPreview);
router.post('/uploadDocument',$eAppController.api.uploadDocument);
router.get('/getPicForPreview/:fileName',$eAppController.api.getPicForPreview);
router.post('/removeDocument/:eAppId/:docType/:docOwnerId/:fileName',$eAppController.api.deleteDocument);



//familyMember
router.get('/getAvailableFamilyRoleList', $posapiController.api.getAvailableFamilyRoleList);
router.get('/clientSearchExcludeFamilyMember', $posapiController.api.clientSearchExcludeFamilyMember);

router.get('/getDoc/:docId', $searchEngine.api.getDoc);
// router.post('/createFNA', $FNAController.api.createFNA);
router.get('/relationalSearch', $searchEngine.api.relationalSearch);
router.post('/createClientFna', $searchEngine.api.relationCreate);

//FNA
router.get('/getFnaForm/:docId', $FNAController.api.getFNAForm);
router.get('/getFnaListByClient/:clientId', $FNAController.api.getFNAListByClient);
router.get('/getLinkableFNAListByClient/:clientId/:quoteId', $FNAController.api.getLinkableFNAListByClient);
router.get('/getFnaCafeFormWithClientData/:clientId/:formId', $FNAController.api.getFnaCafeFormWithClientData);
router.post('/createFnaQuote/:fnaFormId/:quoteId', $FNAController.api.createFnaQuote);
router.post('/deleteFnaQuote/:fnaFormId/:quoteId', $FNAController.api.deleteFnaQuote);
router.post('/fnaValidationAgainstQuote/:fnaFormId/:quoteId', $FNAController.api.fnaValidationAgainstQuote);
router.post('/fnaValidationAgainstQuotes/:fnaFormId', $FNAController.api.fnaValidationAgainstQuotes);
router.post('/saveSignedFna/:fnaFormId', $FNAController.api.checkFnaSignedComplete);
router.post('/checkFnaExpired/:fnaFormId', $FNAController.api.checkFnaExpired);
router.post('/generateFnaPdf/:docId', $FNAController.api.generateFnaPdf);
router.get('/getFnaPdf/:docId', $FNAController.api.getFnaPdf);
router.get('/getWholeFamily/:docId', $FNAController.api.getFnaPdf);
router.get('/goToQuoteView/:quoteId', $FNAController.api.goToQuoteView);
router.post('/createDummyQuote/:fnaFormId/:clientId/:basicPlan', $quoteController.api.cloneQuote,$FNAController.api.createFnaQuote,$quoteController.api.createClientQuote);
router.post('/createDummyQuote/:fnaFormId/:quoteId', $quoteController.api.cloneQuote,$FNAController.api.createFnaQuote);
//quotation
router.get('/getAvailableQuoteListForFNA/:clientId/:fnaFormId', $quoteController.api.getAvailableQuoteListForFNA);
router.get('/getQuoteListByFnaGroupByFamilyMember/:clientId/:fnaFormId', $quoteController.api.getQuoteListByFnaGroupByFamilyMember);
router.get('/getQuoteListByClient/:clientId', $quoteController.api.getQuoteListByClient);
router.post('/createQuoteEapp/:quoteId/:eAppId', $quoteController.api.createQuoteEapp);
router.post('/deleteQuoteEapp/:quoteId/:eAppId', $quoteController.api.deleteQuoteEapp);
router.post('/updateQuoteStatus/:quoteId', $quoteController.api.updateQuoteStatus);
router.post('/markDeleteQuote/:quoteId', $quoteController.api.markDeleteQuote);
router.get('/getAvailableProductList/:clientId/:fnaFormId', $quoteController.api.getAvailableProductList);
router.post('/cloneQuote/:quoteId', $quoteController.api.cloneQuote);
router.post('/invalidateQuotation/:quoteId', $quoteController.api.invalidateQuotation);
router.post('/createClientQuote/:clientId/:quoteId', $quoteController.api.createClientQuote);
router.post('/createQuoteDummy/:clientId/:basicPlan', $quoteController.api.cloneQuote,$quoteController.api.createClientQuote);
//eApp
router.post('/createEapp/:clientId/:quoteId',$eAppController.api.createEapp);
router.post('/createClientEapp/:cpId/:eAppId', $eAppController.api.createClientEapp);
router.post('/deleteClientEapp/:cpId/:eAppId', $eAppController.api.deleteClientEapp);
router.get('/getEappListByClient/:clientId', $eAppController.api.getEappListByClient);
router.get('/get_doc_list/:eAppId', $eAppController.api.get_doc_list);
router.post('/updateEappSignStatus/:docId/:signatureType',$eAppController.api.updateEappSignStatus);
router.post('/submitEapp/:docId/',$eAppController.api.submitEapp);
router.get('/get_doc_list_for_upload/:eAppId', $eAppController.api.get_doc_list_for_upload);
router.get('/getUploadedDocument/:eAppId/:docType/:docOwnerId', $eAppController.api.getUploadedDocument);
router.get('/getAvailableQuoteListForEapp/:clientId', $eAppController.api.getAvailableQuoteListForEapp);
router.post('/markDeleteEapp/:eAppId', $eAppController.api.markDeleteEapp);


module.exports = router;