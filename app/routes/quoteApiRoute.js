var express = require('express');
var router = express.Router();
var $quoteContoller = require('../controller/quoteContoller');

router.post('/fnaValidationAgainstQuotes/:fnaFormId', $quoteContoller.api.fnaValidationAgainstQuotes);

module.exports = router;