import express from 'express';

import indexController from '../controllers/index.controller';

const router = express.Router();

router.get('/', (req, res) => {
  indexController.homePage(req, res);
});

router.get('/test', (req, res) => {
  indexController.homePage2(req, res);
});

export default router;
