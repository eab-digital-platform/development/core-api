// const $quotationController = require('../controller/QuotationContoler');

import $quotationController from '../controller/QuotationContoler';

const express = require('express');

const router = express.Router();
// const $userProfile = require('../controller/userProfileControler');
const $clientProfile = require('../controller/clientProfileControler');
const $searchEngine = require('../controller/searchEngineController');
const $powerSearchController = require('../controller/powerSearchController');

// router.get('/userProfile/:userId',$CheckApiToken, $userProfile.api.loadUserProfile);
// router.get('/getCP/:clientId', $CheckApiToken,$clientProfile.api.getCP);
// router.post('/createCP',$CheckApiToken, $clientProfile.api.createCP);

router.get('/userProfile/:userId', $searchEngine.api.userProfile);
// Client Profile
router.get('/getCp/:clientId', $clientProfile.api.getCP);
router.delete('/deleteCp/:clientId', $clientProfile.api.deleteCP);
router.post('/createCP', $clientProfile.api.createCP);
router.get('/textSearch', $clientProfile.api.textSearch);


router.get('/getDoc/:docId', $searchEngine.api.getDoc);
router.get('/search', $searchEngine.api.search);
router.delete('/delete', $searchEngine.api.delete);
// router.delete('/delete', $searchEngine.api.delete);
router.post('/create', $searchEngine.api.create);
router.post('/update', $searchEngine.api.update);


router.get('/relationalSearch', $searchEngine.api.relationalSearch);
router.post('/createUpdateRelation', $searchEngine.api.relationCreate);
// router.get(
//   '/clientSearchExcludeFamilyMember',
//   $searchEngine.api.clientSearchExcludeFamilyMember,
// );
router.post('/relationDelete', $searchEngine.api.relationDelete);

router.get('/initSearchCriteria', $powerSearchController.api.initSearchCriteria);
router.get('/initSearchBar', $powerSearchController.api.initSearchBar);
router.post('/powerSearchResult', $powerSearchController.api.powerSearchResult);

router.post('/sendInfo2Quotation', $quotationController.api.sendInfo2Quotation);
module.exports = router;
