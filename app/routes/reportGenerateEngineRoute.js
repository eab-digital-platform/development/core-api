var express = require('express');
var router = express.Router();
var $reportGenerateEngineController = require('../controller/reportGenerateEngineController');
router.get('/generatePDF', $reportGenerateEngineController.api.generatePDF);
module.exports = router;