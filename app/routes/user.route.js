import express from 'express';
import {
  celebrate,
  Joi
} from 'celebrate';
const router = express.Router();
import userController from '../controllers/user.controller';


router.post('/create',
  celebrate({
    body: Joi.object({
      name: Joi.string().required(),
      email: Joi.string().required(),
      password: Joi.string().required(),
    }),
  }),
  async (req, res, next) => {
    const userDTO = req.body;
    // Call to service layer.
    const result = await userController.create(userDTO);
    
    // Return a response to client.
    return res.status(200).send(result);
  }
);

export default router;