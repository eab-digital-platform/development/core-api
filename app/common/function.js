import logger from '../loaders/logger';
const _ = require('lodash');
import systemConfig from '../../config/systemConfig';
const searchAPI = require('../controller/searchEngineController');
const ModelSysParameter = require('../models/ModelSysParameter');
const getSystemParameter = async (companyCode)=>{
    let queryString = {"docType":"sysParameter","EAB.companyCode":companyCode};
    let rtnValue = await searchAPI.fnSearch(queryString,"get");
    return rtnValue;
};
  export {
    getSystemParameter
  };