import config from '../../config/systemConfig';
import logger from '../loaders/logger';
const utils = require('../utils/utils');
const _ = require('lodash');
const MQHelper = require("./rabbitmq.util");
const uuid = require("uuid/v1");
const searchEngineController = require("../controller/searchEngineController");



async function onReceivedMessageCreate(message) {
logger.info("MQ | Received Message");
   
   /* ??? */
   if (message.properties.replyTo) {
       let data = JSON.parse(message.content);
       let mappingSetting = await utils.getDocType(_.get(data.data,"formID",""));
       for(let it in mappingSetting){
            data = _.set(data,it,mappingSetting[it]);
       }
       let docType = _.get(mappingSetting,"mappingModel","");
    //    logger.info("data = ",data);
       let result = await searchEngineController.fnCreate(docType,data).catch(error=>{
            result = {"success":false,message:""+error};
        });
        // logger.debug("result = ",result);
        if(_.has(result,"success")){
            if(!result.success){
                result = {"success":false,message:result};
            }
        }
       this.sendAck({ replyTo: message.properties.replyTo, acknowledgement: result, message})
   }
}
async function onReceivedMessageUpdate(message) {
    logger.info("MQ onReceivedMessageUpdate");
    /* ??? */
    if (message.properties.replyTo) {
        let data = JSON.parse(message.content);
        let mappingSetting = await utils.getDocType(_.get(data.data,"formID",""));
        for(let it in mappingSetting){
             data = _.set(data,it,mappingSetting[it]);
        }
        let docType = _.get(mappingSetting,"mappingModel","");
        let result = await searchEngineController._fnUpdate(docType,data.updateCriteria,data);
        // logger.info("onReceivedMessageUpdate result = ",result);
        if(!result["success"]){
            result = {"success":false,message:result};
        }
        this.sendAck({ replyTo: message.properties.replyTo, acknowledgement: result, message})
    }
 }

const trial = async () => {
    const rabbitCreate = new MQHelper({
        queue: config.rabbitMQchannelCreate, 
        option: MQHelper.getQueueOption({ durable: false }),
        onReceivedMessage: onReceivedMessageCreate,
        role: MQHelper.ROLE.CONSUMER
    });

    const rabbitUpdate = new MQHelper({
        queue: config.rabbitMQchannelUpdate, 
        option: MQHelper.getQueueOption({ durable: false }),
        onReceivedMessage: onReceivedMessageUpdate,
        role: MQHelper.ROLE.CONSUMER
    });

    // console.log(rabbit);
    // await new Promise((resolve) => {
    //     rabbit.sendMessage({
    //         message: "456",
    //         onCalslback: (message) => {
    //             console.log("MQ | SendQueue | callback ", message.content.toString());
    //             resolve(message);
    //         },
    //         queueOption: { correlationId: uuid() },
    //         consumeOption: { noAck: true }
    //     })
    // })
    // rabbit.closeConnection();
    // process.exit(0);
};
trial();