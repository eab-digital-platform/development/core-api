import mongoose from 'mongoose';
import config from '../../config/systemConfig';

const userName = config.databaseUsername;
const pwd = config.databasePassword;
const url = config.databaseURL;
const { databaseName } = config;

export default async () => {
  const DB_URL = `mongodb://${userName}:${pwd}@${url}/${databaseName}`;
  const connection = await mongoose.connect(DB_URL, {
    useNewUrlParser: true,
    useCreateIndex: true,
    useUnifiedTopology: true,
  });
  return connection.connection.db;
};
