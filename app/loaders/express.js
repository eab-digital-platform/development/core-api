import express from 'express';
import cookieParser from 'cookie-parser';
// import bodyParser from 'body-parser';
import morgan from 'morgan';
import cors from 'cors';
import helmet from 'helmet';

const {
  errors,
} = require('celebrate');
const apiRouter = require('../routes/route');
const BRERoute = require('../routes/BRERoute');
const posApiRoute = require('../routes/posApiRoute');
const reportGenerateEngineRoute = require('../routes/reportGenerateEngineRoute');

// rabbitMQ
// const mqListener = require('../rabbitmq/receiver');

export default async (app) => {
  // Init Helmet security lib
  // Included features by default:
  // 1. dnsPrefetchControl(controls browser DNS prefetching),
  // 2. frameguard(prevent clickjacking),
  // 3. hidePoweredBy (remove the X-Powered-By header),
  // 4. hsts (HTTP Strict Transport Security),
  // 5. ieNoOpen (sets X-Download-Options for IE8+)
  // 6. noSniff (keep clients from sniffing the MIME type)
  // 7. xssFilter(adds some small XSS protections)
  // ref to: https://helmetjs.github.io/docs/

  // 20200320
  // app.use(bodyParser.json());
  // app.use(bodyParser.urlencoded({ extended: false }));

  app.use(cors());
  app.use(helmet());
  app.use(helmet.noCache());

  app.use(morgan('dev'));
  app.use(express.json());
  app.use(express.urlencoded({
    extended: false,
  }));
  app.use(cookieParser());
  // POS
  app.use('/pos-api', posApiRoute);
  // core-api
  app.use('/core-api', apiRouter);
  // BRE
  app.use('/bre-api', reportGenerateEngineRoute);

  // rabbitMQ
  // app.use(mqListener());

  // report engine
  app.use('/reportGenerateEngine', BRERoute);

  app.use(errors());

  // / catch 404 and forward to error handler
  app.use((req, res, next) => {
    const err = new Error('Not Found');
    err.status = 404;
    next(err);
  });

  // / error handlers
  app.use((err, req, res, next) => {
    /**
         * Handle 401 thrown by express-jwt library
         */
    if (err.name === 'UnauthorizedError') {
      return res
        .status(err.status)
        .send({
          message: err.message,
        })
        .end();
    }
    return next(err);
  });
  app.use((err, req, res) => {
    res.status(err.status || 500);
    res.json({
      errors: {
        message: err.message,
      },
    });
  });
};
