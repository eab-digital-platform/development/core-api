import logger from '../../loaders/logger';
const _ = require('lodash');
import fnaValueMapping from './fnaValueMapping';
import systemConfig from '../../../config/systemConfig';
const searchAPI = require('../../controller/searchEngineController');
const ModelFNA = require('../../models/ModelFNA');
const dbHelper = require('../../utils/dbHelper');
const commonFunction = require('../../common/function');
var moment = require("moment");
const getMappingValue = (path,val)=>{
    let rtnValue = {};
    let hasPath = _.has(fnaValueMapping,path);
    // logger.debug("fnaValueMapping = ",fnaValueMapping);
    // logger.debug("path = ",path);
    // logger.debug("hasPath = ",hasPath);
    if(hasPath){
        let item = _.get(fnaValueMapping,path);
        let valueType = _.get(item,"valueType");
        let multiple = _.get(item,"multiple");        
        if(!multiple && _.isArray(val)){
            val = _.head(val);
        }
        rtnValue = _.set(rtnValue,"valueType",valueType);
        if(_.eq(valueType,"directValue")){

        }else if(_.eq(valueType,"mappingValue")){
            let valueMapping = _.get(item,"valueMapping");
            let c = _.set({},"key",val);
            let valItem = _.find(valueMapping,c);
            // logger.debug("valueMapping = ",valueMapping);
            // logger.debug("valItem = ",valItem);
            for(let i in valItem){
                rtnValue = _.set(rtnValue,i,valItem[i]);
            }
            // rtnValue = _.set(rtnValue,"valueType",valItem["value"]);            
        }else if(_.eq(valueType,"mappingRange")){
            let valueMapping = _.get(item,"rangeMapping");
            let c = _.set({},"key",val);
            let valItem = _.find(valueMapping,c);
            // logger.debug("valueMapping = ",valueMapping);
            // logger.debug("valItem = ",valItem);
            for(let i in valItem){
                rtnValue = _.set(rtnValue,i,valItem[i]);
            }
        }
        // let valueSpec = _.get(fnaValueMapping,path)["valueSpec"];
    }
    // logger.debug("rtnValue = ",rtnValue);
    return rtnValue;
};
//净收入
const netIncome =(body)=>{
  let vNetIncome = 0.0;
  let avgMonthExp = averageMonthlyExpenses(body);
  let monthIncome = monthlyIncome(body);
  vNetIncome = _.subtract(monthIncome,avgMonthExp);
  return vNetIncome;
}
//平均月花费
const averageMonthlyExpenses =(body)=>{
  let Q4 = _.get(body,"CAFE.FNA.FI.Q4");
  return Q4;
}
//可支配收入百分比
const percentageOfDisposableIncome =(body)=>{
  let Q1c1 =  _.get(body,"CAFE.FNA.FI.Q1C1");
  Q1c1 = getMappingValue("FNA.FI.Q1C1",Q1c1);
  Q1c1 = _.get(Q1c1,"valueTo");
  return Q1c1;
}
//流动资产百分比
const percentageOfLiquidAssets =(body)=>{
  let Q1c2 =  _.get(body,"CAFE.FNA.FI.Q1C2");
  Q1c2 = getMappingValue("FNA.FI.Q1C2",Q1c2);
  Q1c2 = _.get(Q1c2,"valueTo");
  return Q1c2;
}
//月收入
const monthlyIncome =(body)=>{
  let Q1a = 0.0;//monthly income
  let Q1A1 = _.get(body,"CAFE.FNA.FI.Q1A1");
  if(_.isArray(monthlyIncome)){
    monthlyIncome = _.head(monthlyIncome);
  }
  if(Q1A1){
    Q1a = Q1A1;
  }else{
    Q1a = getMappingValue("FNA.FI.Q1A2",_.get(body,"CAFE.FNA.FI.Q1A2"));
    Q1a = _.get(Q1a,"valueTo");
  }
  return Q1a;
}

const fnaAnnualizePremium =(body)=>{
  let disIncome = percentageOfDisposableIncome(body);
  let vNetIncome = netIncome(body);
  let annPremium =_.round(_.multiply(_.multiply(vNetIncome,12),_.divide(disIncome,100)));
  return annPremium;
}
//缴付年期
const paymentTerm =(body)=>{  
  let payTerm =  _.get(body,"CAFE.FNA.CP.Q4");
  payTerm = getMappingValue("FNA.CP.Q4",payTerm);
  payTerm = _.get(payTerm,"value");
  return payTerm;
}
//最大保额
const maximumSumInsured =(body)=>{  
  let maxSumIns = _.get(body,"CAFE.FNA.FI.Q3");
  logger.debug("maxSumIns = ",maxSumIns)
  maxSumIns = getMappingValue("FNA.FI.Q3",maxSumIns);
  logger.debug("maxSumIns = ",maxSumIns)
  maxSumIns = _.get(maxSumIns,"valueTo");
  logger.debug("maxSumIns = ",maxSumIns)
  maxSumIns = _.round(_.multiply(maxSumIns,1.2));
  logger.debug("maxSumIns = ",maxSumIns)
  return maxSumIns;
}
//最小保额
const minimumSumInsured =(body)=>{  
  let minSumIns = _.get(body,"CAFE.FNA.FI.Q3");
  minSumIns = getMappingValue("FNA.FI.Q3",minSumIns);
  minSumIns = _.get(minSumIns,"valueFr");
  minSumIns = _.round(_.multiply(minSumIns,0.5));
  return minSumIns;
}
//缴付频率
const paymentFrequency =(paymentMode)=>{
  let payFrequency = 1;
  if(_.eq(paymentMode,"M")){
    payFrequency = 12;
  }else if(_.eq(paymentMode,"Q")){
    payFrequency = 4;
  }else if(_.eq(paymentMode,"S")){
    payFrequency = 2;
  }else if(_.eq(paymentMode,"A")){
    payFrequency = 1;
  }
  return payFrequency;
}
//流动资产
const liquidAssets =(body)=>{  
  let nLiquidAssets =  _.get(body,"CAFE.FNA.FI.Q1B2");
  if(_.isArray(nLiquidAssets)){
    nLiquidAssets = _.head(nLiquidAssets);
  }
  return nLiquidAssets;
}
//长期负债
const longTermLiabilities =(body)=>{  
  let nLongTermLiabilities =  _.get(body,"CAFE.FNA.FI.Q5A");
  return nLongTermLiabilities;
}
//短期负债
const shortTermLiabilities =(body)=>{  
  let nShortTermLiabilities =  _.get(body,"CAFE.FNA.FI.Q5B");
  return nShortTermLiabilities;
}
//负债
const liabilities =(body)=>{
  let vLongTermLiabilities = longTermLiabilities(body);
  let vShortTermLiabilities = shortTermLiabilities(body);
  return _.add(vLongTermLiabilities,vShortTermLiabilities);
}


 

const calculationAfterCompleteFNA =(body)=>{
    // logger.debug("calAnnualizedPremium fnaFunction = ",body);
    let isFNACompleted = _.get(body,"CAFE.isCompleted",false);
    if(isFNACompleted){
      let maxAnnPremium = 0.0;
      let payTerm = paymentTerm(body);
      let maxSumIns = maximumSumInsured(body);
      let minSumIns = minimumSumInsured(body);      
      maxAnnPremium = fnaAnnualizePremium(body);
      logger.debug("maxSumIns  = ",maxSumIns);
      logger.debug("minSumIns  = ",minSumIns);
      logger.debug("payTerm  = ",payTerm);
      logger.debug("maxAnnPremium  = ",maxAnnPremium);
      var updString = _.set(body,"EAB.annualizePremium",maxAnnPremium);
      updString = _.set(updString,"EAB.payTerm",payTerm);
      updString = _.set(updString,"EAB.maxSumIns",maxSumIns);
      updString = _.set(updString,"EAB.minSumIns",minSumIns);
      updString = _.set(updString,"EAB.status","C");
      return updString;
    }
  };
  const toCafeParam = (data,cafeFormID)=>{
    let prefix = _.get(fnaValueMapping,"formPersonalParticularsPrefix");
    let Cafe = _.get(data,"CAFE");
    let rtnValue = {"publishFormId":cafeFormID};
    let items = [];
    let toCafeFormFieldSetting = _.get(fnaValueMapping,"toCafeFormFieldSetting");
    for(let item in Cafe){
      let referenceId = item;
      if(prefix && !_.eq(item,"responseID") && !_.eq(item,"formID")){
        referenceId = _.join([prefix,referenceId],".");
      }
      let toCafeFormField = _.find(toCafeFormFieldSetting,{"fieldName":item});
      let toCafeFormFieldType = _.get(toCafeFormField,"questionType");
      if(_.eq(toCafeFormFieldType,"DROP_DOWN")){
        items = _.concat(items,_.set(_.set({},"referenceId",referenceId),"value",[{"referenceId":Cafe[item]}]));
      }else if(_.eq(toCafeFormFieldType,"DATE")){
        items = _.concat(items,_.set(_.set({},"referenceId",referenceId),"value",Cafe[item]));
      }else{
        items = _.concat(items,_.set(_.set({},"referenceId",referenceId),"value",Cafe[item]));
      }
    }
    items = _.concat(items,_.set(_.set({},"referenceId","clientID"),"value",_.get(data,"_id")));
    rtnValue = _.set(rtnValue,"importData",items);
    rtnValue = _.set(rtnValue,"mqParameter",_.set({},"channel",systemConfig["rabbitMQchannelCreate"]));
    // logger.debug("rtnValue = ",rtnValue);
    return rtnValue;
};
const suitabilityCheck = async (quote,fna)=>{
  logger.debug("suitabilityCheck"); 
  let premTermPass = false;
  let siPass = false;
  try{
      let plans = _.get(quote,"plans");
      let basicPlan = _.find(plans,{"planInd":"B"});
      let {premTerm,sumInsured} = basicPlan;
      let fnaPremTerm = paymentTerm(fna);
      logger.debug("fnaPremTerm = ",fnaPremTerm);
      fnaPremTerm = fnaPremTerm.replace("years","").replace(" ","");
      fnaPremTerm = _.split(fnaPremTerm,"-")[1];
      let fnaSumInsured = minimumSumInsured(fna);
      logger.debug("fnaSumInsured = ",fnaSumInsured);
      if(_.toNumber(fnaPremTerm)>=_.toNumber(premTerm)){
          premTermPass = true;
      }
      if(sumInsured >= fnaSumInsured){
          siPass = true;
      }
      return premTermPass && siPass;
  }catch(err){
    throw new Error(err.message)}
  };
  const affordabilityCheck = async (quote,fna)=>{
    logger.debug("affordabilityCheck");
    
    try{
        let plans = _.get(quote,"plans");
        let basicPlan = _.find(plans,{"planInd":"B"});
        let modalPremium = _.get(basicPlan,"premium");
        let payTerms = _.get(basicPlan,"premTerm");
        let {paymentMode} = quote;
        let payFrequency = paymentFrequency(paymentMode);
        let quoteAnnualizePremium = _.multiply(modalPremium,payFrequency);
        let vNetIncome = netIncome(fna);
        let vPercentageOfDisposableIncome = _.divide(quoteAnnualizePremium,_.multiply(vNetIncome,12));
        vPercentageOfDisposableIncome = _.round(_.multiply(vPercentageOfDisposableIncome,100));
        let vDisposableIncome = percentageOfDisposableIncome(fna);
        logger.debug("modalPremium = ",modalPremium);
        logger.debug("quoteAnnualizePremium = ",quoteAnnualizePremium);
        logger.debug("vNetIncome = ",vNetIncome);
        logger.debug("vDisposableIncome = ",vDisposableIncome);
        logger.debug("vPercentageOfDisposableIncome = ",vPercentageOfDisposableIncome);
        if(vPercentageOfDisposableIncome<=vDisposableIncome){
          return true;
        }
        logger.debug("false");
        let totalPremiumPay = _.multiply(modalPremium,payTerms);
        let vLiquidAssets = liquidAssets(fna);
        let vLiabilities = liabilities(fna);
        let vNetAssets = _.subtract(vLiquidAssets,vLiabilities);
        let calPercentageOfLiquidAssets = _.divide(totalPremiumPay,vNetAssets);
        calPercentageOfLiquidAssets = _.round(_.multiply(calPercentageOfLiquidAssets,100));
        let vPercentageOfLiquidAssets = percentageOfLiquidAssets(fna);
        logger.debug("totalPremiumPay = ",totalPremiumPay);
        logger.debug("vLiquidAssets = ",vLiquidAssets);
        logger.debug("vLiabilities = ",vLiabilities);
        logger.debug("vNetAssets = ",vNetAssets);
        logger.debug("calPercentageOfLiquidAssets = ",calPercentageOfLiquidAssets);
        logger.debug("vPercentageOfLiquidAssets = ",vPercentageOfLiquidAssets);        
        if(calPercentageOfLiquidAssets<=vPercentageOfLiquidAssets){
           return true;
        }
        logger.debug("false");
        return false;
    }catch(err){
      throw new Error(err.message)}
    };
    const suitAffordCheckAndUpdateMapping = async function(quote, fna){
      let resResult = {};
      let errorMessage = {};
      if(quote && fna){
          let checkSuit = await suitabilityCheck(quote,fna).catch(error=>{
            errorMessage = {"message":error.message};
          });    
          let checkAfford = await affordabilityCheck(quote,fna).catch(error=>{
            errorMessage = {"message":error.message};
          });
          let canProceed = (checkSuit&&checkAfford)?true:false;
          let mappingParameter = {"relationType":"fnaQuoteMapping"};
          let relationShip = {"oppositID":_.toString(_.get(quote,"_id")),"checkSuit":checkSuit,"checkAfford":checkAfford};
          let data = {"currentID":fna._id};
          data["relationShip"]=relationShip;
          mappingParameter["data"]=data;    
          await searchAPI.fnRelationCreate("posFNAMst",mappingParameter).then((result)=>{
            if(result["success"]){
              resResult = result.result;
            }else{
              errorMessage = {"message":error.message};
          }}).catch(error=>{errorMessage = error.message});
      }else{
        errorMessage = {"message":"quote or fna data missing!"};
      }
      if(!_.isEmpty(errorMessage)){
        return {"success":false,"message":errorMessage.message}
      }else{
        return {"success":true,"result":resResult}
      }
    
    };
const signedFNA = async function(fnaFormId){
  let signedStatus = false;
  if(fnaFormId){
    let updateStr = {"EAB.status":"S",updateDate:_.now()};
    await dbHelper.dbHelpFindOneAndUpdate(ModelFNA,{"_id":fnaFormId},updateStr).catch(error=>{
        signedStatus = false;
    });
    signedStatus = true;
  }
  return signedStatus;
};
const saveFnaSignedForm = async function(fnaFormId){
  let saved = true;
  //
  return saved;
};
const fnaExpired = async function(fnaFormId,compCode){
  let vContinue = false;
  if(fnaFormId){
    let rtresult = await commonFunction.getSystemParameter(compCode);
    vContinue = _.get(rtresult,"success");
    let para = {"docType":"posFNAMst","searchStr":{"_id":fnaFormId}};
    if(vContinue){
    let fna = await searchAPI.fnSearch(para,"post");
    vContinue = _.get(fna,"success");
    if(vContinue){
        let updateDate = _.get(_.head(fna.result),"updateDate");
        rtresult = rtresult.result;
        let fnaExpiredDays = _.get(_.head(rtresult),"CAFE.fnaExpiredDays");
        updateDate = moment(updateDate).add(fnaExpiredDays, 'days').format("YYYYMMDD");        
        let today = moment().format("YYYYMMDD");
        if(today>updateDate){
          await dbHelper.dbHelpFindOneAndUpdate(ModelFNA,{"_id":fnaFormId},{"EAB.status":"E",updateDate:_.now()}).catch(error=>{
            vContinue = false;
          });
        }
      }
  }
}
  return vContinue;
};
  export {
    calculationAfterCompleteFNA,
    toCafeParam,
    suitabilityCheck,
    affordabilityCheck,
    suitAffordCheckAndUpdateMapping,
    signedFNA,
    fnaExpired
  };