import logger from '../../loaders/logger';
const _ = require('lodash');
// import fnaValueMapping from './fnaValueMapping';
import POSConfig from '../../../config/POSConfig';
const searchAPI = require('../../controller/searchEngineController');
const ModelApplication = require('../../models/ModelApplication');
const dbHelper = require('../../utils/dbHelper');
const utils = require('../../utils/utils');
// const commonFunction = require('../../common/function');
// var moment = require("moment");
const validateForClientEappMapping = function(params){
  let vContinue = true;
  logger.debug("in validateForClientEappMapping",params);
  return vContinue;
};
const getAvailableProductList = function(client,productList){
  let rtnProductList = productList["prodCategories"];

  return rtnProductList;
};
const getDocListForSignature = async function(eAppId){
  let docList = await dbHelper.dbHelpFindOne(ModelApplication,{"_id":eAppId});
  if(docList){
    docList = _.get(docList,"SIGNATURE");
    const signatureConfigs = POSConfig["signatureFormMapping"]["eApp"];
    if(!_.isArray(docList)){
      docList = [docList];
    }
    for(var i in docList){
      let type = docList[i]["type"];
      // logger.debug("type = ",type);
      const signatureConfig = _.find(signatureConfigs,{type:type});
      // logger.debug("signatureConfig = ",signatureConfig);
      if(signatureConfig){
        _.set(docList[i],"docType",signatureConfig.docType);
        const clientSigned = _.get(docList[i],"cleintSigned");
        const agentSigned = _.get(docList[i],"agentSigned");
        _.set(docList[i],"isFullSigned",(clientSigned && agentSigned));
        _.set(docList[i],"docType",signatureConfig.docType);
        if(signatureConfig.self){
          _.set(docList[i],"docId",eAppId);
        }else{
          const relateObject = await searchAPI.getRelatedObject(signatureConfig.value,eAppId,signatureConfig.isCurrent);
          if(relateObject){
            if(!signatureConfig.isCurrent){
              _.set(docList[i],"docId",_.get(_.head(relateObject),"currentID"));
            }else{
              _.set(docList[i],"docId",_.get(_.head(_.get(_.head(relateObject),"relationship")),"oppositID"));
            }
          }
        }        
      }
    }
  }
  return docList;
};
const getDocListForUpload = async function(eAppId){
  // let docList = await dbHelper.dbHelpFindOne(ModelApplication,{"_id":eAppId});
  let docList = {
    "uploadDocs":[
      {
        "groupBy":"",
        "groupName":"My Self",
        "items":[
          {"docType":"idCard","mandatory":true,"eAppDocId":"5e670add9106f1fe73ccb665","clientDocId":"5e55dc99fea3a16c747344fd"},
          {"docType":"marryProve","mandatory":false,"eAppDocId":"5e670add9106f1fe73ccb665","clientDocId":"5e55dc99fea3a16c747344fd"}
        ]
      },
      {
        "groupBy":"",
        "groupName":"Proposer",
        "items":[
          {"docType":"idCard","mandatory":true,"eAppDocId":"5e670add9106f1fe73ccb665","clientDocId":"5e55db36fea3a16c747344f7"},
          {"docType":"marryProve","mandatory":false,"eAppDocId":"5e670add9106f1fe73ccb665","clientDocId":"5e55db36fea3a16c747344f7"}
        ]
      }
    ]
  };
  return docList;
};
const validateForEapplicationStatusUpdate = function(applicationId){
  let vContinue = true;
  logger.debug("in validateForEapplicationStatusUpdate",applicationId);
  return vContinue;
};
const invalidateEapplication = async (applicationId)=>{
  let vContinue = true;
  let updString ={"EAB.isDeleted":true};
  if(vContinue)
     vContinue = validateForEapplicationStatusUpdate(applicationId);
  if(vContinue){
    let resResult = await dbHelper.dbHelpFindOneAndUpdate(ModelApplication,{"_id":applicationId},updString);
    return utils.unlinkAllRelationship("eApp",applicationId);
  }
};
const uploadDocument = async (applicationId)=>{
  let vContinue = true;
  if(vContinue){
    let resResult = await dbHelper.dbHelpFindOneAndUpdate(ModelApplication,{"_id":applicationId},updString);

    if(resResult._id){
      return true;
    }
    else{
      return false;
    }
  }
};
const hasLinked = async (quoteId)=>{
  let linked = false;
// let fnaLinked = await dbHelper.searchAllResult(ModelRelationMapping,{"EAB.type":"quoteEapp"});
// // logger.debug(fnaLinked);
// if(fnaLinked && fnaLinked.length>0){
//   fnaLinked.forEach(item =>{
//     // logger.debug("item =",item);
//     let relation = item.relationship;
//     // logger.debug("in hasLinked relation =",relation);
//     let quote = _.find(relation,{oppositID:_.toString(quoteId)});
//     // logger.debug("in hasLinked quote =",quote);
//     if(quote){
//       linked = true;
//     }
//   });
// }
//   if(!linked){
//   let eAppLinked = await dbHelper.searchAllResult(ModelRelationMapping,{"EAB.type":"quoteEapp","currentID":quoteId});
//   // logger.debug(eAppLinked);
//   if(eAppLinked && eAppLinked.length>0){
//     eAppLinked.forEach(item=>{
//       let relationShip = item.relationship;
//       if(relationShip && relationShip.length>0){
//         linked = true;
//       }
//     })
    
//   }}
/////////
/////////remove
return linked;
};
const getEappListByClient = async(inResult,clientId,lang)=>{
  var eapps = _.cloneDeep(inResult);
  // logger.debug("eapps = ",eapps)

    for(let i in eapps){
      // logger.debug("item = ",eapps[item])
      // const relationQueryStr = {$elemMatch:{oppositID:item._id}};
      logger.debug("applicationStatus = ",_.get(eapps[i],"EAB.applicationStatus",""))
      if(_.eq(_.get(eapps[i],"EAB.applicationStatus",""),"A")){
        _.set(eapps[i],"applyShow",true);
        _.set(eapps[i],"order",1);
      }
      if(_.eq(_.get(eapps[i],"EAB.applicationStatus",""),"S")){
        _.set(eapps[i],"signedShow",true);
        _.set(eapps[i],"order",2);
      }
      if(_.eq(_.get(eapps[i],"EAB.applicationStatus",""),"C")){
        _.set(eapps[i],"submitShow",true);
        _.set(eapps[i],"order",3);
      }
      if(_.eq(_.get(eapps[i],"EAB.applicationStatus",""),"I")){
        _.set(eapps[i],"invalidShow",true);
        _.set(eapps[i],"invalidReason","The proposal is deleted");
        _.set(eapps[i],"order",4);
      }

      if(_.eq(_.get(eapps[i],"EAB.applicationStatus",""),"I") || _.eq(_.get(eapps[i],"EAB.applicationStatus",""),"C")){
        _.set(eapps[i],"viewEappShow",true);
      }
      const failFnaExist = false;
      if(_.eq(_.get(eapps[i],"EAB.applicationStatus",""),"A") && !failFnaExist){
        _.set(eapps[i],"continueEappShow",true);
      }
      const fnaEapp = await searchAPI.getRelatedObject("fnaEapp",_.toString(eapps[i]._id),false).catch(error=>{vContinue = false;errorMessage=error.message});
      // logger.debug("11111111111",fnaEapp);
      if(!_.isEmpty(fnaEapp)){
        _.set(eapps[i],"fnaLinkShow",true);
        var fnaDocId = _.get(_.head(fnaEapp),"currentID");
        _.set(eapps[i],"fna._id",fnaDocId);

      }
      const relatoinMapping = await searchAPI.getRelatedObject("quoteEapp",_.toString(eapps[i]._id),false).catch(error=>{vContinue = false;errorMessage=error.message});
      // logger.debug("relatoinMapping = ",relatoinMapping)
      if(!_.isEmpty(relatoinMapping)){
        const quoteId = _.head(relatoinMapping)["currentID"];
        // logger.debug("quoteId = ",quoteId)
        const queryStr = {docType:"quotation",searchStr:{_id:quoteId}};
        var quote = await searchAPI.fnSearch(queryStr,"post");
        // logger.debug("quote = ",quote)
        // logger.debug("eapps[i] = ",eapps[i]);
        quote =_.head(quote.result);
        const basicPlan = _.find(quote.plans,{"planInd":"B"});
        _.set(quote,"basicCovCode",basicPlan.covCode);
        _.set(quote,"basicPlanCode",basicPlan.planCode);
        _.set(quote,"basicCovName",basicPlan.covName[lang]);
        // logger.debug("quote = ",quote)
        _.set(eapps[i],"quote",quote);
        // logger.debug("eapps[i] = ",eapps[i]);
      }
    }
    _.orderBy(eapps,["order","updateDate"],["desc","desc"]);
    _.remove(eapps,o=>{
      
      const status = _.get(o,"EAB.status");
      const fnaOptional = true;
      const fnaLink =false;
      const fnaLinkWithPass = true;
      const fnaMandatory = true;
      // logger.debug("startEappDisplay = ",true);
      if(_.eq(status,"Q") && ((fnaOptional && !fnaLink) || fnaLinkWithPass)){
        //  logger.debug("startEappDisplay = ",true);
         _.set(o,"startEappDisplay",true);
      }
      if(_.eq(status,"A") && ((fnaOptional && !fnaLink) || (fnaLinkWithPass && fnaMandatory))){
        _.set(o,"continueEappDisplay",true);
      }
      if(_.eq(status,"S")){
        _.set(o,"viewEappDisplay",true);
      }
      return _.get(o,"EAB.isDeleted");
    });
    
    return eapps;
};
  export {
    validateForClientEappMapping,
    getAvailableProductList,
    getDocListForSignature,
    invalidateEapplication,
    getDocListForUpload,
    uploadDocument,
    hasLinked,
    getEappListByClient
  };