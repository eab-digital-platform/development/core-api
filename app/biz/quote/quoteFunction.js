import logger from '../../loaders/logger';
const _ = require('lodash');
// import fnaValueMapping from './fnaValueMapping';
// import systemConfig from '../../../config/systemConfig';
// const searchAPI = require('../../controller/searchEngineController');
// const ModelFNA = require('../../models/ModelFNA');
const dbHelper = require('../../utils/dbHelper');
const utils = require('../../utils/utils');
const commonFunction = require('../../common/function');
var moment = require("moment");
const ModelRelationMapping = require('../../models/ModelRelationMapping')
const ModelApplication = require('../../models/ModelApplication')
const ModelQuote = require('../../models/ModelQuote')
const ModelProductList = require('../../models/ModelProductList')
const validateForQuoteStatusUpdate = function(params){
  let vContinue = true;
  logger.debug("in validateForQuoteStatusUpdate",params);
  return vContinue;
};
const getAvailableProductList = function(client,productList){
  let rtnProductList = productList["prodCategories"];

  return rtnProductList;
};
const hasLinked = async (quoteId,fnaId)=>{
    let linked = false;
  let fnaLinked = await dbHelper.searchAllResult(ModelRelationMapping,{"EAB.type":"fnaQuote"});
  // logger.debug("fnaLinked = ",fnaLinked);
  // logger.debug("quoteId = ",quoteId);
  // logger.debug("fnaId = ",{"EAB.type":"fnaQuote","currentID":{$ne:fnaId}});
  if(fnaLinked && fnaLinked.length>0){
    fnaLinked.forEach(item =>{
      // logger.debug("item =",item);
      let relation = item.relationship;
      // logger.debug("in hasLinked relation =",relation);
      let quote = _.find(relation,{oppositID:_.toString(quoteId)});
      // logger.debug("in hasLinked quote =",quote);
      if(quote){
        linked = true;
      }
    });
  }
  // logger.debug("linked1 = ",linked);
    if(!linked){
    let eAppLinked = await dbHelper.searchAllResult(ModelRelationMapping,{"EAB.type":"quoteEapp","currentID":quoteId});
    // logger.debug(eAppLinked);
    if(eAppLinked && eAppLinked.length>0){
      eAppLinked.forEach(item=>{
        let relationShip = item.relationship;
        if(relationShip && relationShip.length>0){
          linked = true;
        }
      })
      
    }}
    // logger.debug("linked2 = ",linked);
  return linked;
};
const invalidateQuotation = async(quoteId)=>{
  let vContinue = true;
  let updString ={"EAB.isDeleted":true};
  if(vContinue)
     vContinue = validateForQuoteStatusUpdate(quoteId,"01");
  if(vContinue){
    let resResult = await dbHelper.dbHelpFindOneAndUpdate(ModelQuote,{"_id":quoteId},updString);
    return utils.unlinkAllRelationship("quote",quoteId);
  }
};
const isFnaReqiuredProduct = async(productCode)=>{
  var vContinue = true;
  var errorMessage = "";
  
  var products = await dbHelper.dbHelpFindOne(ModelProductList,{}).catch(error=>{if(error){vContinue = false;errorMessage =error.message}});
  var prodCategories = products.prodCategories;
  prodCategories.forEach(cate=>{
    var catProducts = _.get(cate,"products");
    var product = _.find(catProducts,{planCode:productCode})
    return _.get(product,"fnaReqiurement");

  });
  if(!vContinue){
    throw new Error(errorMessage);
  }
  return false;
  
};

const getQuoteListByClient = async(inResult,clientId,lang)=>{
      // var rtResult = {};
      var quotes = _.cloneDeep(inResult);
      var errorMessage = "";
      for(let i in quotes){
        var o = quotes[i];
        const status = _.get(o,"EAB.status");

        let basicPlan = _.find(o.plans,{"planInd":"B"});
        o["basicPlanCode"] = basicPlan.covCode;
        o["basicPlanName"] = basicPlan.covName[lang];

        const fnaOptional = !(await isFnaReqiuredProduct(basicPlan).catch(error=>{if(error){errorMessage = error.message;}}));
        const fnaLink = _.isEmpty(utils.getRelatedObject("clientQuote",_.toString(o._id),false))?false:true;
        const fnaLinkWithPass = fnaLink;//?????????
        const fnaMandatory = !fnaOptional;
        if(_.eq(status,"Q") && ((fnaOptional && !fnaLink) || fnaLinkWithPass)){             
           _.set(o,"startEappDisplay",true);
           _.set(o,"quotedShow",true);
        }
        if(_.eq(status,"A") && ((fnaOptional && !fnaLink) || (fnaLinkWithPass && fnaMandatory))){
          _.set(o,"continueEappDisplay",true);
          _.set(o,"applyingShow",true);
          var con = {"EAB.type":"quoteEapp","currentID":_.toString(o._id)};
          const quoteEapp = await dbHelper.dbHelpFindOne(ModelRelationMapping,con).catch(error=>{if(error)errorMessage = error.message});
          const eAppDocId = _.get(_.head(_.get(quoteEapp,"relationship")),"oppositID","");
          if(quoteEapp && !errorMessage){
            _.set(o,"EAB.eAppDocId",eAppDocId);
          }
          con = {"_id":eAppDocId};
          logger.debug("eAppDocId = ",eAppDocId)
          const eApp = await dbHelper.dbHelpFindOne(ModelApplication,con).catch(error=>{if(error)errorMessage = error.message});
          if(eApp && !errorMessage){
            const eAppId = _.get(eApp,"EAB.eAppId","");
            _.set(o,"EAB.eAppId",eAppId);
          }
        }
        if(_.eq(status,"S")){
          _.set(o,"viewEappDisplay",true);
          _.set(o,"signedShow",true);
        }
        if(_.eq(status,"E")){
          _.set(o,"expiredShow",true);
        }
        _.set(o,"quote._id",_.toString(_.get(o,"_id")));

        const linkedToFna = fnaOptional && !fnaLink;
        const fnaLinkedSuccess = fnaLink;
        const fnaLinkedFail = !fnaOptional && !fnaLink;
        const fnaRequired = !fnaOptional;

        _.set(o,"linkedToFna",linkedToFna);
        if(linkedToFna)
          _.set(o,"linkedToFnaTips","This Proposal is FNA compulsory. You need to link and FNA before staring eApplication");
        _.set(o,"fnaLinkedSuccess",fnaLinkedSuccess);
        _.set(o,"fnaLinkedFail",fnaLinkedFail);
        _.set(o,"fnaRequired",fnaRequired);

        _.set(o,"EAB.clientDocId",clientId);
        _.set(o,"EAB.quoteDocId",o._id);
        // return _.get(o,"EAB.isDeleted");
      }
      // rtResult = {success:true,result:quotes}
      return quotes;
};

  export {
    validateForQuoteStatusUpdate,
    getAvailableProductList,
    hasLinked,
    invalidateQuotation,
    isFnaReqiuredProduct,
    getQuoteListByClient
  };