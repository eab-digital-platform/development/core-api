
import mongoose from 'mongoose';
// var mongoose = require('../utils/db.js');
var Schema = mongoose.Schema;
var ModelApplication = new Schema({
  createDate: { type: Date, default: Date.now },
  createBy: String,
  updateDate: { type: Date, default: Date.now },
  updateBy: String,
  EAB: {type:Object},
  SIGNATURE:{type:Object},
  PAYMENT:{type:Object},
  UPLOAD_DOCUMENTS:{type:Object},
  CAFE: {type:Object}
},
{
    collection: 'posApplicationMst'
});
module.exports = mongoose.model('ModelApplication', ModelApplication);