import mongoose from 'mongoose';
var Schema = mongoose.Schema;

var ModelUserProfile = new Schema({  
    agentCode : String,
    agentCodeDisp : String,
    userID : String,
    agentStatus : Number,
    allowtoSaleStaffPlan : Boolean,
    brokerMasterCode : String,
    brokerProposerLimit : Number,
    brokerSalesLimit : Number,
    channel : String,
    compCode : String,
    company : Object,
    contractEffDate : Date,
    contractStatus : Number,
    createDate : Date,
    distributionCode : String,
    email : String,
    firstName : String,
    firstNameChi : String,
    fullName : String,
    fullNameChi :String,
    keycloakUserID : String,
    lastName : String,
    lastNameChi : String,
    lastUpdateDate : Date,
    managerCode : String,
    mobile : String,
    name : String,
    organization : String,
    passcode : String,
    position : String,
    profileId : String,
    role : String,
    status : String,
    submitAgentCode : String,
    terminationDate : Date,
    title : String,
    type : String,
    unitCode : String,
    userType : String
},
{
    collection: 'userProfile'
});

module.exports = mongoose.model('ModelUserProfile', ModelUserProfile);