import mongoose from 'mongoose';
var Schema = mongoose.Schema;

var ModelProductList = new Schema({  
    createDate: { type: Date, default: Date.now },
    createBy: String,
    updateDate: { type: Date, default: Date.now },
    updateBy: String,
    EAB: {type:Object},
    prodCategories: {type:Object}
},
{
    collection: 'posProductList'
});

module.exports = mongoose.model('ModelProductList', ModelProductList);