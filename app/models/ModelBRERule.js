import mongoose from 'mongoose';
var Schema = mongoose.Schema;
var ModelBRERule = new Schema({
    createBy: String,
    updateBy:String,
    updateDate:{type:Date,default:Date.now},
    EAB: {type:Object},
    options:{type:Object},
    ruleType:String,
    createDate:{type:Date,default:Date.now}
},
{
    collection: 'BRERuleSetting'
});
module.exports = mongoose.model('ModelBRERule', ModelBRERule);