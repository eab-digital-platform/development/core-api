import mongoose from 'mongoose';
import { date } from 'joi';
var Schema = mongoose.Schema;
var ModelSysMappingTable = new Schema({
  mappingType:String,
  mapping:{
    type: Array,
    default: []
}
},
{
    collection: 'sysMappingTable'
});
module.exports = mongoose.model('ModelSysMappingTable', ModelSysMappingTable);