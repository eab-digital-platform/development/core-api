const uploadSetting = require('./uploadSetting');
const _ = require('lodash');
var xlsx = require('node-xlsx');
var fs = require('fs');
var moment = require("moment");
//读取文件内容
const uploadExcel = (excelPath)=>{
    // console.log("11111111111111 = ",moment().format("YYYY-MM-DD"));
    var obj = xlsx.parse(excelPath);
    var excelObj=obj[0].data;
    // console.log(excelObj);
    var data = [];
    let ignoreRows = uploadSetting.fnaMappingTools.ignoreRows;
    let ignoreCols = uploadSetting.fnaMappingTools.ignoreCols;
    let maxExcelCols = uploadSetting.fnaMappingTools.maxExcelCols;
    let maxExcelRows = uploadSetting.fnaMappingTools.maxExcelRows;
    let rows = uploadSetting.fnaMappingTools.rows;

    let uploadJson ={};
    let rowsValue = [];
    let readRowIndex = 0;
    let loopIndex = 0;
    for(let i in rows){
        let settingRow = rows[i];
        let rowSpan = _.get(settingRow,"rowSpan",1);
        let rowName = _.get(settingRow,"rowName");
        let repeat = _.get(settingRow,"repeat",1);
        let columns = rows[i].columns;
        console.log("rowName = ",rowName);
        while(repeat > 0 && readRowIndex < maxExcelRows){
            while(_.indexOf(ignoreRows,readRowIndex+1)>=0 && readRowIndex<=maxExcelRows){
                readRowIndex ++;
            }
            console.log("readRowIndex = ",readRowIndex);
            let valueRow = excelObj[readRowIndex];
            let readColIndex = 0;
            for(let j in columns){
                let colSpan = _.get(columns[j],"colSpan",1);
                // while((!valueRow[readColIndex]||_.indexOf(ignoreCols,readColIndex+1)>=0) && readColIndex<=maxExcelCols){
                while(_.indexOf(ignoreCols,readColIndex+1)>=0 && readColIndex<=maxExcelCols){
                    readColIndex ++;
                }
                while(colSpan >0){
                    let valueCol = valueRow[readColIndex]
                    let ignoreSentence = _.get(columns[j],"ignoreSentence",[]);
                    for(let t in ignoreSentence){
                        valueCol = _.replace(valueCol,ignoreSentence[t],"");
                    }
                    // console.log("readColIndex = ",readColIndex);
                    if(valueCol)
                        console.log("valueCol1 = ",valueCol);
                    readColIndex ++;
                    colSpan --;
                }
                // readColIndex = readColIndex + colSpan - 1;           
            }
        repeat --;        
        readRowIndex ++;
        readRowIndex = readRowIndex + rowSpan - 1;
        }
        
    }
};
uploadExcel('D:\\New folder\\Ditital platform\\node-restful-api-starter\\pos-api\\app\\utils\\DigitalPlatformProducts.xlsx');