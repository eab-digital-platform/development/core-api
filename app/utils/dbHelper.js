const axios = require('axios');
const searchCnt = (modelObj, queryParams) => new Promise((resolve, reject) => {
  modelObj.countDocuments(queryParams,function (err, count) {
     if(err){
      reject(err);
     }else{
      resolve(count);
     }
  });
});

const searchPageResult = (modelObj, queryParams, start,
  pageSize, sortParams) => new Promise((resolve, reject) => {
    modelObj.find(queryParams).skip(start).limit(pageSize)
    .sort(sortParams)
    .exec((err, doc) => {
      if (err) {
        reject(err);
      } else {
        resolve(doc);
      }
    });
});

const pageQuery = (page, pageSize, modelObj,
  queryParams, sortParams) => new Promise((resolve, reject) => {
    // console.log("pageQuery ="+JSON.stringify(queryParams));
    // console.log("pageQuery page="+page);
  const start = page * pageSize;
  const pageinfo = { pageNumber: page };
  // console.log(start);
  axios.all([searchPageResult(modelObj, queryParams, start, pageSize, sortParams),
    searchCnt(modelObj, queryParams)])
    .then(axios.spread((records, count) => Promise.resolve({
      records,
      count,
    })))
    .then((result) => {
      pageinfo.totle = result.count;
      pageinfo.results = result.records;
      resolve(pageinfo);
    })
    .catch((error) => {
      reject(error);
    });
});


const searchAllResult = (modelObj,queryParams, sortParams) => new Promise((resolve, reject) => {
  modelObj.find(queryParams)
    .sort(sortParams)
    .exec((err, doc) => {
      if (err) {
        reject(err);
      } else {
        resolve(doc);
      }
    });
});

const find = (modelObj,queryParams) => new Promise((resolve, reject) => {
  modelObj.find(queryParams)
    .exec((err, doc) => {
      if (err) {
        reject(err);
      } else {
        resolve(doc);
      }
    });
});

const searchQuery= (modelObj, queryParams,queryAndOrParams,
   populate, sortParams) => new Promise((resolve, reject) => {
    modelObj.find(queryParams).and(queryAndOrParams)
    .sort(sortParams)
    .exec((err, doc) => {
      if (err) {
        reject(err);
      } else {
        resolve(doc);
      }
    });
});


const loadGroupAndProject = (groupMode,groupParams,sortGroup,projectModel,projectParams,sortProject) => new Promise((resolve, reject) => {
  const info = {};
  axios.all([searchAllResult(groupMode,groupParams,sortGroup),
    searchAllResult(projectModel,projectParams,sortProject)])
    .then(axios.spread((groupList, projectList) => Promise.resolve({
      groupList,
      projectList,
    })))
    .then((result) => {
      info.group = result.groupList;
      info.project = result.projectList;
      resolve(info);
    })
    .catch((error) => {
      reject(error);
    });
});

const dbHelpFindOneAndUpdate = (Model, queryParams,updStr) => new Promise((resolve, reject) => {
  
  Model.findOneAndUpdate(queryParams,
  {$set: 
    updStr
  }, 
  {new: true}, // return updated record; if false return record before update
  (err, newRecord) => {
    if (err ){
        reject(err);
    } else {
      resolve(newRecord);
    }

}); 
  
});
const dbHelpFindOneAndPush = (Model, queryParams,updStr) => new Promise((resolve, reject) => {
  
  Model.update(queryParams,
  {$push: 
    updStr
  }, 
  {new: true}, // return updated record; if false return record before update
  (err, newRecord) => {
    if (err ){
        reject(err);
    } else {
      resolve(newRecord);
    }

}); 
  
});
const dbHelpFindOneAndPull = (Model, queryParams,updStr) => new Promise((resolve, reject) => {
  
  Model.update(queryParams,
  {$pull: 
    updStr
  }, 
  {new: true}, // return updated record; if false return record before update
  (err, newRecord) => {
    if (err ){
        reject(err);
    } else {
      resolve(newRecord);
    }

}); 
  
});
const dbHelpFindOneAndAddToSet = (Model, queryParams,updStr) => new Promise((resolve, reject) => {
  
  Model.update(queryParams,
  {$addToSet: 
    updStr
  }, 
  {new: true}, // return updated record; if false return record before update
  (err, newRecord) => {
    if (err ){
        reject(err);
    } else {
      resolve(newRecord);
    }

}); 
  
});

const dbHelpSave = (Model) => new Promise((resolve, reject) => {
  Model.save((err, res) => {
    if (err) {
      reject(err);
    } else {
      resolve(res);
      // console.log(`Res:${res}`);
    }
  });
  
});
const dbHelpFindOne =(Model,queryParams) => new Promise((resolve, reject) => {
  Model.findOne(queryParams,function(err, doc){
     if(err){
      reject(null);
     }else {
      resolve(doc);
     }
  });
  
});
const dbHelpRemove = (Model,queryParams) => new Promise((resolve, reject) => {
 var cnt = Model.remove(queryParams, function (err) {
    if(err){
      reject(err);
    }else{
      resolve('Removed Success');
    }
});
});

const loadExpTimeSheet = (groupMode,groupParams,sortGroup,
  projectModel,projectParams,sortProject,
  modelStaff,staffParams,sortStaff) => new Promise((resolve, reject) => {
  const info = {};
  axios.all([searchAllResult(groupMode,groupParams,sortGroup),
    searchAllResult(projectModel,projectParams,sortProject),
    searchAllResult(modelStaff,staffParams,sortStaff),
  ])
    .then(axios.spread((groupList, projectList,staffList) => Promise.resolve({
      groupList,
      projectList,
      staffList,
    })))
    .then((result) => {
      info.success =true;
      info.group = result.groupList;
      info.project = result.projectList;
      info.staff =result.staffList;
      // console.log(JSON.stringify(result));
      resolve(info);
    })
    .catch((error) => {
      reject(error);
    });
});

const searchResult = (Model,queryParams, sortParams) => new Promise((resolve, reject) => {
  Model.find(queryParams)
    .sort(sortParams)
    .exec((err, doc) => {
      if (err) {
        reject(err);
      } else {
        resolve(doc);
      }
    });
});


const dbHelpUpdate = (Model, queryParams,updStr) => new Promise((resolve, reject) => {
  Model.update(queryParams,
   {$set:updStr} , 
  {multi: true}, // return updated record; if false return record before update
  (err, newRecord) => {
    if (err ){
        reject(err);
    } else {
      resolve(newRecord);
    }
}); 
});


const allGroupAndStaff = (groupMode,groupParams,sortGroup,modelStaff,staffParams,sortStaff) => new Promise((resolve, reject) => {
  const info = {};
  axios.all([searchAllResult(groupMode,groupParams,sortGroup),
    searchAllResult(modelStaff,staffParams,sortStaff)])
    .then(axios.spread((groupList, staffList) => Promise.resolve({
      groupList,
      staffList,
    })))
    .then((result) => {
      info.group = result.groupList;
      info.staff = result.staffList;
      resolve(info);
    })
    .catch((error) => {
      reject(error);
    });
});
const textIndexSearch= (modelObj, keyWords) => new Promise((resolve, reject) => {
   modelObj.find({ $text: { $search: keyWords }},{ score: { $meta: "textScore" }  }).sort( { score: { $meta: "textScore" } })
   .exec((err, doc) => {
     if (err) {
       reject(err);
     } else {
       resolve(doc);
     }
   });
});

const dbHelpAggregate = (Model, lookup,match) => new Promise((resolve, reject) => {
  Model.aggregate([{
    $match: match
  },{
    $lookup:lookup
  }
  ]).exec(function (error,doc){
    if (error ){
      reject(error);
    } else {
    resolve(doc);
    }
}); 
});
module.exports = {
  searchCnt,
  pageQuery,
  searchAllResult,
  searchQuery,
  loadGroupAndProject,
  dbHelpFindOneAndUpdate,
  dbHelpUpdate,
  dbHelpSave,
  dbHelpFindOne,
  dbHelpRemove,
  loadExpTimeSheet,
  searchResult,
  allGroupAndStaff,
  textIndexSearch,
  dbHelpAggregate,
  dbHelpFindOneAndPush,
  dbHelpFindOneAndAddToSet,
  dbHelpFindOneAndPull,
  find
};
