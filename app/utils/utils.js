import logger from '../loaders/logger';
import POSConfig from '../../config/POSConfig';
import axios from 'axios';
import request from 'request-promise';
const ModelSysMappingTable = require("../models/ModelSysMappingTable");
const ModelRelationMapping = require("../models/ModelRelationMapping");
const _ = require('lodash');
const dbHelper = require('../utils/dbHelper');
const fs = require('fs');
// const  getDocType =(formID)=> new Promise(resolve=>{
//     resolve(_getDocType(formID));
// });
const  getDocType = async (formID)=>{
    let docType = {};
    let queryParams = {"mappingType":"formModelMapping"};
    // logger.error("utils._getDocType queryParams = ",queryParams);
    let result = await dbHelper.dbHelpFindOne(ModelSysMappingTable,queryParams);
    let formModelMapping = _.get(result,"mapping",[]);
    if(formModelMapping && formModelMapping.length>0){
        let condition = _.set({},"formID",formID);
        docType = _.find(formModelMapping,condition);
        // for(let it in docTypeMapping){
        //     docType = _.set(docType,it,docTypeMapping[it]);
        // }
    }
    // logger.error("utils._getDocType docType = ",docType);
    return docType;
};

const  dbClone = async function (Model,fromId,clientDocId,resetParams){
    const oldOne = await dbHelper.dbHelpFindOne(Model,{_id:fromId}).catch(error=>{return false});
    var newOne =new Model();
    const newId = newOne._id;
    console.log(`newId`,newId);
    newOne = _.cloneDeep(oldOne);
    _.set(newOne,"_id",newId);
    _.set(newOne,"isNew",true);
    if(resetParams){
        for(let i in resetParams){
            _.set(newOne,i,resetParams[i]);
        }
    }
    _.set(newOne,"iCDocId",clientDocId);
    await dbHelper.dbHelpSave(newOne).catch(error=>{return false});
    return newId;
    // return "newId";
};
const  formatString = function (fieldValue,formatType){
    var pattern = "";
    if(_.eq(formatType,"ccy")){
        pattern = /(?=((?!\b)\d{3})+$)/g;
    }
    return String(fieldValue).replace(pattern, ',');
};
const handleArray=(root,datasrc,formatType,callBack)=>{
    let rtnSrc = datasrc;
  let array = root.split(".");
  let first = _.get(datasrc,array[0]);
  let right = _.takeRight(array,array.length-1);
  right = _.join(right,".");
    if(_.isArray(first)){
      first.forEach(item=>{
        handleArray(right,item,formatType,callBack);
      });
    }else{
       first = callBack(first,formatType);
       _.set(rtnSrc,array[0],first);
    }
    return rtnSrc;
  };
  const getFileFromFileSystem = async (fileName)=>{
      let filePath = "";
      let rtFileData ="";
    await axios({
        method: "get",
        url: `${POSConfig["file-api"]}/${POSConfig["pdfBucket"]}/${fileName}`,
        // data: parameter,
        headers: {
            "Content-Type": "application/text"
        }
        })
        .then((response) => {
            logger.debug("getFileFromFileSystem = ",response.data)
            filePath = response.data;
            // logger.logger("filePath =",filePath);
        })
        .catch((error) => {
          logger.error("getFileFromFileSystem error:",error.message)
        });
        if(filePath){
        await axios({
            method: "get",
            url: `${filePath}`,
            // data: parameter,
            // headers: {
            //     "Content-Type": "application/octet-stream"
            // },
            responseType:"arraybuffer"
            })
            .then((response) => {
                rtFileData = response.data;
                // logger.logger("rtFileData =",rtFileData);
            })
            .catch((error) => {
              logger.error("getFileFromFileSystem error:",error.message)
            });
        }
        return rtFileData;
  };
  const saveFileToFileSystem = async (fileName,fileStream)=>{
    let saved =false;
      const options = {
          method: 'PUT',
          url: `${POSConfig["file-api"]}/${POSConfig["pdfBucket"]}/${fileName}`,
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
          },
          formData: {
            file: {
              value: fileStream,
              options: {
                filename: fileName,
                contentType: null,
              },
            },
          },
        };
        const result = await request(options).catch(error => {saved=false; logger.error("saveFileToFileSystem error:",error.message) });
        logger.debug("saveFileToFileSystem result = ",result)
        if(_.eq(result,"{}")){
            saved=true;
        }        
      return saved;
};
  const fileToBase64 = async(fileData) => {
    // let data = fs.readFileSync(fileData);
    let data = new Buffer(fileData).toString('base64');
    let base64 =  data;
    return base64;
  };
  const base64ToFile = async(filePath,fileName,base64) => {
    // logger.debug(base64);
    var success = false;
    var dataBuffer = new Buffer(base64, 'base64');
    await fs.writeFile(filePath+fileName, dataBuffer, function(err) {
      if(err){
        success= false;
      }else{
        success= true;
      }
      });
    return success;
  };
  const getRelatedObject = async (relationType,curId,isCurrent)=>{
    var criteria = {"EAB.type":relationType};
    let str = {$elemMatch:{oppositID:curId}};
    if(isCurrent){
      _.set(criteria,"currentID",curId);
    }else{
      _.set(criteria,"relationship",str);
    }
    let rtnValue = await dbHelper.searchAllResult(ModelRelationMapping,criteria);
    // logger.debug("getRelatedObject rtnValue = ",rtnValue);
    return rtnValue;
  };
  const unlinkRelationship = async (relationType,curId,isCurrent)=>{
    var criteria = {"EAB.type":relationType};
    var str = {$elemMatch:{oppositID:curId}};
    var rtnValue = true;
    if(isCurrent){
      _.set(criteria,"currentID",curId);
      await dbHelper.dbHelpRemove(ModelRelationMapping,criteria).catch(error=>{if(error)rtnValue =false});
    }else{
      _.set(criteria,"relationship",str);
      await dbHelper.dbHelpFindOneAndPull(ModelRelationMapping,criteria,_.set({},"relationship",{oppositID:curId})).catch(error=>{if(error)rtnValue =false});
    }
    // logger.debug("getRelatedObject rtnValue = ",rtnValue);
    return rtnValue;
  };
  const unlinkAllRelationship = async (unlinkType,curId)=>{
    var unlinkConfig = POSConfig["unlinkRelationshipSetting"];
    unlinkConfig = _.get(unlinkConfig,unlinkType);
    if(unlinkConfig){
      for(let i in unlinkConfig){
        var unlinkResult = await unlinkRelationship(_.get(unlinkConfig[i],"unlinkRelationship"),curId,_.get(unlinkConfig[i],"isCurrent"));
        if(!unlinkResult){return false;}
      }
    }
    return true;
  };
export {
    getDocType,
    dbClone,
    formatString,
    handleArray,
    getFileFromFileSystem,
    fileToBase64,
    saveFileToFileSystem,
    base64ToFile,
    getRelatedObject,
    unlinkRelationship,
    unlinkAllRelationship
};