import systemConfig from '../../config/systemConfig';
import userConfig from '../../config/userConfig';
import powerSearchConfig from '../../config/powerSearchConfig';
import logger from '../loaders/logger';
import mongoose from 'mongoose';
var ModelSeqMst = require('../models/ModelSeqMst');
var powerSearchProperties = require('../powerSearchProperties');
var ModelRelationMapping = require('../models/ModelRelationMapping');
var ModelSysMappingTable = require('../models/ModelSysMappingTable');
const _ = require('lodash');
const dbHelper = require('../utils/dbHelper');
const utils = require('../utils/utils');
var moment = require("moment");
const searchAPI = require('./searchEngineController');
// function getSystemCollectionRule(searchType){
//   //need to be implement
//   return systemConfig.collectionRules[searchType];
// }
const getCoreRelatedObject = async (relationType,curId,isCurrent)=>{
  var criteria = {"EAB.type":relationType};
  let str = {$elemMatch:{oppositID:curId}};
  if(isCurrent){
    _.set(criteria,"currentID",curId);
  }else{
    _.set(criteria,"relationship",str);
  }
  // logger.debug("curId = ",curId);
  // logger.debug("criteria = ",criteria);
  let rtnValue = {};
  await dbHelper.searchAllResult(ModelRelationMapping,criteria)
  .then(result=>{
    // logger.debug("result = ",result);
    rtnValue = {success:true,result:result}
  }).catch(error=>{
    if(error){
      rtnValue = {success:false,message:error.message}
      logger.error("error in getCoreRelatedObject : ",error.message);
    }
  });
  // logger.debug("size of result in getCoreRelatedObject : ",_.size(rtnValue));
  return rtnValue;
};
const  initCreateMetaData =(model,patameter) => new Promise((resolve,reject) =>{
  resolve(_initCreateMetaData(model,patameter));
});
const _initCreateMetaData = async (model,patameter)=>{
  var rt = {};
  const collectionRule = getUserCollectionRule(model);
  // logger.debug("collectionRule:",collectionRule);
  const createMetaDatas = collectionRule.createMetaDatas;
  // logger.debug("createMetaDatas:",createMetaDatas);
  if(createMetaDatas){
  for(var createDefaultField of createMetaDatas){  
    // logger.debug("createDefaultField:",createDefaultField);    
     await replaceFields(model,createDefaultField,patameter).then(result=>{       
      if(result.success){
        _.set(rt,createDefaultField.fieldName,result["result"]);
      }else{
        logger.debug("error message:",result["message"]);
        // _.set(rt,createDefaultField,result["message"]);
      }     
    });      
  };
  _.set(rt,"createDate",_.now());
  _.set(rt,"updateDate",_.now());
  }
  // logger.debug("rt:",rt);
  return rt;
}
function getUserCollectionRule(searchType){
  //need to be implement
  return userConfig.collectionRules[searchType];
}
function getUserCollectionRuleByModelName(modelName){
  //need to be implement
  return _.find(userConfig.collectionRules,_.set({},"model",modelName));
}
function getLoginInfo(){
   //need to be implement
  return {"fullName":"zeng.fanzhong","userCode":"088811","compCode":"01","compSubCode":"AG","agentCode":"000001"};
}
async function replaceFields(modelType,defindedField,parameter){
  const fieldName = defindedField["fieldName"];
  const fieldValue = defindedField["fieldValue"];
  const rule = defindedField["rule"];
  const seqRule = defindedField["seqRule"];
  const source = defindedField["source"];

  const internalInfo = getLoginInfo();

  if(fieldValue.indexOf("{")>=0 && fieldValue.indexOf("}")>=0){
       if(fieldValue=="{generate}"){
          finalFieldValue = rule;
          for(var logininfo in internalInfo){            
            if(finalFieldValue.indexOf("{"+logininfo+"}")>=0){
              finalFieldValue=finalFieldValue.replace("{"+logininfo+"}",internalInfo[logininfo]);
            }
          }
          if(finalFieldValue.indexOf("{seq}")>=0){
            const seqResult = await dbHelper.dbHelpFindOne(ModelSeqMst,{"_id":modelType});
            var seq = 1;
            if(!_.isEmpty(seqResult)){
              seq = seqResult["value"]+1;
            }else{
              var seqMst = new ModelSeqMst({
                 "_id":modelType,
                 "value":1
              });
              await dbHelper.dbHelpSave(seqMst);
            }
            let strSeq ="";
            await dbHelper.dbHelpUpdate(ModelSeqMst,{"_id":modelType},{value:seq});
            if(seqRule["paddingChar"] && seqRule["paddingCNT"]){
              for(let i=0;i<seqRule["paddingCNT"]-seq.toString().length;i++){
                strSeq = strSeq + seqRule["paddingChar"];
              }
            }
            strSeq = strSeq + seq;
            finalFieldValue = finalFieldValue.replace("{seq}",strSeq);
          }
          return {"success":true,result:finalFieldValue};
       }else{
        var finalFieldValue = fieldValue.replace("{","").replace("}","");
        if(source=="loginInfo"){
            if(internalInfo.hasOwnProperty(finalFieldValue)){
                return {"success":true,result:internalInfo[finalFieldValue]};
            }else{
                return {"success":false,message:fieldName+" : No "+finalFieldValue+" definded in Login information"};
            }
          
        }else if(source=="parameter"){
              if(parameter.data.hasOwnProperty(finalFieldValue)){
                return {"success":true,result:parameter.data[finalFieldValue]};
              }else{
                return {"success":false,message:fieldName+" : No "+finalFieldValue+" definded in input Parameter"};
              }

        }
      }
}else{
  return {"success":true,result:fieldValue};
}

};
const fillValueFromObject = function(fieldNameList,fromObject,formatFields){
  // logger.debug("fromObject = ",fromObject);
  // logger.debug("formatFields = ",formatFields);
  var rtnObject = {};
  if(_.head(fieldNameList).indexOf("*")!=0){
    // logger.debug("fieldNameList=",fieldNameList);
    // logger.debug("fromObject=",fromObject["EAB"]);
    let dateFormat = systemConfig.dateFormat;
    fieldNameList.forEach(showField=>{
      let fieldValue = {};
      if(showField.indexOf(".*")>=0){
        showField = showField.replace(".*","");
        fieldValue = _.get(fromObject["_doc"],showField);   
      }
      else{
        fieldValue = _.get(fromObject,showField);
        if(showField.indexOf("{datetime}")>=0){
          showField = showField.replace("{datetime}","");
          fieldValue = _.get(fromObject,showField);
          fieldValue = moment(fieldValue).format(dateFormat);
        }else{
          if(_.isDate(fieldValue)){
            fieldValue = moment(fieldValue).format(dateFormat);
          }
        }        
      }
      rtnObject = _.set(rtnObject,showField,fieldValue);
    });
  }else{
    rtnObject = fromObject["_doc"];
    
  }
  // logger.debug("rtnObject 111 =",rtnObject);
  if(formatFields)
    rtnObject = formatField(formatFields,rtnObject);
  // logger.debug("rtnObject 222 =",rtnObject);
  return rtnObject;
}
const formatField = function(formatFieldsConfig,fromObject){
  var rtnObject = fromObject;
  // logger.debug("formatFieldsConfig 111 =",formatFieldsConfig);
  if(!_.isArray(formatFieldsConfig)){
    formatFieldsConfig = [formatFieldsConfig];
  }
  formatFieldsConfig.forEach(formatField=>{
    let fieldName = _.get(formatField,"fieldName");
    let formatType = _.get(formatField,"formatType");
    let as = _.get(formatField,"as");
    if(!as){
      as = fieldName;
    }
    let fieldValue = _.get(fromObject,fieldName);
    if(fieldValue){
      if(_.eq(formatType,"ccy")){
        fieldValue = utils.formatString(fieldValue,"ccy");
      }      
    }else{
      rtnObject = utils.handleArray(formatField["fieldName"],rtnObject,formatType,utils.formatString);
    }
    rtnObject = _.set(rtnObject,as,fieldValue);
  });
  // logger.debug("rtnObject = ",rtnObject)
  return rtnObject;
}

const prepareLookupDesc = async function(lookupDesc){
  let lookupDescs = {};
  // logger.debug("lookupDesc = ",lookupDesc);
  for(let i in lookupDesc){
    // logger.debug("lookupDesc = ",lookupDesc[i]);
    let lookupModelName = _.get(lookupDesc[i],"fromModel");
    // logger.debug("lookupModelName = ",lookupModelName);
    let lookupModel = require("../models/"+lookupModelName);
    let fromMatch = lookupDesc[i].fromMatch;
    let lookupCriteria = {};
    // logger.debug("lookupCriteria = ",lookupCriteria);
    if(fromMatch){
      lookupCriteria = _.set({},fromMatch.matchField,fromMatch.matchValue);
    }
    let loopResult = await dbHelper.searchAllResult(lookupModel,lookupCriteria);
    lookupDescs = _.set(lookupDescs,lookupDesc[i].localField,loopResult);   
  }
  return lookupDescs;
}
const getLookupDesc = function(paramStaffWithRelation,lookupDesc,lookupDescValue,language){
  let staffWithRelation = paramStaffWithRelation;
  // logger.debug("staffWithRelation = ",staffWithRelation);
  if(!language){
    language = "en";
  }
  for(let i in lookupDesc){
    let productDescs = [];
    let localField = lookupDesc[i].localField;
    let foreignPath = lookupDesc[i].foreignPath;
    let foreignField = lookupDesc[i].foreignField;
    let subForeignPath = lookupDesc[i].subForeignPath;
    // logger.debug("foreignPath = ",foreignPath);
    let lookupValues = _.head(_.get(lookupDescValue,localField));
    // logger.debug("lookupValues = ",lookupValues);
    lookupValues = _.get(lookupValues,foreignPath);
    // logger.debug("lookupValues = ",lookupValues);

    let isValueArray =true;
    if(subForeignPath){
      let localFieldValue =_.get(staffWithRelation,localField);
    if(!_.isArray(localFieldValue)){
      localFieldValue = [localFieldValue];
      isValueArray = false;
    }
    localFieldValue.forEach(item1 =>{
      let lookupValues2 ="";
    lookupValues.forEach(item=>{            
      let lookupValues1 = item[subForeignPath];                  
        let lookupValues3 = _.find(lookupValues1,_.set({},foreignField,item1))
        if(lookupValues3){
          lookupValues2 = lookupValues3[lookupDesc[i].lookup];
          lookupValues2 = _.get(lookupValues2,language);
        }          
      })
      if(!isValueArray){
        productDescs ={"itemValue":item1,"itemDesc":lookupValues2};
      }
      else{
        productDescs = _.concat(productDescs,{"itemValue":item1,"itemDesc":lookupValues2});
      }               
      });
      // logger.debug("productDescs = ",productDescs)
      staffWithRelation = _.set(staffWithRelation,localField,productDescs);
    }
    else{
      let localFieldValue =_.get(staffWithRelation,localField);
      
      if(!_.isArray(localFieldValue)){
        localFieldValue = [localFieldValue];
        isValueArray = false;
      }
      localFieldValue.forEach(item1=>{
        let lookupValues2 ="";
        let lookupValue = _.find(lookupValues,_.set({},foreignField,item1));
          lookupValue = _.get(lookupValue,lookupDesc[i].lookup);
          lookupValue = _.get(lookupValue,language);
          // logger.debug("lookupValues2 = ",lookupValues2);
          if(!isValueArray){
            productDescs ={"itemValue":item1,"itemDesc":lookupValue};
          }
          else{
            productDescs = _.concat(productDescs,{"itemValue":item1,"itemDesc":lookupValue});
          }
      })
      // lookupValue = _.get(lookupValue,lookupDesc[i].lookup);
      // lookupValue = _.get(lookupValue,language);
      // logger.debug("lookupValue = ",lookupValue);
      staffWithRelation = _.set(staffWithRelation,localField,productDescs);     
    }
  }
  //  logger.debug("staffWithRelation = ",staffWithRelation);
  return staffWithRelation;
}

async function _fnSearch(parameter,queryType){
  logger.debug("_fnSearch = ",parameter);
  var searchType = parameter.docType;
  var searchKey = parameter.searchKey;
  var showSelf = parameter.showSelf;
  var showRelation = parameter.showRelation;
  let queryParams = {};
  let queryAndOrParams= {};
  let orderBy = {};
  let finalResult = [];
  let resultIndex = 0;
  let rpResultSuccess = true;
  
  const searchRule = getUserCollectionRule(searchType);
  const indexFields = searchRule.indexFileds;
  const showFieldsList = _.get(_.get(searchRule,"showFieldsList"),_.get(parameter,"showFieldsList","default"));
  let lookupDesc = _.get(searchRule,"lookupDesc",[]);
  let sortBy = _.get(searchRule,"sortBy",[]);
  const exactMatchFields = searchRule.dependency;
  const orderByConfig = searchRule.orderBy;
  let intexItems = []; 
  let intexItemIndex = 0;
  
  if(indexFields && searchKey){   
    indexFields.forEach((indexField) => {
      let intexItem = {};
      intexItem[indexField] = {'$regex': searchKey, $options: '$i'};
      intexItems[intexItemIndex++]=intexItem;
    })
    queryAndOrParams={$or:intexItems};
  }
  let notInArray = _.get(parameter,"notInArray",[]);
  if(notInArray.length>0){
    queryAndOrParams = _.assign(queryAndOrParams,{"_id":{$nin:notInArray}})
  }
  if (exactMatchFields) {
    for(var exactMatchField of exactMatchFields){
      await replaceFields(searchType,exactMatchField,parameter).then(result =>{
        if(result["success"]){
          queryParams[exactMatchField["fieldName"]]=result["result"];
        }else{          
          finalResult[resultIndex++] = resultIndex+". "+result["message"];
          rpResultSuccess = false;
        }
      });
    }
  }
   if(rpResultSuccess){
      if(parameter.searchStr){
        let searchString =parameter.searchStr;
        if(queryType=="get"){
          searchString = JSON.parse(parameter.searchStr);
        }
        for(var str in searchString){
          queryParams[str] = searchString[str];
        }
      }
      if(orderByConfig){
        orderByConfig.forEach(order=>{
        orderBy[order["orderName"]]=order["order"];
        })
      }
      logger.info("orderBy =",orderBy);
      logger.info("queryParams =",queryParams);
      logger.info("queryAndOrParams =",queryAndOrParams);
      var Model = require('../models/'+searchRule.model);    
      var newStaffs = await dbHelper.searchQuery(Model,queryParams,queryAndOrParams,
        {},
        orderBy).catch(error=>{
          finalResult[resultIndex++]= ""+error;
        });
        _.remove(newStaffs,(newStatff)=>{return _.get(newStatff,"EAB.isDeleted")});
        if(_.get(newStaffs,"length",0)>0){
          let retrunStaffs = [];
          let lookupDescs = await prepareLookupDesc(lookupDesc);
          // logger.debug("lookupDescs = ",lookupDescs);
            for(let newStaff of newStaffs){
              if(showSelf){
                _.set(newStaff,"isSelf",true);
              }
              let staffWithRelation =fillValueFromObject(showFieldsList,newStaff,_.get(searchRule,"formatFields"));
              // logger.debug("showFieldsList = ",showFieldsList);          
              staffWithRelation = getLookupDesc(staffWithRelation,lookupDesc,lookupDescs,"en");
              if(showRelation){
                let relationSearchParameter = {};
                relationSearchParameter["docType"]=searchType;
                relationSearchParameter["key"]=""+newStaff._id;
                relationSearchParameter["lang"]=_.get(parameter,"lang","en");
                relationSearchParameter["relationType"]=_.get(parameter,"relationType","");
                let relation = await _fnRelationalSearch(relationSearchParameter,queryType);
                // logger.debug("relationSearchParameter = ",relationSearchParameter)
                if(_.get(relation,"success")){
                  let familyMember = relation["result"];
                  // logger.debug("familyMember result = ",relation);
                  if(showSelf){
                    familyMember.forEach(item=>{
                      staffWithRelation =_.concat(staffWithRelation,fillValueFromObject(showFieldsList,item,_.get(searchRule,"formatFields")));
                    });
                  }else{
                    staffWithRelation[_.get(parameter,"relationType","")] = familyMember;
                  }
                }
              }
              retrunStaffs = _.concat(retrunStaffs,staffWithRelation);
          //  logger.debug("retrunStaffs = ",retrunStaffs);

          }
          if(sortBy){
            let orderName = [];
            let sortType = [];
            sortBy.forEach(item=>{
              orderName = _.concat(orderName,item.sortName);
              sortType = _.concat(sortType,item.sortType);
            });
            retrunStaffs = _.orderBy(retrunStaffs,orderName,sortType);
          }

          finalResult= {"success":true,result:retrunStaffs};
          // logger.info("search count = ",newStaffs.length);
          // logger.info("finalResult = ",finalResult);
        }else{
          finalResult= "No record found!";
        }
    }
  return finalResult;
  };
async function _fnRelationalSearch(parameter,queryType){
  var showSelf = parameter.showSelf;
  var searchType = parameter.docType;
  var grouping = parameter.grouping;
  var lang = _.lowerCase(_.get(parameter,"lang","en"));  
  // logger.info("lang = ",lang);
  let queryParams = {};
  let finalResult = [];
  let resultIndex = 0;
  let rpResultSuccess = true;  
  const searchRule = getUserCollectionRule(searchType);
  if(rpResultSuccess){
    let relationConfig = searchRule.relationMappingConfig;
    if(relationConfig){
      let relationType = _.get(parameter,"relationType");
      relationConfig = _.find(relationConfig,_.set({},"relationType",relationType))
      let lookupDesc = _.get(relationConfig,"lookupDesc",[]);
      let sortBy = _.get(relationConfig,"sortBy",[]);
      const relationRule = getUserCollectionRule(relationConfig.relationModel);
      const typeDefindedField = _.get(relationConfig,"typeDefindedField");
      const typeDefindedFieldValue = _.get(relationRule,"type");
      queryParams = {};
      let relateModel = require("../models/"+relationRule.model);
      queryParams[relationConfig.refKey]=parameter.key;
      queryParams[typeDefindedField]=typeDefindedFieldValue;
      logger.debug("match = ",queryParams);
      var lookups = relationRule.lookups;
      // logger.debug("lookups = ",lookups);
      let returnResult = [];
      let mappingKey = _.get(relationConfig,"mappingKey","");
      if(mappingKey.indexOf(".")>=0){
        mappingKey = mappingKey.split(".")[1];
      }
      if(lookups){
          var localField = lookups[0].localField;
          var subLocalField = lookups[0].subLocalField;
          var finalLocalField = localField;
          var foreignField = lookups[0].foreignField;
          if(subLocalField){
            finalLocalField = _.join([localField,subLocalField],".");
          }
          var as = lookups[0].as;
          var lookup = {

                from: lookups[0].from, // 需要关联的表
                localField: finalLocalField, // product 表需要关联的键
                foreignField: foreignField, // orders 的 matching key
                as: as // 对应的外键集合的数据
              
          };
          // logger.debug("lookup = ",lookup);
          logger.debug("queryParams = ",queryParams);
      const newStaffs = await dbHelper.dbHelpAggregate(relateModel,lookup,queryParams).catch(error=>{
          finalResult[resultIndex++]= ""+error;
          rpResultSuccess = false;
        });
        let displayFields = _.get(_.head(lookups),"returnFields",[]);
        // logger.debug("newStaffs = ",newStaffs);
        if(newStaffs.length>0){
          var relationships =_.get(_.head(newStaffs),localField,[]);
          var descriptions =_.get(_.head(newStaffs),as,[]);
          queryParams=[];
            relationships.forEach(relation=>{
              let temp = {};
              temp =_.assign(temp,displayFields.forEach(displayField=>{
                let lable = displayField;               
                if(lable.indexOf("{localField.}")>=0){
                  lable = lable.replace("{localField.}","");
                  temp[lable] =relation[lable];                 
                }else if(lable.indexOf("{as.}")>=0){
                  lable = lable.replace("{as.}","");
                  let role = _.get(relation,subLocalField);
                  let option = {};
                  if(foreignField.indexOf(".")>=0){
                   
                    let optionsKey = foreignField.split(".")[0];
                     let options = _.get(_.head(descriptions),optionsKey);
                     let foreignKey = foreignField.split(".")[1];
                     option = _.find(options,{[foreignKey]:role});
                  }else{
                     option = _.find(options,{[foreignField]:role});
                  }
                  temp[lable] =_.get(_.get(option,lable,""),lang,"");
                }
                 return temp;
              }));
              returnResult = _.concat(returnResult,temp);

              if(relationConfig.showOpposit){               
                 queryParams=_.concat(queryParams, _.get(relation,mappingKey));               
              }
            });
          }else{
            rpResultSuccess = false;
          }
      }else{

        const newStaffs =await dbHelper.searchAllResult(relateModel,queryParams);
        if(newStaffs.length>0){
          queryParams=[];
          var relationships =_.get(_.head(newStaffs),"relationship",[]);
            relationships.forEach(relation=>{
              returnResult = _.concat(returnResult,relation);
              if(relationConfig.showOpposit){               
                 queryParams=_.concat(queryParams, _.get(relation,mappingKey));
              }
            });
          }else{
            finalResult= {success:false,message:"No record found!"};
            rpResultSuccess = false;
          }

      }
      if(showSelf){
        queryParams=_.concat(queryParams, parameter.key);      
      }
      if(rpResultSuccess){
            let queryParamsTmp={};
            let finalRnResult = [];
            if(relationConfig.showOpposit){
              queryParamsTmp[_.get(relationConfig,"oppositKey")] = {$in:queryParams};
              // logger.debug("queryParamsTmp 1111=",queryParamsTmp);
              let modelOpposit = require("../models/"+relationConfig.oppositModel);
              // logger.info("modelOpposit = ",modelOpposit);
              const opposits =await dbHelper.searchAllResult(modelOpposit,queryParamsTmp).catch(error=>{});
              // if(_.get(parameter,"relationalSearchStr")){

              // }
              // logger.info("opposits = ",opposits);
              // logger.info("returnResult = ",returnResult);
              _.remove(opposits,(opposit)=>{return _.get(opposit,"EAB.isDeleted")});
              if(opposits.length>0){
                let lookupDescs = await prepareLookupDesc(lookupDesc);
                // logger.debug("lookupDescs = ",lookupDescs);
                let showOppositFields = _.get(relationConfig,"showOppositFields");
                opposits.forEach(opposit =>{ 
                  // logger.info("opposit = ",opposit);                          
                  let temp = fillValueFromObject(showOppositFields,opposit,_.get(relationConfig,"formatFields"));
                  let opkey = opposit[_.get(relationConfig,"oppositKey")];
                  opkey = _.toString(opkey);
                  let mpkey = _.get(relationConfig,"mappingKey");
                  if(mpkey.indexOf(".")>0){
                    mpkey = _.takeRight(mpkey.split("."));
                    mpkey = _.head(mpkey);
                  }
                  let oppositMappings = _.find(returnResult,_.set({},mpkey,opkey));
                  if(oppositMappings){
                     for(let oppositMapping in oppositMappings){
                       temp[oppositMapping] = oppositMappings[oppositMapping];
                     }
                  }else{
                    temp["self"] = true;
                  }
                  // item = _.assign(item,temp);
                  temp = getLookupDesc(temp,lookupDesc,lookupDescs,"en");
                  finalRnResult = _.concat(finalRnResult,temp);
              });  
              logger.debug("relation search count = ",opposits.length);
              }
              if(showSelf){

              }
            }
          if(sortBy){
            let orderName = [];
            let sortType = [];
            sortBy.forEach(item=>{
              orderName = _.concat(orderName,item.sortName);
              sortType = _.concat(sortType,item.sortType);
            });
            finalRnResult = _.orderBy(finalRnResult,orderName,sortType);
          }
          // logger.debug("finalRnResult = ",finalRnResult);
          if(grouping){
            finalRnResult = _.groupBy(finalRnResult,grouping.groupBy);
            finalRnResult = _(finalRnResult).map((items, iCid) => {
              return {
                groupBy:iCid,
                groupName: _.get(_.head(items),grouping.groupName),
                items: items
              }
            });
          }
          finalResult= {"success":true,result:finalRnResult};
        }
        // else{
        //   finalResult= "No record found!";
        //   rpResultSuccess = false;
        // }
  
    }
}
  return finalResult;
  };
  const getLabel = (key,lang)=>{
    var label = _.get(powerSearchProperties.setting,lang);
    label = _.get(label,key);
    return label;
  };
  /*/ 
  parameters:
  1.JSON路径层次 plans.planCode
  2.要分析的JSON数据源
  3.是否去重复： true 为去重复
  4.过滤条件,没有处理变量的情况，可扩展 :{"fieldName":"planInd","fieldValue":"B"} 
  5.自定义处理函数

  callBack parameter:
  1. 每层节点的值 : 
  2. 每层节点的节点名 : plans,planCode
  3. 每层节点原数据的所有节点值，最后的节点才有值，其他时候为null: plans =>null, planCode才有值

  example for call
  1. values = handleArray(item.fieldName,_.head(allResult),true,null,(item,node,c)=>{return _.set({},node,item)});

  2. handleArray(labelMappingField,j,true,_.set({},labelMappingField,loopItem),(a,b,c)=>{
    if(c){
      let t = _.get(c,labelField);
      _.set(labelCol,a,t);
    }
  });
  /*/
  const handleArray=(root,datasrc,uniq,callBack)=>{
  let array = root.indexOf(".")>0?root.split("."):root;
  if(!_.isArray(array))
    array = [array];
  let first = _.get(datasrc,array[0]);
  // logger.debug("first = ",first);
  let right = _.takeRight(array,array.length-1);
  right = _.join(right,".");
  var tempArray = [];
    if(_.isArray(first)){
      first.forEach(item=>{
        var fResult = handleArray(right,item,uniq,callBack);
        // logger.debug("fResult = ",fResult);
        if(!_.isEmpty(fResult)){
          if(!_.isArray(fResult)){
            fResult = [fResult];
          }
          if(uniq)
            tempArray = _.union(tempArray,fResult);
          else
            tempArray = _.concat(tempArray,fResult);
        }
      });
      // tempArray = _.set({},array[0],tempArray);
      if(callBack){
        tempArray = callBack(tempArray,array[0]);
       }
      return  tempArray;   
    }else{
      if(callBack){
        first = callBack(first,array[0],datasrc);
        // logger.debug("first2 = ",first);
      }
      // if(uniq)
      //       tempArray = _.union(tempArray,first);
      //     else
      //       tempArray = _.concat(tempArray,first);
    return first;
  }
  };
  const handleField= (mappingSetting,item,inValue,mappingItem,lang,self,filter,coreRelationFlag)=>{
    // var inValue = inValue1;
    // var mappingItem = mappingItem1;
    // logger.debug("filter = ",filter,",self = ",self);
    var filteredResult = _.cloneDeep(inValue);
    var removeCoreRelationOpposit = [];
    const displayKey = item.displayKey;
    var selfField ={title:getLabel(displayKey,lang),searchType:displayKey,items:[]};
    // logger.debug("mappingSetting = ",mappingSetting)
    const localField = mappingSetting.localField;
    const localCriteria = mappingSetting.localCriteria;
    const labelField = item.labelField;
    var values = []
    if(!_.isArray(inValue))
      inValue = [inValue];
      // logger.debug("here inValue = ",inValue);
    inValue.forEach(rItem=>{
      // logger.debug("here item = ",item);
      // logger.debug("here rItem = ",rItem);
      // logger.debug("here self = ",self);
      ///////////////////////////////////////////////////
      // logger.debug("here rItem._id = ",_.get(rItem,"_id"));
      // logger.debug("here rItem.EAB.oppositID = ",_.get(rItem,"EAB.oppositID"));
      ////////////////////////////////////////////////////
      var uniqKey = _.get(rItem,"_id");
      // logger.debug("here uniqKey = ",uniqKey);
      var subItem = {};
      var localValue = "";
      if(_.hasIn(rItem,localField)){
        _.set(subItem,"localValue",_.get(rItem,localField));
        localValue = _.get(rItem,localField);
      }

      if(_.hasIn(rItem,item.fieldName)){
        if(filter){
          // logger.debug("here filter = ",filter);
          // logger.debug("here _.get(rItem,item.fieldName) = ",_.get(rItem,item.fieldName));
          var found = _.indexOf(filter,_.get(rItem,item.fieldName));
          // logger.debug("here found 111= ",found);
          if(found<0)
            _.remove(filteredResult,(n)=>{return _.eq(n,rItem) && self});
            values = _.union(values,[{uniqKey:uniqKey,value:_.get(rItem,item.fieldName),label:_.get(rItem,labelField)}]);    
        }
        else
          values = _.union(values,[{value:_.get(rItem,item.fieldName),label:_.get(rItem,labelField)}]);
      }
      else{
        handleArray(item.fieldName,rItem,true,(a,b,c)=>{
          if(c){
            // logger.debug("here a = ",a);
            // logger.debug("here b = ",b);
            // logger.debug("here c = ",c);
            var remove = false;
            if(localCriteria){
              if(!_.isArray(localCriteria)){
                localCriteria = [localCriteria];
              }
              localCriteria.forEach(i=>{
                // logger.debug("here i = ",i);
                // logger.debug("here i.fieldValue = ",i.fieldValue);
                // logger.debug("here _.get(c,i.fieldName) = ",_.get(c,i.fieldName));
                 if(!_.eq(_.toString(_.get(c,i.fieldName)),_.toString(i.fieldValue)))
                    remove = true;
              })
            }
            if(!remove){
              if(filter){
                // logger.debug("here _.get(c,b) = ",_.get(c,b));           
                var found = _.indexOf(filter,_.get(c,b));
                // logger.debug("here found 2222= ",found);
                if(found<0)
                  _.remove(filteredResult,(n)=>_.eq(n,rItem)&& self);
                  values = _.union(values, [{uniqKey:uniqKey,value:_.get(c,b),label:_.get(c,labelField)}]);       
              }else{
                // logger.debug("11111111111 c = ",c,"bbbb = ",b,",labelField = ",labelField);
                values = _.union(values, [{value:_.get(c,b),label:_.get(c,labelField)}]);
              }
            }
          }
        });
      }
    });
      // logger.debug("values = ",values);
    if(!_.isEmpty(mappingItem) && !_.isArray(mappingItem))
        mappingItem = [mappingItem];
    //  logger.debug("mappingSetting = ",mappingSetting);
    const foreignField = mappingSetting.foreignField;
    
    const labelLookupKey = item.labelLookupKey;
    var labelCol = {};
    // logger.debug("mappingItem = ",mappingItem);
    if(!_.isEmpty(mappingItem)){
      mappingItem.forEach(j=>{            
        handleArray(foreignField,j,true,(a,b,c)=>{
            if(c){            
              let t = _.get(c,labelField);
              // logger.debug("a = ",a);
              // logger.debug("b = ",b);
              _.set(labelCol,a,t);
              // logger.debug("labelCol = ",labelCol);
            }
          });
        });
    }
      // logger.debug("labelCol = ",labelCol);
    _.forEach(values,(loopItem,i)=>{
      var label = "";
      var value = loopItem.value;
      // logger.debug("value = ",value);
          if(!_.isEmpty(labelCol)){
            label = _.get(labelCol,value);
            // logger.debug("label = ",label);
            if(filter){
              var t = value;
              if(labelLookupKey){
                t = label;
              }
              var found = _.indexOf(filter,t);
              // logger.debug("here found 333 = ",found);
              
              if(found<0){
                // logger.debug("here found 444 = ",_.get(loopItem,"uniqKey"));
                _.remove(filteredResult,(n)=>{
                  // logger.debug("here found 555 = ",_.toString(_.get(n,"_id")));
                  return _.eq(_.toString(_.get(n,"_id")),_.toString(_.get(loopItem,"uniqKey")))
                });
                if(!self && coreRelationFlag){
                  inValue.forEach(itemTP=>{
                    if(_.eq(_.toString(_.get(itemTP,"_id")),_.toString(_.get(loopItem,"uniqKey")))){
                      removeCoreRelationOpposit = _.union(removeCoreRelationOpposit,[_.get(itemTP,"EAB.oppositID")]);
                    }
                  })
                }
              }    
            }else{
              if(labelLookupKey){
              // if(!self && !coreRelationFlag)
                  value = label;
              // logger.debug("label = ",label);
              // if(labelLookupKey){
                var lookup = getLabel(labelLookupKey,lang);
                if(!_.isArray(lookup))
                  lookup = [lookup];
                label = _.get(_.find(lookup,{key:_.toString(label)}),"value",label)
              }else{
                label = _.get(label,lang,label);
              }
          }
          }else{
            if(loopItem.label && !_.isEmpty(loopItem.label)){
              label = loopItem.label;
            }
            if(filter){
              var found = _.indexOf(filter,value); 
              // logger.debug("found = ",found);
              // logger.debug("value = ",value);
              if(found<0){
                _.remove(filteredResult,(n)=>{
                  // logger.debug("ffffffffffffff ",_.toString(_.get(n,"_id")),"--------",_.get(loopItem,"uniqKey"))
                  return _.eq(_.toString(_.get(n,"_id")),_.toString(_.get(loopItem,"uniqKey")))
                });
                
                if(!self && coreRelationFlag){
                  inValue.forEach(itemTP=>{
                    if(_.eq(_.toString(_.get(itemTP,"_id")),_.toString(_.get(loopItem,"uniqKey")))){
                      removeCoreRelationOpposit = _.union(removeCoreRelationOpposit,[_.get(itemTP,"EAB.oppositID")]);
                    }
                  })
                }
              }
              // logger.debug("filteredResultsssssss = ",_.size(filteredResult));   
            }
          }
      var rtItem = {};
      if(label){
          rtItem = {value:value,label:label};
      }else{
          rtItem = {value:value,label:value};
      }
      if(!_.isEmpty(loopItem)){
        if(label || value)
          selfField.items = _.unionBy(selfField.items,[rtItem],"value")
      }
    })
    // logger.debug("here selfField = ",selfField);
    // selfField = _.set({},displayKey,selfField);
    _.set(selfField,"dispalySeq",item.dispalySeq);
      // logger.debug("selfField = ",selfField);
    if(filter){
      // logger.debug("filteredResult1111 = ",filteredResult);
      if(!self && coreRelationFlag){
        // logger.debug("here removeCoreRelationOpposit = ",removeCoreRelationOpposit);
        return removeCoreRelationOpposit;
      }else{
        return filteredResult;
      }
    }else{
      return selfField
    };
  };
  const getCoreRelationMappingResult = async (coreRelationSetting,allResult)=>{
    var rtResult = {};
    var coreRelationCriteria = [];
   
    var coreRelationMapping = [];
    var goAhead = true;
    for(let i in allResult){
      if(goAhead){
      var coreRleationMappingResult = await getCoreRelatedObject(coreRelationSetting.relationType,_.toString(_.get(allResult[i],"_id")),coreRelationSetting.isCurrent);
        if(_.get(coreRleationMappingResult,"success")){
          var coreRleationResultItems = coreRleationMappingResult.result;

          if(!_.isArray(coreRleationResultItems)){
            coreRleationResultItems = [coreRleationResultItems];
          }                    
          coreRleationResultItems.forEach(item=>{
            if(coreRelationSetting.isCurrent){
              coreRelationCriteria = _.union(coreRelationCriteria,_.get(item,"relationship"));
              coreRelationMapping = _.concat(coreRelationMapping,{_id:_.toString(_.get(allResult[i],"_id")),opposit:_.get(item,"relationship")[0]})
            }
            else{
              coreRelationCriteria = _.union(coreRelationCriteria,[_.get(item,"currentID")]);
              coreRelationMapping = _.concat(coreRelationMapping,{_id:_.toString(_.get(allResult[i],"_id")),opposit:_.get(item,"currentID")})
            }
          });
          
        }else{
          goAhead = false;
        }
      }
    }
    var coreRelationMappedModel = require(`../models/${coreRelationSetting.oppositTable}`);
    var coreRleationResultTMP = await dbHelper.searchAllResult(coreRelationMappedModel,{"_id":{$in:coreRelationCriteria}}).catch(error=>{
      if(error)
        rtResult = {success:false,message:error.message}
    });
    if(_.size(coreRelationMapping)>0 && _.size(coreRleationResultTMP)>0){
      var coreRleationResult = [];
      coreRelationMapping.forEach((item,i)=>{
        coreRleationResultTMP.forEach((itemt,j)=>{
          var _id = _.toString(_.get(item,"_id"))
          var oppositMappedID = _.toString(_.get(item,"opposit"))
          var oppositID = _.toString(_.get(itemt,"_id"))
          var c = _.eq(oppositMappedID,oppositID);
          if(c){
            var temp = _.cloneDeep(itemt)
            _.set(temp,"EAB.oppositID",_id)
            coreRleationResult = _.concat(coreRleationResult,temp);
          }
        })
      })
    }
    if(goAhead)
      rtResult = {success:true,result:coreRleationResult}
    // logger.debug("coreRleationResult.size in getCoreRelationMappingResult : ",coreRleationResult);
    return rtResult;
  }
  const handleCriteria= async (criteriaMappings,allResult,lang,searchCriteria)=>{
    var goAhead = true;
    var filterResult =_.cloneDeep(allResult)
    const selfFieldDef = _.filter(criteriaMappings,{"self":true});
        // logger.debug("_.size(selfFieldDef) = ",_.size(selfFieldDef));
        var fReturn ={};
        var selfFields = [];
        if(_.size(selfFieldDef)>0 && _.size(allResult)>0){
          if(!_.isArray(allResult))
            allResult = [allResult];
          for(let i in selfFieldDef){
            //mapping store in sysMappingTable
            const coreMappingSetting = selfFieldDef[i].coreMappingSetting;
            if(coreMappingSetting){
              const labelMappingType = coreMappingSetting.mappingType;
              var mappingItem  = await dbHelper.dbHelpFindOne(ModelSysMappingTable,{mappingType:labelMappingType});
              // logger.debug("mappingItem111 = ",mappingItem);
              for(let j in selfFieldDef[i].fields){
                // _.set(selfFieldDef[i].fields[j],"")
                if(searchCriteria){
                  var criteria = _.get(searchCriteria,_.get(selfFieldDef[i].fields[j],"displayKey"));
                  if(criteria){
                    if(!_.isArray(criteria))
                      criteria = [criteria]
                    filterResult = handleField(coreMappingSetting,selfFieldDef[i].fields[j],filterResult,{},lang,true,criteria)
                  }                    
                }else{
                  selfFields = _.concat(selfFields,handleField(coreMappingSetting,selfFieldDef[i].fields[j],allResult,mappingItem,lang,true));
                }
              }
            }
            //mapping store in definded join table
            const joinTableSetting = selfFieldDef[i].joinTableSetting;
            if(joinTableSetting){
              const joinModel = require(`../models/${joinTableSetting.joinTable}`);
              var foreignCriteria = {};
              if(joinTableSetting.baseForeignCriteria){
                if(!_.isArray(joinTableSetting.baseForeignCriteria))
                  joinTableSetting.baseForeignCriteria = [joinTableSetting.baseForeignCriteria];
                  joinTableSetting.baseForeignCriteria.forEach(i=>{
                    foreignCriteria[i.fieldName] = i.fieldValue;
                  })
              }
              // logger.debug("foreignCriteria = ",foreignCriteria);
              var joinResult = await dbHelper.searchAllResult(joinModel,foreignCriteria);
              // joinResult = filter(joinTableSetting,allResult,joinResult);
              // logger.debug("joinResult = ",joinResult);
              for(let j in selfFieldDef[i].fields){
                if(searchCriteria){
                  var criteria = _.get(searchCriteria,_.get(selfFieldDef[i].fields[j],"displayKey"));
                  // logger.debug("criteria = ",criteria);
                  if(criteria){
                    if(!_.isArray(criteria))
                      criteria = [criteria]
                    filterResult = handleField(joinTableSetting,selfFieldDef[i].fields[j],filterResult,{},lang,true,criteria)
                  }                 
                }else{
                  selfFields = _.concat(selfFields,handleField(joinTableSetting,selfFieldDef[i].fields[j],allResult,joinResult,lang,true));
                }
              }
            // logger.debug("localValueArray = ",localValueArray);
            }
            if(!coreMappingSetting && !joinTableSetting){
              for(let j in selfFieldDef[i].fields){
                if(searchCriteria){
                  var criteria = _.get(searchCriteria,_.get(selfFieldDef[i].fields[j],"displayKey"));
                  logger.debug("criteria = ",criteria);
                  if(criteria){
                    if(!_.isArray(criteria))
                      criteria = [criteria]
                    filterResult = handleField({},selfFieldDef[i].fields[j],filterResult,{},lang,true,criteria)
                  }                 
                }else{
                  selfFields = _.concat(selfFields,handleField({},selfFieldDef[i].fields[j],allResult,{},lang,true));
                }
              }
            }
          }
        }
        const joinTableFieldsDef =  _.filter(criteriaMappings,{"self":false});
        // logger.debug("joinTableFieldsDef = ",joinTableFieldsDef);
        if(_.size(joinTableFieldsDef)>0 && _.size(allResult)>0){          
          for(let i in joinTableFieldsDef){
            const joinTableSetting = joinTableFieldsDef[i].joinTableSetting;
            const coreRelationSetting = joinTableFieldsDef[i].coreRelationSetting;
            var joinResult = {};
            if(joinTableSetting){
              const joinModel = require(`../models/${joinTableSetting.joinTable}`);
              var foreignCriteria = {};
              if(joinTableSetting.baseForeignCriteria){
                if(!_.isArray(joinTableSetting.baseForeignCriteria))
                  joinTableSetting.baseForeignCriteria = [joinTableSetting.baseForeignCriteria];
                  joinTableSetting.baseForeignCriteria.forEach(i=>{
                    foreignCriteria[i.fieldName] = i.fieldValue;
                  })
              }
              // logger.debug("foreignCriteria = ",foreignCriteria);
              joinResult = await dbHelper.searchAllResult(joinModel,foreignCriteria);
              // logger.debug("joinResult= ",joinResult);              
              if(joinTableSetting.coreRelationSetting){
                // logger.debug("joinTableSetting.coreRelationSetting = ",joinTableSetting.coreRelationSetting);
                var coreRleationResult = [];
                    let temp = await getCoreRelationMappingResult(joinTableSetting.coreRelationSetting,allResult);
                    if(_.get(temp,"success")){
                      coreRleationResult = temp.result;
                    }else{
                      goAhead = false;
                    }               
              }             
            }
            else if(coreRelationSetting){
              let temp = await getCoreRelationMappingResult(coreRelationSetting,allResult);
              if(_.get(temp,"success")){
                coreRleationResult = temp.result;
              }else{
                goAhead = false;
              }
            }
              // logger.debug("coreRleationResult = ",coreRleationResult);
            if(goAhead){                              
              // logger.debug("joinResult = ",joinResult);
              for(let j in joinTableFieldsDef[i].fields){
                // logger.debug("selfFieldDef[i].fields[j] = ",joinTableFieldsDef[i].fields[j]);
                if(searchCriteria){
                  var criteria = _.get(searchCriteria,_.get(joinTableFieldsDef[i].fields[j],"displayKey"));
                  // logger.debug("criteria = ",criteria);
                  if(criteria){
                    if(!_.isArray(criteria))
                      criteria = [criteria]
                    if(joinTableSetting){
                      if(joinTableSetting.coreRelationSetting){
                        ////////////
                        // coreRleationResult.forEach(i=>logger.debug("_id = ",i._id,",EAB.id =",i["EAB"]["oppositID"]));
                        let filterResultTemp = handleField(joinTableSetting,joinTableFieldsDef[i].fields[j],coreRleationResult,joinResult,lang,false,criteria,true)
                        // logger.debug("filterResultTemp = ",filterResultTemp);
                        // logger.debug("filterResult = ",filterResult);
                        if(filterResultTemp && _.size(filterResultTemp)>0){
                          _.remove(filterResult,(n)=>{
                            let index = _.indexOf(filterResultTemp,_.toString(_.get(n,"_id")));
                              if(index>=0){
                                return true;
                              }
                          });
                        }
                        // logger.debug("filterResult = ",filterResult);
                        ////////////
                      }
                      else{
                        filterResult = handleField(joinTableSetting,joinTableFieldsDef[i].fields[j],filterResult,joinResult,lang,false,criteria)
                      }
                    }else if(coreRelationSetting){
                      let filterResultTemp = handleField(coreRelationSetting,joinTableFieldsDef[i].fields[j],coreRleationResult,joinResult,lang,false,criteria,true)
                        if(filterResultTemp && _.size(filterResultTemp)>0){
                          _.remove(filterResult,(n)=>{
                            let index = _.indexOf(filterResultTemp,_.toString(_.get(n,"_id")));
                              if(index>=0){
                                return true;
                              }
                          });
                        }
                    }
                  }
                }else{
                  if(joinTableSetting){
                    if(joinTableSetting.coreRelationSetting){
                      selfFields = _.concat(selfFields,handleField(joinTableSetting,joinTableFieldsDef[i].fields[j],coreRleationResult,joinResult,lang,false,null,true));
                    }
                    else{
                      selfFields = _.concat(selfFields,handleField(joinTableSetting,joinTableFieldsDef[i].fields[j],allResult,joinResult,lang,false,null,false));
                    }
                  }else if(coreRelationSetting){
                    // logger.debug("coreRleationResult = ",coreRleationResult)
                      selfFields = _.concat(selfFields,handleField(coreRelationSetting,joinTableFieldsDef[i].fields[j],coreRleationResult,joinResult,lang,false,null,true));
                  }
                }
              }
            }
            // logger.debug("localValueArray = ",localValueArray);            
          }
        }

        selfFields = _.sortBy(selfFields,["dispalySeq"]);
        _.set(fReturn,"searchCriteriaMapping",selfFields)
        fReturn = {success:true,result:fReturn};
        if(searchCriteria){
          return {success:true,result:filterResult}
        }
        else{
          return fReturn;
      }

  }
  async function fnInitSearchCriteria(searchType,basicCriteria,lang){
    // var rtResult = {};
    const searchConfig = _.get(powerSearchConfig,searchType);
    const criteriaMappings = _.get(searchConfig,"criteriaMapping");
    // const relationshipSearch = _.get(searchConfig,"relationshipSearch");
    // const mainModel = _.get(searchConfig,"mainModel");
    var fReturn ={};
    var allResult = await initBaseResult(searchConfig,basicCriteria);
    if(_.get(allResult,"success")){
        allResult = allResult.result;
        logger.debug("allResult size = ",_.size(allResult));
        fReturn = await handleCriteria(criteriaMappings,allResult,lang);        
        _.set(fReturn,"relationshipSearchKey",basicCriteria)
    }else{
      var msg = _.get(allResult,"message")
      if(msg)
        fReturn = {success:false,message:msg};
    }
    return fReturn;
  };
  function fnInitSearchBar(searchType,lang){
    const searchConfig = _.get(powerSearchConfig,searchType);
    const searchBar = _.get(searchConfig,"searchBar");
    return {"keywordLabel":getLabel(_.get(searchBar,"displayKey",""),lang),"searchBarLabel":getLabel(_.get(searchBar,"searchBarLabelKey",""),lang)};
  };
  function filterKeyword(mainModelResult,indexFieldSetting,searchKey,lang){
    logger.debug("filterKeyword .size before = ",_.size(mainModelResult));
    if(mainModelResult){
      if(!_.isArray(mainModelResult))
        mainModelResult = [mainModelResult];
        var rtResult = [];
      for(let i in indexFieldSetting){
        const fieldName = _.get(indexFieldSetting[i],"fieldName")
        const self = _.get(indexFieldSetting[i],"self");        
        var tempModelResult = _.cloneDeep(mainModelResult);
        // logger.debug("tempModelResult = ",tempModelResult)
        
        _.remove(tempModelResult,(item)=>{
          var uniqKey = _.get(item,"_id");
          var fieldValue = [];
          if(self){
            if(_.hasIn(item,fieldName)){
              fieldValue = _.get(item,fieldName);
            }else{
              handleArray(fieldName,item,false,(a,b,c)=>{
                if(c){
                  // logger.debug("c = ",c)
                  // logger.debug("b = ",b)
                  // logger.debug("a = ",a)
                  var tempValue = _.get(c,b);
                  tempValue = _.get(tempValue,lang,tempValue);
                  fieldValue = _.union(fieldValue,[tempValue]);
                }
              });
            }
            // logger.debug("item = ",item._id)
            // logger.debug("fieldValue = ",fieldValue)
          }
          if(!_.isArray(fieldValue))
            fieldValue = [fieldValue];
          var remove = true;
          _.forEach(fieldValue,(item,i)=>{
            if(_.includes(item,searchKey))
              remove = false; 
          });
          return remove;
        });
        // logger.debug("tempModelResult = ",tempModelResult)
        rtResult = _.union(rtResult,tempModelResult);
      }
    }
    logger.debug("filterKeyword .size after = ",_.size(rtResult));
    return rtResult;
  };
  async function filterCriteria(mainModelResult,criteriaMappings,searchParam,lang){
    return await handleCriteria(criteriaMappings,mainModelResult,lang,searchParam);
  };
  async function initBaseResult(searchConfig,basicCriteria){
    const relationshipSearch = _.get(searchConfig,"relationshipSearch");
    const mainModel = _.get(searchConfig,"mainModel");
    var allResult = {};
    var rtResult = {};
    if(relationshipSearch){
      _.set(relationshipSearch,"key",basicCriteria);
      allResult = await _fnRelationalSearch(relationshipSearch,null).catch(error=>{
        if(error)
          rtResult = {success:false,message:error.message}
      });
      if(_.get(allResult,"success"))
        allResult = allResult.result;
      
    }else{
      const model = require(`../models/${mainModel}`);
      allResult = await dbHelper.searchAllResult(model,{}).catch(error=>{
        if(error)
          rtResult = {success:false,message:error.message}
      });
    }
    // logger.debug("rtResult = ",rtResult);
    if(!_.isEmpty(rtResult))
      return rtResult;
    else
      return {success:true,result:allResult};
  };
  async function fnPowerSearchResult(searchType,basicCriteria,searchParam,lang){
    const searchConfig = _.get(powerSearchConfig,searchType);
    const criteriaMappings = _.get(searchConfig,"criteriaMapping");    
    var allResult = await initBaseResult(searchConfig,basicCriteria);
    // logger.debug("allResult size = ",allResult);
    logger.debug("basicCriteria = ",basicCriteria);
    if(_.get(allResult,"success")){
      allResult = allResult.result;
    logger.debug("size of allResult before filter= ",_.size(allResult));
    //  logger.debug("criteriaMappings = ",criteriaMappings);
    const searchBarIndexField = _.get(searchConfig,"searchBar.indexTableField");
    const searchKey = _.get(searchParam,"searchKey")

    if(!_.isEmpty(searchKey) && !_.isEmpty(searchBarIndexField)){
      if(!_.isArray(searchBarIndexField))
         searchBarIndexField = [searchBarIndexField];
      allResult = filterKeyword(allResult,searchBarIndexField,searchKey,lang)
    }
    logger.debug("size of allResult after filterKeyword filter= ",_.size(allResult));
      if(!_.isEmpty(criteriaMappings) && !_.isEmpty(searchParam)){
        // logger.debug("searchParam = ",searchParam);
        // allResult = await filterCriteria(allResult,criteriaMappings,searchParam,lang);
        allResult = await handleCriteria(criteriaMappings,allResult,lang,searchParam)
        allResult = allResult.result;
      }
      logger.debug("size of allResult after filterCriteria filter= ",_.size(allResult));

      const convertDataMethod = _.get(searchConfig,"convertDataMethod");
      if(convertDataMethod && _.size(allResult)>0){
        const path =  _.get(convertDataMethod,"patch");
        const method =  _.get(convertDataMethod,"method");
        let caller = require(path);
        allResult = await caller[method](allResult,basicCriteria,lang);
      }
    }else{
      var msg =_.get(allResult,"message");
      if(msg)
        return {success:false,message:msg}
    }
    return {success:true,result:allResult}
  }
exports.api = {
  initSearchCriteria: async function (req, res, next) {
    const searchType = _.get(req.query,"docType",_.get(req.body,"docType"));
    const relationshipSearchKey = _.get(req.query,"relationshipSearchKey",_.get(req.body,"relationshipSearchKey"));
    const lang = _.get(req.query,"lang",_.get(req.body,"lang"),"en");
    // logger.debug("basicCriteria = ",basicCriteria);
    var criteriaMappings = await fnInitSearchCriteria(searchType,relationshipSearchKey,lang);
    if(_.get(criteriaMappings,"success"))
      res.json({"success":true,result:criteriaMappings.result});
    else
      res.json({"success":false,message:criteriaMappings.message});
  },
  initSearchBar: async function (req, res, next) {
    const searchType = _.get(req.query,"docType",_.get(req.body,"docType"));
    const lang = _.get(req.query,"lang",_.get(req.body,"lang"),"en");
    // logger.debug("basicCriteria = ",basicCriteria);
    var searchBarLabel = fnInitSearchBar(searchType,lang);
    res.json({"success":true,result:searchBarLabel});
  },
  powerSearchResult: async function (req, res, next) {
    const searchType = _.get(req.query,"docType",_.get(req.body,"docType"));
    const relationshipSearchKey = _.get(req.query,"relationshipSearchKey",_.get(req.body,"relationshipSearchKey"));
    const searchKey = _.get(req.query,"searchKey",_.get(req.body,"searchKey"));
    const searchParam = _.get(req.query,"searchParam",_.get(req.body,"searchParam"));
    _.set(searchParam,"searchKey",searchKey);
    const lang = _.get(req.query,"lang",_.get(req.body,"lang","en"));
    logger.error("searchParam = ",searchParam,",lang = ",lang,",searchType = ",searchType,",searchKey = ",searchKey);
    var searchResult = await fnPowerSearchResult(searchType,relationshipSearchKey,searchParam,lang);
     


    if(_.get(searchResult,"success"))
      res.json({"success":true,result:searchResult.result});
    else
      res.json({"success":false,message:searchResult.message});
  }
};