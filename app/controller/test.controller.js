/**
 * @module controller/test
 */

/**
 * @author Kevin Liang <kevin.liang@eabsystems.com>
 * @function
 * @description Test 1
 * @param {object} req - express request param
 * @param {object} res - express response param
 * @returns {string} test1
 */
const test1 = (req, res) => {
  res.send('test1');
};

/**
 * @author Kevin Liang <kevin.liang@eabsystems.com>
 * @function
 * @description Test 2
 * @param {object} req - express request param
 * @param {object} res - express response param
 * @returns {string} test2
 */
const test2 = (req, res) => {
  res.send('test2');
};

export default {
  test1,
  test2,
};
