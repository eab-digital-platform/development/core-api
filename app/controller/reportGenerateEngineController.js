var ModelUserProfile = require('../models/ModelUserProfile');

const dbHelper = require('../utils/dbHelper');
const _ = require('lodash');


exports.api = {
generatePDF:async function(req, res, next){
let vContinue = true;
const quoteId = req.params.quoteId;

var errMessage = "";
// var createString = {"EAB.applicationStatus":"S"};
// logger.debug("updateStr = ",updateStr);
const result = await searchAPI.fnCreate("eApplication",{}).catch(error=>{
    vContinue = false;errMessage = error.message;
});
logger.debug("eapp = ",result)
var returnJson = {};
if(vContinue && result){
    let mappingParameter = {"relationType":"quoteEAppMapping"};
    const eAppId = _.toString(result.result._id);
    let relationShip = {"oppositID":eAppId};
    let data = {"currentID":quoteId};
    data["relationShip"]=relationShip;
    mappingParameter["data"]=data;
    _.set(returnJson,"eAppDocId",eAppId);
    _.set(returnJson,"eAppId",result.result.EAB.eAppId);
    logger.debug("mappingParameter = ",mappingParameter)
    const reletionResult = await searchAPI.fnRelationCreate("quotation",mappingParameter).catch(error=>{
    vContinue = false;errMessage = error.message;
    });
    logger.debug("reletionResult = ",reletionResult)
    }else{
    vContinue = false;
    }
if(vContinue){
    res.json({"success":true,result:returnJson});
}else{
    logger.error("/pos-api/createEapp error:",errMessage);
    res.json({"success":false,message:"Interal Server Error!"});
}
}
}