import axios from 'axios';
import logger from '../loaders/logger';

const get = require('lodash/get');

const sendInfo2Quotation = async (req, res) => {
  logger.info('===sendInfo2Quotation传递过来的body===', req.body);
  const returnFromQuotation = await axios({
    method: 'post',
    url: 'http://www.quotation.com',
    data: req.body,
    headers: {
      'Content-Type': 'application/json',
    },
  });

  if (returnFromQuotation) {
    const returnStatus = get(returnFromQuotation, 'success', false);
    if (returnStatus) {
      res.json({ success: true, message: '', data: returnFromQuotation });
    } else {
      res.json({ success: false, message: 'the status is fail from quotation api' });
    }
  } else {
    res.json({ success: false, message: 'there is no return data from quotation api' });
  }
};

export default {
  api: {
    sendInfo2Quotation,
  },
};
