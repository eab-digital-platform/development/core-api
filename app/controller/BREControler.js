import logger from '../loaders/logger';
var ModelBRERule = require('../models/ModelBRERule');

const dbHelper = require('../utils/dbHelper');
const _ = require('lodash');

const recursiveFunction = function(object){

}
exports.api = {
  lookup:function(req, res, next){
    // logger.info("in BRE lookup ");
    let lookupType = _.get(req.query,"lookupType",_.get(req.body,"lookupType"));
    let lookupCriteria = _.get(req.query,"eq",_.get(req.body,"eq",{}));
    let occuranceCheck = _.get(req.query,"occuranceCheck",_.get(req.body,"occuranceCheck",{}));
    let mustExists = _.get(req.query,"mustExist",_.get(req.body,"mustExist",{}));
    // logger.info("occuranceCheck = ",occuranceCheck);
    let occuranceFilter = _.get(req.query,"occuranceFilter",_.get(req.body,"occuranceFilter",true));
    let queryParams = _.set({},"ruleType",lookupType);
    let queryAndOrParams= {};
    dbHelper.dbHelpFindOne(ModelBRERule,queryParams,queryAndOrParams,
        { },
        {}).then((result) => {
      if(!_.isEmpty(result)){
        let options = _.get(result,"options",[]);
        logger.info("in BRE lookup before filter=>options.length = ",options.length);
        for(let criteria in lookupCriteria){
          let oValue ="";
          if(_.has(options,criteria)){
          if(criteria.indexOf(".")>=0){
            // let criteriaTmp = criteria.split(".");
            // let objectTmp = _.head(options);
            // criteriaTmp.forEach(node=>{
            //   objectTmp = _.get(objectTmp,node);
            //   if(_.isArray(objectTmp)){
            //     objectTmp.forEach(i =>{
            //       objectTmp =_.get(i,node);
            //       for(let j in i){
            //         logger.info(""+j,i[j]);
            //       }                  
            //     });
            //   }else{

            //   }
            // });
            
          }else{
            _.remove(options,o=>{
                return !(_.eq(_.get(o,criteria),lookupCriteria[criteria]));
            });
          }
        }else{
          logger.info("no property ",criteria);
        }
        }
        logger.info("in BRE lookup after filter eq=>options.length = ",options.length);
          
          for(let mustExist in mustExists){
            
            if(mustExist.indexOf(".")){
              let ind = 0;
              let genderExist = _.set({},mustExist.split(".")[1],mustExists[mustExist]);
              _.remove(options,o=>{
                let opposit = _.get(o,mustExist.split(".")[0]);
                // logger.info("opposit = ",opposit);
                let exist = false;
                if(_.isArray(opposit)){
                  if(_.find(opposit,genderExist)){
                        exist = true;
                        return;
                      }
                  // opposit.forEach(i =>{
                  //   // logger.info("must have = ",i);
                  //   // logger.info("mustExists[mustExist] = ",mustExists[mustExist]);
                  //   // if(_.includes(i,"PGD")){
                      
                  //     // logger.info("genderExist = ",genderExist);
                  //     logger.info("[i] = ",[i]);
                  //   if(_.find([i],genderExist)){
                  //     exist = true;
                  //     return;
                  //   }
                  // });
                }else{
                  if(_.includes(opposit,mustExists[mustExist])){
                    exist = true;
                    return;
                  }
                }
                logger.info("exist = ",exist);
                return !exist;
            });
            }else{

            }
            // _.remove(options,o=>{
            //     return (_.eq(_.get(o,criteria),lookupCriteria[criteria]));
            // });
          }

        logger.info("in BRE lookup after filter mustExist=>options.length = ",options.length);
        _.remove(options,o=>{
            let overLimit = false;
            for(let occuranceCheckItem in occuranceCheck){
                if(_.eq(_.get(o,"value"),occuranceCheckItem) && _.gte(occuranceCheck[occuranceCheckItem],_.get(o,"occurance"))){
                    overLimit = true;
                }          
                if(!occuranceFilter){
                    _.set(o,"overLimit",overLimit);
                }                
            }
        // return _.eq(_.get(o,"value"),occuranceCheckItem) && _.gte(occuranceCheck[occuranceCheckItem],_.get(o,"occurance")) && occuranceFilter;
            return overLimit && occuranceFilter;
            });
        logger.info("in BRE lookup after filter occuranceCheck=>options.length = ",options.length);
        res.json({ success: true, lookup: options });
      }else{
         res.json({ success: false, message: "No lookup found!!"});
      }
    
      })
        .catch((error) => {
          res.json({ success: false,message: ''+ error});
        });
  }
  
  

}