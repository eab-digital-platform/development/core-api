import POSConfig from '../../config/POSConfig';
import userConfig from '../../config/userConfig';
import logger from '../loaders/logger';
import mongoose from 'mongoose';
import axios from 'axios';
var ModelSeqMst = require('../models/ModelSeqMst');
const _ = require('lodash');
const dbHelper = require('../utils/dbHelper');
// function getSystemCollectionRule(searchType){
//   //need to be implement
//   return systemConfig.collectionRules[searchType];
// }
exports.api = {
//clientSearchExcludeFamilyMember
 clientSearchExcludeFamilyMember: async function (req, res, next) {
  if (req.query.docType || req.body.docType) {
    let queryType = "";
    let parameter = {};
    if(req.query.docType){
       queryType ="get";
       parameter = req.query;
    }
    else{
        queryType ="post";
        parameter = req.body;
    }
    parameter = _.set(parameter,"relationType","familyMember");
      let familyMember = {};
      await axios({
        method: "get",
        url: `${POSConfig["core-api"]}/relationalSearch`,
        data: parameter,
        headers: {
            "Content-Type": "application/json"
        }
    })
    .then((response) => {
        // resolve(response.data);
        familyMember = response.data;
    })
    .catch((error) => {
        console.log("error: ", error);
        // resolve(false);
    });
    let success = _.get(familyMember,"success",false);
    logger.info("familyMember = ",familyMember);  
    let notInArray = [];  
    if(success){      
      familyMember["result"].forEach(opposit=>{
                notInArray=_.concat(notInArray,opposit["oppositID"]);
      });
      }
      notInArray=_.concat(notInArray,_.get(parameter,"key",""));
      logger.info("notInArray",notInArray);         
      _.set(parameter,"notInArray",notInArray);
      
      await axios({
      method: "get",
      url: `${POSConfig["core-api"]}/search`,
      data: parameter,
      headers: {
          "Content-Type": "application/json"
      }
      })
      .then((response) => {
          // resolve(response.data);
          familyMember = response.data;
          if(_.get(response.data,"success",false)){
            res.json({success:true, result: response.data["result"] });
          }else{
            res.json({success:false, message: response.data });
          }
      })
      .catch((error) => {
          console.log("error: ", error);
          res.json({success:false, message: ''+error });
      });
            //  await _fnSearch(parameter,queryType).then(vClients=>{
            //    if(_.get(vClients,"success"))
            //       res.json({success:true, result: vClients["result"] });
            //     else
            //       res.json({success:false, message: vClients });
            //  });
    // }else{
    //   res.json({ success: false, result: familyMember["message"]});
    // }    
}else{
  res.json({ success: false,message: "required parameter:docType Missed"});
}
},
 getAvailableFamilyRoleList:async function(req, res, next){
    let docID = _.get(req.query,"docID",_.get(req.body,"docID",req.params.docID));
    let occuranceFilter = _.get(req.query,"occuranceFilter",_.get(req.body,"occuranceFilter",true));
    let oppositGender = _.get(req.query,"oppositGender",_.get(req.body,"oppositGender",{}));
    // logger.info("docID = ",docID);
    let queryType = "post";
    let parameter = {};
    _.set(parameter,"docType","clientProfile");
    _.set(parameter,"key",docID);
    _.set(parameter,"showRelation",true);
    _.set(parameter,"searchStr",_.set({},"_id",docID));
    // logger.info("in getAvailableFamilyRoleList");
    let gender = "";
    // logger.info("parameter",parameter);
      let result ={};
     await axios({
      method: "get",
      url: `${POSConfig["core-api"]}/search`,
      data: parameter,
      headers: {
          "Content-Type": "application/json"
      }
      })
      .then((response) => {
          // resolve(response.data);
          result = response.data;
          gender =_.get(_.get(_.head(result["result"]),"CAFE"),"gender","");
      })
      .catch((error) => {
          console.log("error: ", error);
          res.json({success:false, message: ''+error });
      });
     let occurance = {};
         if(_.get(result,"success",false)){
            occurance = _.countBy(_.get(_.head(result["result"]),"familyMember",[]),"oppositRole");
         }
         logger.info("in getAvailableFamilyRoleList==>occurance check list = ",occurance);
         let queryString = {"occuranceFilter":occuranceFilter,"lookupType":"availableFamilyRole","occuranceCheck":occurance};
         if(!_.isEmpty(oppositGender)){
            _.set(queryString,"eq",_.set({},"gender",oppositGender));
         }
         _.set(queryString,"mustExist",{"opposite.gender":gender});
         logger.info("in getAvailableFamilyRoleList==>queryString",queryString);
    await axios({
      method: "get",
      url: `${POSConfig["BREngine"]}/lookup`,
      data: queryString,
      headers: {
          "Content-Type": "application/json"
      }
      })
      .then((response) => {
          result = response.data;
      })
      .catch((error) => {
          res.json({success:false, message: ''+error });
      });
      if(result["success"])
        res.json({success:true, result: result["lookup"] });
      else
        res.json({success:false, message: "No Data!!!" });


}  
};