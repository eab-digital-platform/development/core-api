import logger from '../loaders/logger';
import POSConfig from '../../config/POSConfig';
import axios from 'axios';
import request from 'request-promise';
const ModelSysMappingTable = require("../models/ModelSysMappingTable");
const _ = require('lodash');
const fs = require('fs');
const dbHelper = require('../utils/dbHelper');
const getSignedPdfFromEsignature = async (signatureParam)=>{
    const signedForm = await axios({
        method: "get",
        url: `${POSConfig["eSignatureBackend"]}/loadSignedPDF`,
        params: signatureParam,
        headers: {
            "Content-Type": "application/pdf; charset=utf-8"
        }
        }).catch(error=>{ vContinue = false;logger.debug("error = ",error.message);});
    if(!signedForm.data.Status){
        return signedForm.data;
        // return fileStream;
    }else{
        return null;
    }};
export {
    getSignedPdfFromEsignature
};