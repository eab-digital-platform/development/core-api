import POSConfig from '../../config/POSConfig';
import eSignatureRules from '../../config/eSignatureRules';
import logger from '../loaders/logger';
import axios from 'axios';
import fs from 'fs';
import mongoose from 'mongoose';
const path = require('path');
// import axios from 'axios';
// const ModelClientProfile = require('../models/ModelClientProfile');
// const ModelFNA = require('../models/ModelFNA');
const _ = require('lodash');
const dbHelper = require('../utils/dbHelper');
const searchAPI = require('./searchEngineController');
const ModelQuote = require('../models/ModelQuote');
const ModelClientProfile = require('../models/ModelClientProfile');
const ModelProductList = require('../models/ModelProductList');
const ModelApplication = require('../models/ModelApplication');
const eAppFunction = require('../biz/eApp/eAppFunction');
const utils = require('../utils/utils');
const eSignatureController = require('./eSignatureController');
// const { Readable } = require('stream');
const updateSignedStatus = async (signatureType, docId) => {
  var updateStr = {};
  if (_.eq(signatureType, "agent")) {
    updateStr = { "SIGNATURE.$.agentSigned": true };
  } else if (_.eq(signatureType, "client")) {
    updateStr = { "SIGNATURE.$.cleintSigned": true };
  }
  // logger.debug("updateStr = ",updateStr);
  const updateResult = await dbHelper.dbHelpUpdate(ModelApplication, { "_id": docId, "SIGNATURE.type": "eAppForm" }, updateStr);
  // logger.debug("updateResult = ",updateResult)
  if (_.eq(updateResult.ok, 1)) {
    return true;
  } else {
    return false;
  }
};
exports.api = {
  getEappListByClient: async function (req, res, next) {
    const lang = "en";
    let clientId = req.params.clientId;
    let errorMessage = "";
    let vContinue = true;
    let relationalSearchStr = _.get(req.body, "relationalSearchStr", _.get(req.query, "relationalSearchStr", {}));
    let relationalSearchKey = _.get(req.body, "relationalSearchKey", _.get(req.query, "relationalSearchKey", {}));
    // logger.debug("relationalSearchStr = ",relationalSearchStr)
    // logger.debug("relationalSearchKey = ",relationalSearchKey)
    let relationSearchParameter = { "docType": "clientProfile", "relationType": "clienteAppMapping", "key": clientId, "relationalSearchStr": relationalSearchStr, "relationalSearchKey": relationalSearchKey };
    if (clientId) {
      let eapps = await searchAPI._fnRelationalSearch(relationSearchParameter, null).catch(error => {
        errorMessage = error;
        vContinue = false;
      });
      // logger.debug("eapps = ",eapps)
      if (_.get(eapps, "success")) {
        eapps = eapps["result"];

        for (let i in eapps) {
          // logger.debug("item = ",eapps[item])
          // const relationQueryStr = {$elemMatch:{oppositID:item._id}};
          if (_.eq(_.get(eapps[i], "EAB.applicationStatus", ""), "A")) {
            _.set(eapps[i], "applyShow", true);
            _.set(eapps[i], "order", 1);
          }
          if (_.eq(_.get(eapps[i], "EAB.applicationStatus", ""), "S")) {
            _.set(eapps[i], "signedShow", true);
            _.set(eapps[i], "order", 2);
          }
          if (_.eq(_.get(eapps[i], "EAB.applicationStatus", ""), "C")) {
            _.set(eapps[i], "submitShow", true);
            _.set(eapps[i], "order", 3);
          }
          if (_.eq(_.get(eapps[i], "EAB.applicationStatus", ""), "I")) {
            _.set(eapps[i], "invalidShow", true);
            _.set(eapps[i], "invalidReason", "The proposal is deleted");
            _.set(eapps[i], "order", 4);
          }

          if (_.eq(_.get(eapps[i], "EAB.applicationStatus", ""), "I") || _.eq(_.get(eapps[i], "EAB.applicationStatus", ""), "C")) {
            _.set(eapps[i], "viewEappShow", true);
          }
          const failFnaExist = false;
          if (_.eq(_.get(eapps[i], "EAB.applicationStatus", ""), "A") && !failFnaExist) {
            _.set(eapps[i], "continueEappShow", true);
          }
          const fnaEapp = await searchAPI.getRelatedObject("fnaEapp", _.toString(eapps[i]._id), false).catch(error => { vContinue = false; errorMessage = error.message });
          // logger.debug("11111111111",fnaEapp);
          if (!_.isEmpty(fnaEapp)) {
            _.set(eapps[i], "fnaLinkShow", true);
            var fnaDocId = _.get(_.head(fnaEapp), "currentID");
            _.set(eapps[i], "fna._id", fnaDocId);

          }
          const relatoinMapping = await searchAPI.getRelatedObject("quoteEapp", _.toString(eapps[i]._id), false).catch(error => { vContinue = false; errorMessage = error.message });
          // logger.debug("relatoinMapping = ",relatoinMapping)
          if (!_.isEmpty(relatoinMapping)) {
            const quoteId = _.head(relatoinMapping)["currentID"];
            // logger.debug("quoteId = ",quoteId)
            const queryStr = { docType: "quotation", searchStr: { _id: quoteId } };
            var quote = await searchAPI.fnSearch(queryStr, "post");
            // logger.debug("quote = ",quote)
            // logger.debug("eapps[i] = ",eapps[i]);
            quote = _.head(quote.result);
            const basicPlan = _.find(quote.plans, { "planInd": "B" });
            _.set(quote, "basicCovCode", basicPlan.covCode);
            _.set(quote, "basicPlanCode", basicPlan.planCode);
            _.set(quote, "basicCovName", basicPlan.covName[lang]);
            // logger.debug("quote = ",quote)
            _.set(eapps[i], "quote", quote);
            // logger.debug("eapps[i] = ",eapps[i]);
          }
        }
        _.orderBy(eapps, ["order", "updateDate"], ["desc", "desc"]);
        _.remove(eapps, o => {

          const status = _.get(o, "EAB.status");
          const fnaOptional = true;
          const fnaLink = false;
          const fnaLinkWithPass = true;
          const fnaMandatory = true;
          // logger.debug("startEappDisplay = ",true);
          if (_.eq(status, "Q") && ((fnaOptional && !fnaLink) || fnaLinkWithPass)) {
            //  logger.debug("startEappDisplay = ",true);
            _.set(o, "startEappDisplay", true);
          }
          if (_.eq(status, "A") && ((fnaOptional && !fnaLink) || (fnaLinkWithPass && fnaMandatory))) {
            _.set(o, "continueEappDisplay", true);
          }
          if (_.eq(status, "S")) {
            _.set(o, "viewEappDisplay", true);
          }
          return _.get(o, "EAB.isDeleted");
        });
      }
      else
        errorMessage = _.get(eapps, "message");
      if (errorMessage) {
        res.json({ "success": false, message: errorMessage });
      } else {
        res.json({ "success": true, result: eapps });
      }
    } else {
      res.json({ "success": false, message: "Parameter clientId missing!" });
    }
  },
  createClientEapp: async function (req, res, next) {
    let cpId = req.params.cpId;
    let eAppId = req.params.eAppId;
    let errorMessage = "";
    let resResult = "";
    if (eAppId && cpId) {
      let mappingParameter = { "relationType": "clienteAppMapping" };
      let relationShip = { "oppositID": eAppId };
      let data = { "currentID": cpId };
      data["relationShip"] = relationShip;
      mappingParameter["data"] = data;

      await searchAPI.fnRelationCreate("clientProfile", mappingParameter).then((result) => {
        if (result["success"]) {
          resResult = result.result;
        } else {
          errorMessage = result;
        }
      }).catch(error => { errorMessage = error.message });
      if (errorMessage)
        res.json({ "success": false, message: "" + errorMessage });
      else
        res.json({ "success": true, result: resResult });
    } else {
      res.json({ "success": false, message: "Parameter fnaFormId/quoteId missing!" });
    }

  },
  deleteClientEapp: async function (req, res, next) {
    let cpId = req.params.cpId;
    let eAppId = req.params.eAppId;
    let errorMessage = "";
    let resResult = "";
    if (eAppId && cpId) {
      let mappingParameter = { "relationType": "clienteAppMapping", "actionType": "delete" };
      let relationShip = { "oppositID": eAppId };
      let data = { "currentID": cpId };
      data["relationShip"] = relationShip;
      mappingParameter["data"] = data;

      await searchAPI.fnRelationCreate("clientProfile", mappingParameter).then((result) => {
        if (result["success"]) {
          resResult = result.result;
        } else {
          errorMessage = result;
        }
      }).catch(error => { errorMessage = error.message });
      if (errorMessage)
        res.json({ "success": false, message: "" + errorMessage });
      else
        res.json({ "success": true, result: resResult });
    } else {
      res.json({ "success": false, message: "Parameter fnaFormId/quoteId missing!" });
    }

  },
  markDeleteEapp: async function (req, res, next) {
    let eAppId = req.params.eAppId;
    var deleteQuote = await eAppFunction.invalidateEapplication(eAppId, "01")
    if (deleteQuote) {
      res.json({
        "success": true, result: {
          "n": 1,
          "nModified": 1,
          "ok": 1
        }
      });
    }
    else {
      res.json({ "success": false, message: "Internal server error" });
    }
  },
  getAvailableProductList: async function (req, res, next) {
    let vContinue = true;
    let clientId = req.params.clientId;
    if (clientId) {
      const client = await dbHelper.dbHelpFindOne(ModelClientProfile, { "_id": clientId }).catch(error => { vContinue = false });
      const productList = await dbHelper.dbHelpFindOne(ModelProductList, {}).catch(error => { vContinue = false });
      if (vContinue) {
        let availableProductList = quoteFunction.getAvailableProductList(client, productList)
        res.json({ "success": true, result: availableProductList });
      } else {
        res.json({ "success": false, message: "Internal server error!" });
      }
    } else {
      res.json({ "success": false, message: "Parameter clientId missing!" });
    }
  },
  get_doc_list: async function (req, res, next) {
    let vContinue = true;
    let eAppId = req.params.eAppId;
    let docList = await eAppFunction.getDocListForSignature(eAppId);
    res.json({ "success": true, result: docList });

  },
  get_doc_list_for_upload: async function (req, res, next) {
    let vContinue = true;
    let eAppId = req.params.eAppId;
    let docList = await eAppFunction.getDocListForUpload(eAppId);
    res.json({ "success": true, result: docList });
  },
  getUploadedDocument: async function (req, res, next) {
    let vContinue = true;
    var errorMessage = "";
    let eAppId = req.params.eAppId;
    let docType = req.params.docType;
    let docOwnerId = req.params.docOwnerId;
    const result = await dbHelper.dbHelpFindOne(ModelApplication, { "_id": eAppId }).catch(error => { errorMessage = error.message; vContinue = false; });
    if (vContinue && result) {
      var uploadedDocument = _.get(result, "UPLOAD_DOCUMENTS", []);
      // logger.debug("uploadedDocument===1", uploadedDocument);
      const filter = { "docType": docType, "docOwnerId": docOwnerId };
      // logger.debug("filter===2", filter);
      // uploadedDocument = _.takeWhile(uploadedDocument,(o)=>{
      //   return _.eq(o.docType,docType)&&_.eq(o.docOwnerId,docOwnerId)});

      _.remove(uploadedDocument, (o) => {
        return !_.eq(o.docType, docType) || !_.eq(o.docOwnerId, docOwnerId)
      })
      _.each(uploadedDocument, (item) => { _.set(item, "eAppDocId", eAppId) });
      res.json({ "success": true, result: uploadedDocument });
    } else {
      res.json({ "success": true, result: [] });
    }
  },
  uploadDocument: async function (req, res, next) {
    let vContinue = false;
    var multiparty = require('multiparty');
    var form = new multiparty.Form({ uploadDir: path.join(__dirname,'../dgpuploadfiles') });
    form.encoding = 'utf-8';
    // form.uploadDir = fileDir;
    form.maxFilesSize = 10 * 1024 * 1024;
    var errorMessage = "";
    var docId = []
    var docType = []
    var docOwernId = []
    var fileArray = [];
    form.parse(req, function (error, fields, files) {
      docId = _.get(fields, "docId");
      docType = _.get(fields, "docType");
      docOwernId = _.get(fields, "docOwernId");
      // logger.debug("docId===", docId);
      // logger.debug("docType===", docType);
      // logger.debug("docOwernId===", docOwernId);
      // logger.debug("files.uploadFile===", files.uploadFile);
      if (error) {
        errorMessage = error.message;
      }
      else {
        fileArray = files.uploadFile;
      }
      for (let i in fileArray) {
        // logger.debug("origfilename===",fileArray[i].originalFilename);
        const fileSuffix = fileArray[i].originalFilename.split(".")[1];
        const newFileName = _.join([_.join([_.head(docType), _.head(docOwernId), _.now()], "_"), fileSuffix], ".");
        let data = fs.readFileSync(fileArray[i].path);
        vContinue = utils.saveFileToFileSystem(newFileName, data).catch(error => { errorMessage = error.message; vContinue = false; });

        const upldStr = {
          docType: _.head(docType),
          docOwnerId: _.head(docOwernId),
          fileName: newFileName
        };
        const updKey = { "_id": _.head(docId) };
        dbHelper.dbHelpFindOneAndAddToSet(ModelApplication, updKey, _.set({}, "UPLOAD_DOCUMENTS", upldStr)).catch(error => { errorMessage = error.message; vContinue = false; });
      }

      if (!vContinue) {
        logger.error(errorMessage);
        res.json({ "success": false, message: "Interal Server Error!" });
      }
      else
        res.json({ "success": true, result: "Uploaded!" });
    });
  },
  deleteDocument: async function (req, res, next) {
    let vContinue = true;
    var errorMessage = "";
    var docType = req.params.docType;
    var docOwernId = req.params.docOwnerId;
    var fileName = req.params.fileName;
    var eAppId = req.params.eAppId;
    const upldStr = {
      docType: docType,
      docOwnerId: docOwernId,
      fileName: fileName
    };
    const updKey = { "_id": eAppId };
    let result = await dbHelper.dbHelpFindOneAndPull(ModelApplication, updKey, _.set({}, "UPLOAD_DOCUMENTS", upldStr)).catch(error => { errorMessage = error.message; vContinue = false; });
    logger.debug("result = ", result);
    if (vContinue) {
      if (_.eq(_.get(result, "nModified"), 1))
        res.json({ "success": true, result: "Removed!" });
      else
        res.json({ "success": true, result: "0 record Removed!" });
    } else {
      logger.error(errorMessage);
      res.json({ "success": false, message: "Interal Server Error!" });
    }
  },
  startSignature: async function (req, res, next) {
    let vContinue = true;
    const docType = req.params.docType;
    const docId = req.params.docId;
    const colRule = searchAPI.getUserCollectionRule(docType);
    var errMessage = "";
    var rtJson = {};
    if (colRule) {
      const model = require('../models/' + _.get(colRule, "model"));
      const doc = await dbHelper.dbHelpFindOne(model, { "_id": docId }).catch(error => { vContinue = false });
      if (doc) {
        const fileName = _.get(doc, "EAB.formFileName", "");

        var file = await utils.getFileFromFileSystem(fileName);////to be do
        file = await utils.fileToBase64(file);

        if (file) {
          // const signatureRule = eSignatureRules[docType];
          var signatureParam = _.get(eSignatureRules, docType);
          _.set(signatureParam, "docID", _.head(_.split(fileName, ".")));
          _.set(signatureParam, "localization", "EN");
          _.set(signatureParam, "binary", file);
          // logger.debug("signatureParam = ",signatureParam);
          rtJson = await axios({
            method: "post",
            url: `${POSConfig["eSignatureBackend"]}/session_request`,
            data: signatureParam,
            headers: {
              "Content-Type": "application/json"
            }
          })
          // logger.debug("eSignature return = ",rtJson)
          // .then((response) => {
          //     logger.debug("startSignature = ",response.data)
          // })
          // .catch((error) => {
          // });
        } else {
          vContinue = false;
          errMessage = `${fileName} convert to base64 error`;
        }
      } else {
        vContinue = false;
        errMessage = `${docType}:${docId} not found`;
      }
    }
    else {
      vContinue = false;
      errMessage = `${docType} collection rule not found`;
    }

    if (vContinue) {
      res.json({ "success": true, result: rtJson.data });
    } else {
      logger.error("/pos-api/startSignature error:", errMessage);
      res.json({ "success": false, message: "Interal Server Error!" });
    }
  },
  getPDFForPreview: async function (req, res, next) {
    let vContinue = true;
    const docType = req.params.docType;
    const docId = req.params.docId;
    const colRule = searchAPI.getUserCollectionRule(docType);
    var errMessage = "";
    var rtJson = {};
    if (colRule) {
      const model = require('../models/' + _.get(colRule, "model"));
      const doc = await dbHelper.dbHelpFindOne(model, { "_id": docId }).catch(error => { vContinue = false });
      // logger.debug("/pos-api/getPDFForPreview doc = ",doc);
      if (doc) {
        const fileName = _.get(doc, "EAB.formFileName", "");
        // logger.debug("/pos-api/getPDFForPreview fileName = ",fileName);
        // const file = await utils.getFileFromFileSystem(fileName);
        const file = await utils.getFileFromFileSystem("DummyFNA.pdf");//use the dummy pdf now
        if (file) {
          res.setHeader('X-Frame-Options', 'ALLOW-FROM');
          res.setHeader('Content-Type', 'application/pdf; charset=utf-8');
          res.send(file);
        } else {
          vContinue = false;
          errMessage = `${fileName} convert to base64 error`;
        }
      } else {
        vContinue = false;
        errMessage = `${docType}:${docId} not found`;
      }
    }
    else {
      vContinue = false;
      errMessage = `${docType} collection rule not found`;
    }

    if (!vContinue) {
      logger.error("/pos-api/startSignature error:", errMessage);
      res.json({ "success": false, message: "Interal Server Error!" });
    }
  },
  getSignedPDFForPreview: async function (req, res, next) {
    let vContinue = true;
    const docType = req.params.docType;
    const docId = req.params.docId;
    const colRule = searchAPI.getUserCollectionRule(docType);
    logger.debug("docType = ", docType);
    logger.debug("docId = ", docId);
    var errMessage = "";
    if (colRule) {
      const model = require('../models/' + _.get(colRule, "model"));
      const doc = await dbHelper.dbHelpFindOne(model, { "_id": docId }).catch(error => { vContinue = false });
      if (doc) {
        var fileName = _.get(doc, "EAB.formFileName", "");
        fileName = fileName.replace(".", "_signed.");
        logger.debug("getSignedPDFForPreview fileName = ", fileName)
        const file = await utils.getFileFromFileSystem(fileName);
        if (file) {
          res.setHeader('X-Frame-Options', 'ALLOW-FROM');
          res.setHeader('Content-Type', 'application/pdf; charset=utf-8');
          res.send(file);
        } else {
          vContinue = false;
          // errMessage = `${fileName} convert to base64 error`;
        }
      } else {
        vContinue = false;
        errMessage = `${docType}:${docId} not found`;
      }
    }
    else {
      vContinue = false;
      errMessage = `${docType} collection rule not found`;
    }

    if (!vContinue) {
      logger.error("/pos-api/startSignature error:", errMessage);
      res.json({ "success": false, message: "Interal Server Error!" });
    }
  },
  getPicForPreview: async function (req, res, next) {
    let vContinue = true;
    const fileName = req.params.fileName;
    const fileNameSuffix = _.head(_.takeRight(fileName.split(".")));
    var errMessage = "";
    logger.debug("getPicForPreview fileNameSuffix = ", fileNameSuffix)
    var file = await utils.getFileFromFileSystem(fileName);
    if (file) {
      res.setHeader('X-Frame-Options', 'ALLOW-FROM');
      // res.setHeader('Content-Type',`image/${fileNameSuffix};charset=utf-8`);
      // res.setHeader('Content-Type',"application/octet-stream");
      res.setHeader('Content-Type', "image/png;charset=utf-8");
      res.send(file);
    } else {
      vContinue = false;
    }
    if (!vContinue) {
      logger.error("/pos-api/startSignature error:", errMessage);
      res.json({ "success": false, message: "Interal Server Error!" });
    }
  },
  saveSignedForm: async function (req, res, next) {
    let vContinue = true;
    const docType = req.params.docType;
    const docId = req.params.docId;
    const signatureType = req.body.signatureType;
    if (signatureType) {
      _.set(req.params, "signatureType", signatureType);
    }
    const colRule = searchAPI.getUserCollectionRule(docType);
    const eSignSessionID = req.params.eSignSessionID;
    var errMessage = "";
    var signedForm = "";
    if (colRule) {
      const model = require('../models/' + _.get(colRule, "model"));
      const doc = await dbHelper.dbHelpFindOne(model, { "_id": docId }).catch(error => { vContinue = false });
      if (doc) {
        const fileName = _.get(doc, "EAB.formFileName", "");
        const signedFileName = fileName.replace(".", "_signed.");
        var signatureParam = {};
        _.set(signatureParam, "docID", _.head(_.split(fileName, ".")));
        _.set(signatureParam, "sessionID", eSignSessionID);
        _.set(signatureParam, "signedPDFOutputType", "BASE64");
        var fs = require('fs');
        // logger.debug("signatureParam = ",signatureParam);
        signedForm = await eSignatureController.getSignedPdfFromEsignature(signatureParam);
        if (vContinue && signedForm) {
          const tempFilePath = "\\";
          var dataBuffer = new Buffer(signedForm.data, 'base64');
          await fs.writeFileSync(tempFilePath + signedFileName, dataBuffer, function (err) {
            if (err) {
              vContinue = false;
            } else {
              vContinue = true;
            }
          });
          if (vContinue) {
            let data = fs.readFileSync(tempFilePath + signedFileName);
            vContinue = await utils.saveFileToFileSystem(signedFileName, data);
            // vContinue = true;
            errMessage = "Failed save to file system";
            if (vContinue) {
              errMessage = "Failed update eApp Status";
              vContinue = await updateSignedStatus("client", docId);

            }
          }
        } else {
          vContinue = false;
          errMessage = "Failed get signed form from eSignature";
        }
      } else {
        vContinue = false;
        errMessage = `${docType}:${docId} not found`;
      }
    }
    else {
      vContinue = false;
      errMessage = `${docType} collection rule not found`;
    }
    // if(!next){
    if (vContinue) {
      res.json({ "success": true, result: "Saved!" });
      // res.send(signedForm);
    } else {
      logger.error("/pos-api/saveSignedForm error:", errMessage);
      res.json({ "success": false, message: "Interal Server Error!" });
    }
    // }
  },
  updateEappSignStatus: async function (req, res, next) {
    let vContinue = true;
    const signatureType = req.params.signatureType;
    const docId = req.params.docId;

    var errMessage = "";
    var updateStr = {};
    if (_.eq(signatureType, "agent")) {
      updateStr = { "SIGNATURE.$.agentSigned": true };
    } else if (_.eq(signatureType, "client")) {
      updateStr = { "SIGNATURE.$.cleintSigned": true };
    }
    // logger.debug("updateStr = ",updateStr);
    const updateResult = await dbHelper.dbHelpUpdate(ModelApplication, { "_id": docId, "SIGNATURE.type": "eAppForm" }, updateStr).catch(error => { vContinue = false });
    // logger.debug("updateResult = ",updateResult)
    if (vContinue && _.eq(updateResult.ok, 1)) {
    } else {
      vContinue = false;
    }
    if (vContinue) {
      res.json({ "success": true, result: 'updated!' });
    } else {
      logger.error("/pos-api/updateEappSignStatus error:", errMessage);
      res.json({ "success": false, message: "Interal Server Error!" });
    }
  },
  submitEapp: async function (req, res, next) {
    let vContinue = true;
    const docId = req.params.docId;

    var errMessage = "";
    var updateStr = { "EAB.applicationStatus": "S" };
    // logger.debug("updateStr = ",updateStr);
    const updateResult = await dbHelper.dbHelpUpdate(ModelApplication, { "_id": docId }, updateStr).catch(error => { vContinue = false });
    // logger.debug("updateResult = ",updateResult)
    if (vContinue && _.eq(updateResult.ok, 1)) {
    } else {
      vContinue = false;
    }
    if (vContinue) {
      res.json({ "success": true, result: 'updated!' });
    } else {
      logger.error("/pos-api/submitEapp error:", errMessage);
      res.json({ "success": false, message: "Interal Server Error!" });
    }
  },
  createEapp: async function (req, res, next) {
    let vContinue = true;
    const quoteId = req.params.quoteId;
    const clientId = req.params.clientId;
    var errMessage = "";
    // var createString = {"EAB.applicationStatus":"S"};
    // logger.debug("updateStr = ",updateStr);
    const result = await searchAPI.fnCreate("eApplication", {}).catch(error => {
      vContinue = false; errMessage = error.message;
    });
    logger.debug("eapp = ", result)
    var returnJson = {};
    if (vContinue && result) {
      let mappingParameter = { "relationType": "quoteEAppMapping" };
      const eAppId = _.toString(result.result._id);
      let relationShip = { "oppositID": eAppId };
      let data = { "currentID": quoteId };
      data["relationShip"] = relationShip;
      mappingParameter["data"] = data;
      _.set(returnJson, "_id", eAppId);
      _.set(returnJson, "EAB.eAppId", result.result.EAB.eAppId);
      logger.debug("mappingParameter = ", mappingParameter)
      var reletionResult = await searchAPI.fnRelationCreate("quotation", mappingParameter).catch(error => {
        vContinue = false; errMessage = error.message;
      });
      logger.debug("reletionResult quoteEAppMapping = ", reletionResult)

      mappingParameter = { "relationType": "clienteAppMapping" };
      relationShip = { "oppositID": eAppId };
      data = { "currentID": clientId };
      data["relationShip"] = relationShip;
      mappingParameter["data"] = data;
      // _.set(returnJson,"_id",eAppId);
      // _.set(returnJson,"EAB.eAppId",result.result.EAB.eAppId);
      logger.debug("mappingParameter = ", mappingParameter)
      reletionResult = await searchAPI.fnRelationCreate("clientProfile", mappingParameter).catch(error => {
        vContinue = false; errMessage = error.message;
      });
      logger.debug("reletionResult clienteAppMapping = ", reletionResult)


    } else {
      vContinue = false;
    }
    if (vContinue) {
      res.json({ "success": true, result: returnJson });
    } else {
      logger.error("/pos-api/createEapp error:", errMessage);
      res.json({ "success": false, message: "Interal Server Error!" });
    }
  },
  getAvailableQuoteListForEapp: async function (req, res, next) {
    let clientId = req.params.clientId;
    let lang = "en";
    // let clientId = req.params.clientId;
    let relationSearchParameter = { "docType": "clientProfile", "relationType": "clientQuoteMapping", "key": clientId };
    let vquotes = await searchAPI._fnRelationalSearch(relationSearchParameter, null).catch(error => {
      logger.debug(error);
    });
    if (_.get(vquotes, "success")) {
      vquotes = vquotes.result;
      // _.remove(vquotes,quote=>{
      let quoteIds = vquotes.map(item => { return _.toString(item._id) });
      // logger.debug("quoteIds = ",vquotes);
      for (let i in quoteIds) {
        let linked = await eAppFunction.hasLinked(quoteIds[i]);
        logger.debug("linked = ", quoteIds[i], linked);
        if (linked) {
          _.remove(vquotes, (quote) => { return _.eq(quote.oppositID, quoteIds[i]) });
          // _.remove(vquotes,quoteIds[i])
        }
      }
      vquotes.forEach(quote => {
        let basicPlan = _.find(quote.plans, { "planInd": "B" });
        _.set(quote, "basicPlanCode", basicPlan.covCode);
        _.set(quote, "basicPlanName", basicPlan.covName[lang]);
      });
      var rtn = { "clientDocId": clientId, "quotes": vquotes };
      if (vquotes.length > 0)
        res.json({ "success": true, result: rtn });
      else
        res.json({ "success": false, message: "No record!" });
    } else
      res.json({ "success": false, message: vquotes.message });
  }
};
