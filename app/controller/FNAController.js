import POSConfig from '../../config/POSConfig';
import logger from '../loaders/logger';
import mongoose from 'mongoose';
import axios from 'axios';
const fnaFunction = require('../biz/fna/fnaFunction');
var ModelSeqMst = require('../models/ModelSeqMst');
const ModelClientProfile = require('../models/ModelClientProfile');
const ModelFNA = require('../models/ModelFNA');
const _ = require('lodash');
const dbHelper = require('../utils/dbHelper');
const commonFunction = require('../common/function');
const utils = require('../utils/utils');
const searchAPI = require('./searchEngineController');
var ModelQuote = require('../models/ModelQuote');
var fs = require('fs');
// function getSystemCollectionRule(searchType){
//   //need to be implement
//   return systemConfig.collectionRules[searchType];
// }
exports.api = {
getFNAForm:async function(req, res, next){
    let docId = req.params.docId;
      let queryParams = {"searchStr":{"_id":docId},"docType":"posFNAMst"};
      let fna = await searchAPI.fnSearch(queryParams,"post");
      // logger.debug("fna = ",fna.result);
      let rtResult = {};
      if(_.get(fna,"success")){
        fna = fna["result"];
        queryParams = {"searchStr":{"relationship":"{$elemMatch:{$eq:{oppositID:'"+docId+"'}}"},"docType":"relationshipMapping"};
        // let clientFnaMapping = await searchAPI.fnSearch(queryParams,"post");
        let ModelRelationMapping = require("../models/ModelRelationMapping");
        // docId = "5e55dab0fea3a16c747344f6";
        let str = {$elemMatch:{$eq:{oppositID:docId}}};
        let clientFnaMapping = await dbHelper.dbHelpFindOne(ModelRelationMapping,{"relationship":str});
        // let clientFnaMapping = await dbHelper.dbHelpFindOne(ModelRelationMapping,{"currentID":"5e55dc99fea3a16c747344fd"});

        fna.forEach(item=>{

           if(_.eq(_.get(item,"EAB.status"),"E"))
             item = _.set(item,"EAB.disableQuote",true);
           _.set(item,"fna._id",item._id);
          //  _.set(item,"fna.responseID",_.get(item,"EAB.responseID"));
        });
        rtResult = _.head(fna);
        // logger.debug("rtResult = ",rtResult)
        if(clientFnaMapping){
          rtResult =_.set(rtResult,"EAB.clientID",clientFnaMapping.currentID);
        }
        res.json({"success":true,result:rtResult});
      }else
        res.json({"success":false,message:fna});
  },
  getFNAListByClient:async function(req, res, next){
    let clientId = req.params.clientId;
    // logger.debug("fnaFormId = ",fnaFormId);
    // logger.debug("quoteId = ",quoteId);
    // logger.debug("quoteId = ",req.params);
    let errorMessage = "";
    var rtnJson = {};
    let relationSearchParameter = {"docType":"clientProfile",	"relationType":"clientFNAMapping","key":clientId};
    if(clientId){
      let fnas = await searchAPI._fnRelationalSearch(relationSearchParameter,null).catch(error=>{
          errorMessage = error;
      });
      if(_.get(fnas,"success")){
         fnas = fnas["result"];                 
         fnas.forEach(item=>{
            if(_.eq(_.get(item,"EAB.status"),"P"))
              _.set(item,"editShow",true);
            else
              _.set(item,"viewShow",true);
            if(!_.eq(_.get(item,"EAB.status"),"C"))
              _.set(item,"disableRecommendation",true);
            _.set(item,"fna._id",item._id);
              // _.set(item,"EAB.clientID",clientId);
         });
         fnas = _.set({},"fnaList",fnas);
         _.set(fnas,"clientDocId",clientId);
         
        }
      else{
        if(_.eq("No record found!",_.get(fnas,"message"))){
          _.set(rtnJson,"clientDocId",clientId)          
        }else{
         errorMessage = _.get(fnas,"message");
        }
      }
      if(errorMessage){
          res.json({"success":false,message:errorMessage});
      }else{
        if(!_.isEmpty(rtnJson))
          res.json({"success":true,result:rtnJson});
        else
          res.json({"success":true,result:fnas});
      }
    }else{
      res.json({"success":false,message:"Parameter clientId missing!"});
    }
  },
  getLinkableFNAListByClient:async function(req, res, next){
    let clientId = req.params.clientId;
    let quoteId = req.params.quoteId;
    // logger.debug("fnaFormId = ",fnaFormId);
    // logger.debug("quoteId = ",quoteId);
    // logger.debug("quoteId = ",req.params);
    let errorMessage = "";
    let allPass = true;
    let relationSearchParameter = {"docType":"clientProfile",	"relationType":"clientFNAMapping","key":clientId};
    if(clientId){
      let fnas = await searchAPI._fnRelationalSearch(relationSearchParameter,null).catch(error=>{
          errorMessage = error;
      });
      if(_.get(fnas,"success")){
         fnas = fnas["result"];
        //  fnas.forEach(item=>{
        //     if(_.eq(_.get(item,"EAB.status"),"P"))
        //       _.set(item,"editShow",true);
        //     else
        //       _.set(item,"viewShow",true);
        //     if(!_.eq(_.get(item,"EAB.status"),"C"))
        //       _.set(item,"disableRecommendation",true);
        //       _.set(item,"EAB.clientID",clientId);
        //  });
        _.remove(fnas,(item)=>{
          _.set(item,"quoteDocId",quoteId);
          _.set(item,"fna._id",item._id);
          _.set(item,"viewShow",true);
          return !_.eq(_.get(item,"EAB.status"),"C") && !_.eq(_.get(item,"EAB.status"),"S");
        });
        }
      else
         errorMessage = _.get(fnas,"message");
      if(errorMessage){
          res.json({"success":false,message:errorMessage});
      }else{
        res.json({"success":true,result:fnas});
      }
    }else{
      res.json({"success":false,message:"Parameter clientId missing!"});
    }
  },
  getFnaCafeFormWithClientData:async function(req, res, next){

    let clientId = req.params.clientId;
    let formId = req.params.formId;
    // logger.debug("getFnaCafeFormWithClientData clientId = ",clientId);
    // logger.debug("getFnaCafeFormWithClientData formId = ",formId);
    let queryParams = {"_id":clientId};
    let rtResult = {};
    let result = await dbHelper.dbHelpFindOne(ModelClientProfile,queryParams).catch(error=>{
        res.json({"success":false,message:""+error});
      });
    if(result){
      //  logger.debug("getFnaCafeFormWithClientData result = ",result);
       let cafeParam = fnaFunction.toCafeParam(result,formId);
      //  logger.debug("cafeParam = ",JSON.stringify(cafeParam));
       logger.debug("cafeParam = ",cafeParam);
       logger.debug(`${POSConfig["CAFE-api"]}/response/importData`);
       await axios({
        method: "post",
        url: `${POSConfig["CAFE-api"]}/response/importData`,
        data: cafeParam,
        headers: {
            "Content-Type": "application/json"
        }
        })
        .then((response) => {
            // logger.debug("cafe result = ",response.data);
            res.json({"success":true,result:response.data.result});
        })
        .catch((error) => {
            // res.json({success:false, message: 'here :'+error });
            res.json({success:false, message: ''+error });
            
        });

      //  res.json({"success":true,result:cafeParam});
    }else{
      res.json({"success":false,message:"Internal Server Error!"});
    }
  },  
  checkFnaSignedComplete:async function(req, res, next){
    let fnaFormId = req.params.fnaFormId;
      let status = fnaFunction.signedFNA(fnaFormId);
      if(!status){
          res.json({"success":false,message:"Signed failed!"});
      }else{
          res.json({"success":true,result:"Signed successfully!"});
      }
  },
  checkFnaExpired:async function(req, res, next){
    let fnaFormId = req.params.fnaFormId;
      let status = fnaFunction.saveFnaSignedForm(fnaFormId);
      if(status){
         status = fnaFunction.fnaExpired(fnaFormId);
      }
      if(!status){
          res.json({"success":false,message:"Failed!"});
      }else{
          res.json({"success":true,result:"success!"});
      }  
  },
  fnaValidationAgainstQuote:async function(req, res, next){
    let quoteId = req.params.quoteId;
    let fnaFormId = req.params.fnaFormId;
    // logger.debug("fnaFormId = ",fnaFormId);
    // logger.debug("quoteId = ",quoteId);
    // logger.debug("quoteId = ",req.params);
    let errorMessage = "";
    if(fnaFormId && quoteId){
      let quote = await dbHelper.dbHelpFindOne(ModelQuote,{"_id":quoteId}).catch(error=>{
          errorMessage = error;
      });
      
      let fna = await dbHelper.dbHelpFindOne(ModelFNA,{"_id":fnaFormId}).catch(error=>{
          errorMessage = error;
      });
      if(errorMessage){
          res.json({"success":false,message:errorMessage});
      }else{
        if(quote && fna){
          let rm = await fnaFunction.suitAffordCheckAndUpdateMapping(quote,fna);
          if(_.get(rm,"success")){
            res.json({"success":true,result:rm.result});
          }else{
            res.json({"success":false,message:rm.message});
          }
        }else{
          res.json({"success":false,message:"no fna or quote been found!"});
        }
      }
    }else{
      res.json({"success":false,message:"Parameter fnaFormId/quoteId missing!"});
    }    
  },
  fnaValidationAgainstQuotes:async function(req, res, next){
    // let quoteId = req.params.quoteId;
    let fnaFormId = req.params.fnaFormId;
    // logger.debug("fnaFormId = ",fnaFormId);
    // logger.debug("quoteId = ",quoteId);
    // logger.debug("quoteId = ",req.params);
    let errorMessage = "";
    let allPass = true;
    let relationSearchParameter = {"docType":"posFNAMst",	"relationType":"fnaQuoteMapping","key":fnaFormId};
    if(fnaFormId){
      let quotes = await searchAPI._fnRelationalSearch(relationSearchParameter,null).catch(error=>{
          errorMessage = error;
      });
      let fna = await dbHelper.dbHelpFindOne(ModelFNA,{"_id":fnaFormId}).catch(error=>{
          errorMessage = error;
      });
      if(errorMessage){
          res.json({"success":false,message:errorMessage});
      }else{

        if(quotes && fna){
          for(let i in quotes.result){
            let rm = await fnaFunction.suitAffordCheckAndUpdateMapping(quotes.result[i],fna);
            if(_.get(rm,"success")){
            }else{
              // res.json({"success":false,message:rm.message});
              errorMessage = rm.message;
              allPass = false;
            }       
        }
        if(allPass){
          res.json({"success":true,result:"quote updated!!"});
        }else{
          res.json({"success":false,message:errorMessage});
        }
        }else{
          res.json({"success":false,message:"no record updated!"});
        }
      }
    }else{
      res.json({"success":false,message:"Parameter fnaFormId missing!"});
    }
  },

  createFnaQuote:async function(req, res, next){
    let quoteId = req.params.quoteId;
    let fnaFormId = req.params.fnaFormId;
    logger.debug("fnaFormId = ",fnaFormId);
    logger.debug("quoteId = ",quoteId);
    logger.debug("next = ",_.isEmpty(next));
    let errorMessage = "";
    let resResult = "";
    if(fnaFormId && quoteId){
       let mappingParameter = {"relationType":"fnaQuoteMapping"};
       let relationShip = {"oppositID":quoteId};
       let data = {"currentID":fnaFormId};
       data["relationShip"]=relationShip;
       mappingParameter["data"]=data;

       await searchAPI.fnRelationCreate("posFNAMst",mappingParameter).then((result)=>{
        if(result["success"]){
          resResult = result.result;
        }else{
          errorMessage = result.message;
        }}).catch(error=>{errorMessage = error.message});
        if(errorMessage)
          res.json({"success":false,message:"Internal server error=>"+errorMessage});
        else{
          if(!_.isEmpty(next))
            next();
          else
            res.json({"success":true,result:resResult});
        }
    }else{
      res.json({"success":false,message:"Parameter fnaFormId/quoteId missing!"});
    }
    
  },
  deleteFnaQuote:async function(req, res, next){
    let quoteId = req.params.quoteId;
    let fnaFormId = req.params.fnaFormId;
    // logger.debug("fnaFormId = ",fnaFormId);
    // logger.debug("quoteId = ",quoteId);
    // logger.debug("quoteId = ",req.params);
    let errorMessage = "";
    let resResult = "";
    if(fnaFormId && quoteId){
       let mappingParameter = {"relationType":"fnaQuoteMapping","actionType":"delete"};
       let relationShip = {"oppositID":quoteId};
       let data = {"currentID":fnaFormId};
       data["relationShip"]=relationShip;
       mappingParameter["data"]=data;

       await searchAPI.fnRelationCreate("posFNAMst",mappingParameter).then((result)=>{
         logger.debug("result = ",result);
        if(result["success"]){
          resResult = result.result;
        }else{
          errorMessage = result.message;
        }}).catch(error=>{errorMessage = error.message});
        if(errorMessage)
          res.json({"success":false,message:"Internal server error=>"+errorMessage});
        else
          res.json({"success":true,result:resResult});
    }else{
      res.json({"success":false,message:"Parameter fnaFormId/quoteId missing!"});
    }
  },
  getFnaPdf:async function(req, res, next){
    let vContinue = true;
    let file = "";
    let docId = req.params.docId;
    let fileName = "";
      // let fnaForm
      let fna = await dbHelper.dbHelpFindOne(ModelFNA,{"_id":docId});
      if(fna){
        fileName = _.get(fna,"EAB.formFileName","");
        if(!fileName){
          vContinue = false;
        }
      }else{
        vContinue = false;
      }
      if(vContinue){
        logger.debug("fileName = ",fileName);
        file = await utils.getFileFromFileSystem(fileName);
        res.setHeader('X-Frame-Options','ALLOW-FROM');
        res.setHeader('Content-Type','application/pdf; charset=utf-8');
        // file = await utils.fileToBase64("d:\\loadSignedPDF.pdf");
        // logger.debug("test = ",file);
        
      }
      res.send(file);
  },
  generateFnaPdf:async function(req, res, next){
    let docId = req.params.docId;

    res.json({"success":false,message:"Generate FNA PDF Successfully!"});
  }
  ,goToQuoteView:async function(req, res, next){
    let vContinue = true;
    const docType = req.params.docType;
    const docId = req.params.docId;
    var errMessage = "";
      // const model = require('../models/'+_.get(colRule,"model"));
      // const doc = await dbHelper.dbHelpFindOne(model,{"_id":docId}).catch(error=>{vContinue = false});
      // logger.debug("/pos-api/getPDFForPreview doc = ",doc);
      if(true){
        const fileName = "loadSignedPDF.pdf";
        // logger.debug("/pos-api/getPDFForPreview fileName = ",fileName);
        const file = await utils.getFileFromFileSystem(fileName);        
        if(file){
          res.setHeader('X-Frame-Options','ALLOW-FROM');
          res.setHeader('Content-Type','application/pdf; charset=utf-8');
          res.send(file);
        }else{
          vContinue = false;
          errMessage = `${fileName} convert to base64 error`;
        }
      }else{
        vContinue = false;
        errMessage = `${docType}:${docId} not found`;
      }
      
      if(!vContinue){
        logger.error("/pos-api/startSignature error:",errMessage);
        res.json({"success":false,message:"Interal Server Error!"});
      }
  }
};
const  calAnnualizedPremium =(newStaff) => new Promise((resolve,reject) =>{
  resolve(_calAnnualizedPremium(newStaff));
});
const _calAnnualizedPremium =async (newStaff)=>{
  let updString = fnaFunction.calculationAfterCompleteFNA(newStaff);
  let fnaId = newStaff._id;
  await dbHelper.dbHelpFindOneAndUpdate(ModelFNA,{"_id":fnaId},updString);
  logger.debug("calAnnualizedPremium done");
};
const  createClientFNA =(body) => new Promise((resolve,reject) =>{
  resolve(_createClientFNA(body));
});
const _createClientFNA =async (body)=>{
  let param = { "relationType":"clientFNAMapping", "docType": "clientProfile"};
  param = _.set(param,"data.currentID",body.data.clientID);
  param = _.set(param,"data.relationShip.oppositID",body.oppositID);
  // param = _.set(param,"data.relationShip.oppositRole",body.oppositRole);
  logger.debug("createClientFNA body = ",body)
  logger.debug("createClientFNA param = ",param)
  await searchAPI.fnRelationCreate("clientProfile",param).then((result)=>{
    if(result["success"]){
      logger.debug("createClientFNA success = ",result)
  }else{
      logger.debug("createClientFNA fail = ",result)
  }
  }).catch((error) => {
    res.json({ success: false,message:"" +error});
  });
};

export {
    calAnnualizedPremium,
    createClientFNA,
    // _calAnnualizedPremium
  };