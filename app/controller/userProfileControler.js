var ModelUserProfile = require('../models/ModelUserProfile');

const dbHelper = require('../utils/dbHelper');
const _ = require('lodash');


exports.api = {
  loadUserProfile:function(req, res, next){
    let userId = req.params.userId;
    let queryParams = {};
    let queryAndOrParams= {};
    if(userId){
        queryParams = {"_id":userId};
    }

    dbHelper.dbHelpFindOne(ModelUserProfile,queryParams,queryAndOrParams,
        { },
        {}).then((result) => {
      if(!_.isEmpty(result)){
         res.json({ success: true, data: result });
      }else{
         res.json({ success: false, message: userId+":no such user found"});
      }
    
      })
        .catch((error) => {
          res.json({ success: false,message: 'User Profile not found'+ error});
        });

  }
  
  

}