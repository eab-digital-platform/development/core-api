// import POSConfig from '../../config/POSConfig';
import logger from '../loaders/logger';
import mongoose from 'mongoose';
// import axios from 'axios';
// const ModelClientProfile = require('../models/ModelClientProfile');
// const ModelFNA = require('../models/ModelFNA');
const _ = require('lodash');
const dbHelper = require('../utils/dbHelper');
const searchAPI = require('./searchEngineController');
const ModelQuote = require('../models/ModelQuote');
const ModelClientProfile = require('../models/ModelClientProfile');
const ModelProductList = require('../models/ModelProductList');
const ModelApplication = require('../models/ModelApplication');
const quoteFunction = require('../biz/quote/quoteFunction');
const ModelRelationMapping = require('../models/ModelRelationMapping');
const utils = require('../utils/utils');
exports.api = {
  getQuoteListByFnaGroupByFamilyMember:async function(req, res, next){
    let fnaFormId = req.params.fnaFormId;
    let lang = "en";
    let clientId = req.params.clientId;
    // logger.debug("fnaFormId = ",fnaFormId);
    let relationSearchParameter = {"docType":"posFNAMst",	"relationType":"fnaQuoteMapping","key":fnaFormId,"grouping":{"groupBy":"iCid","groupName":"iFullName"}};
    logger.debug("relationSearchParameter = ",relationSearchParameter);
    let quoteGroups = await searchAPI._fnRelationalSearch(relationSearchParameter,null).catch(error=>{
      logger.debug(error);
    });
    if(_.get(quoteGroups,"success")){
      quoteGroups = quoteGroups.result;
      quoteGroups.forEach(quoteGroup=>{
        let quotes = quoteGroup.items;
        if(!_.isArray(quotes)){
          quotes = [quotes];
        }
        // logger.debug("quotes = ",quotes);
        quotes.forEach(quote=>{
            let suitabillity = quote.checkSuit;
            let affordabillity = quote.checkAfford;
            quote["suitPass"] = suitabillity;
            quote["affordPass"] = affordabillity;
            quote["suitFail"] = !suitabillity;
            quote["affordFail"] = !affordabillity;
            quote["clientDocId"]=clientId;

            quote["disableApply"] = !(suitabillity||affordabillity);
            let basicPlan = _.find(quote.plans,{"planInd":"B"});
            quote["basicPlanCode"] = basicPlan.covCode;
            quote["basicPlanName"] = basicPlan.covName[lang];
            delete quote["checkSuit"];
            delete quote["checkAfford"];
            delete quote["oppositID"];
        });
      });
      res.json({"success":true,result:quoteGroups});
    }else
      res.json({"success":false,message:quoteGroups.message});
  },
  getAvailableQuoteListForFNA:async function(req, res, next){
    let clientId = req.params.clientId;
    let fnaFormId = req.params.fnaFormId;
    let lang = "en";
    // let clientId = req.params.clientId;
    let relationSearchParameter = {"docType":"clientProfile",	"relationType":"clientQuoteMapping","key":clientId};
    let vquotes = await searchAPI._fnRelationalSearch(relationSearchParameter,null).catch(error=>{
      logger.debug(error);
    });
    if(_.get(vquotes,"success")){
      vquotes = vquotes.result;
      // _.remove(vquotes,quote=>{
      let quoteIds = vquotes.map(item=>{return _.toString(item._id)});
      // logger.debug("quoteIds = ",vquotes);
      for(let i in quoteIds){
        let linked = await quoteFunction.hasLinked(quoteIds[i],clientId);
        // logger.debug("linked = ",quoteIds[i],linked);
        if(linked){
          _.remove(vquotes,(quote)=>{return _.eq(quote.oppositID,quoteIds[i])});
          // _.remove(vquotes,quoteIds[i])
        }
      }
      vquotes.forEach(quote=>{
        let basicPlan = _.find(quote.plans,{"planInd":"B"});
        _.set(quote,"basicPlanCode",basicPlan.covCode);
        _.set(quote,"basicPlanName",basicPlan.covName[lang]);
      });
      if(vquotes.length>0)
        res.json({"success":true,result:{fnaFormId:fnaFormId,quotes:vquotes}});
      else
        res.json({"success":false,message:"No record!"});
    }else
      res.json({"success":false,message:vquotes.message});
  },
  getQuoteListByClient:async function(req, res, next){
    let clientId = req.params.clientId;
    let params = req.query;
    logger.debug("clientId = ",clientId);
    logger.debug("params = ",params);
    let errorMessage = "";
    const lang = "en";
    let relationalSearchStr = _.get(req.body,"relationalSearchStr",_.get(req.query,"relationalSearchStr",{}));
    let relationalSearchKey =  _.get(req.body,"relationalSearchKey",_.get(req.query,"relationalSearchKey",{}));
    logger.debug("relationalSearchStr = ",relationalSearchStr)
    logger.debug("relationalSearchKey = ",relationalSearchKey)
    let relationSearchParameter = {"docType":"clientProfile",	"relationType":"clientQuoteMapping","key":clientId,"relationalSearchStr":relationalSearchStr,"relationalSearchKey":relationalSearchKey};
      let quotes = await searchAPI._fnRelationalSearch(relationSearchParameter,null).catch(error=>{
        if(error){errorMessage = error.message;}
      });
      // logger.debug("quotes = ",quotes)
      if(_.get(quotes,"success")){
        quotes = quotes["result"];
        for(let i in quotes){
          var o = quotes[i];
          const status = _.get(o,"EAB.status");

          let basicPlan = _.find(o.plans,{"planInd":"B"});
          o["basicPlanCode"] = basicPlan.covCode;
          o["basicPlanName"] = basicPlan.covName[lang];

          const fnaOptional = !(await quoteFunction.isFnaReqiuredProduct(basicPlan).catch(error=>{if(error){errorMessage = error.message;}}));
          const fnaLink = _.isEmpty(utils.getRelatedObject("clientQuote",_.toString(o._id),false))?false:true;
          const fnaLinkWithPass = fnaLink;//?????????
          const fnaMandatory = !fnaOptional;
          if(_.eq(status,"Q") && ((fnaOptional && !fnaLink) || fnaLinkWithPass)){             
             _.set(o,"startEappDisplay",true);
             _.set(o,"quotedShow",true);
          }
          if(_.eq(status,"A") && ((fnaOptional && !fnaLink) || (fnaLinkWithPass && fnaMandatory))){
            _.set(o,"continueEappDisplay",true);
            _.set(o,"applyingShow",true);
            var con = {"EAB.type":"quoteEapp","currentID":_.toString(o._id)};
            const quoteEapp = await dbHelper.dbHelpFindOne(ModelRelationMapping,con).catch(error=>{if(error)errorMessage = error.message});
            const eAppDocId = _.get(_.head(_.get(quoteEapp,"relationship")),"oppositID","");
            if(quoteEapp && !errorMessage){
              _.set(o,"EAB.eAppDocId",eAppDocId);
            }
            con = {"_id":eAppDocId};
            logger.debug("eAppDocId = ",eAppDocId)
            const eApp = await dbHelper.dbHelpFindOne(ModelApplication,con).catch(error=>{if(error)errorMessage = error.message});
            if(eApp && !errorMessage){
              const eAppId = _.get(eApp,"EAB.eAppId","");
              _.set(o,"EAB.eAppId",eAppId);
            }
          }
          if(_.eq(status,"S")){
            _.set(o,"viewEappDisplay",true);
            _.set(o,"signedShow",true);
          }
          if(_.eq(status,"E")){
            _.set(o,"expiredShow",true);
          }
          _.set(o,"quote._id",_.toString(_.get(o,"_id")));

          const linkedToFna = fnaOptional && !fnaLink;
          const fnaLinkedSuccess = fnaLink;
          const fnaLinkedFail = !fnaOptional && !fnaLink;
          const fnaRequired = !fnaOptional;

          _.set(o,"linkedToFna",linkedToFna);
          if(linkedToFna)
            _.set(o,"linkedToFnaTips","This Proposal is FNA compulsory. You need to link and FNA before staring eApplication");
          _.set(o,"fnaLinkedSuccess",fnaLinkedSuccess);
          _.set(o,"fnaLinkedFail",fnaLinkedFail);
          _.set(o,"fnaRequired",fnaRequired);

          _.set(o,"EAB.clientDocId",clientId);
          _.set(o,"EAB.quoteDocId",o._id);
          // return _.get(o,"EAB.isDeleted");
        }
        }
      else
         errorMessage = _.get(quotes,"message");
      if(errorMessage){
          res.json({"success":false,message:errorMessage});
      }else{
        res.json({"success":true,result:quotes});
      }
  },
  createQuoteEapp:async function(req, res, next){
    let quoteId = req.params.quoteId;
    let eAppId = req.params.eAppId;
    let errorMessage = "";
    let resResult = "";
    if(eAppId && quoteId){
       let mappingParameter = {"relationType":"quoteEAppMapping"};
       let relationShip = {"oppositID":eAppId};
       let data = {"currentID":quoteId};
       data["relationShip"]=relationShip;
       mappingParameter["data"]=data;

       await searchAPI.fnRelationCreate("quotation",mappingParameter).then((result)=>{
        if(result["success"]){
          resResult = result.result;
        }else{
          errorMessage = result.message;
        }}).catch(error=>{errorMessage = error.message});
        if(errorMessage)
          res.json({"success":false,message:"Internal server error=>"+errorMessage});
        else
          res.json({"success":true,result:resResult});
    }else{
      res.json({"success":false,message:"Parameter fnaFormId/quoteId missing!"});
    }
    
  },
  deleteQuoteEapp:async function(req, res, next){
    let quoteId = req.params.quoteId;
    let eAppId = req.params.eAppId;
    let errorMessage = "";
    let resResult = "";
    if(eAppId && quoteId){
       let mappingParameter = {"relationType":"quoteEAppMapping","actionType":"delete"};
       let relationShip = {"oppositID":eAppId};
       let data = {"currentID":quoteId};
       data["relationShip"]=relationShip;
       mappingParameter["data"]=data;

       await searchAPI.fnRelationCreate("quotation",mappingParameter).then((result)=>{
        if(result["success"]){
          resResult = result.result;
        }else{
          errorMessage = result.message;
        }}).catch(error=>{errorMessage = error.message});
        if(errorMessage)
          res.json({"success":false,message:"Internal server error=>"+errorMessage});
        else
          res.json({"success":true,result:resResult});
    }else{
      res.json({"success":false,message:"Parameter fnaFormId/quoteId missing!"});
    }
    
  },
  updateQuoteStatus:async function(req, res, next){
    let vContinue = true;
    let quoteId = req.params.quoteId;
    let updString ={"EAB.status":_.get(req.body,"status",_.get(req.query,"status",""))}; 
    let errorMessage = "";

    if(vContinue)
       vContinue = quoteFunction.validateForQuoteStatusUpdate(quoteId,"01")
    if(vContinue){
      let resResult = await dbHelper.dbHelpFindOneAndUpdate(ModelQuote,{"_id":quoteId},updString);
      if(resResult._id){
        res.json({"success":true,result:"Update Success!"});
      }
      else{
      res.json({"success":false,message:"Internal server error=>"+errorMessage});
      }
    }
  },
  markDeleteQuote:async function(req, res, next){
    let quoteId = req.params.quoteId;
    var deleteQuote = await quoteFunction.invalidateQuotation(quoteId,"01")
    if(deleteQuote){
      res.json({"success":true,result:{"n": 1,
      "nModified": 1,
      "ok": 1}});
    }
    else{
      res.json({"success":false,message:"Internal server error"});
    }
  },
  invalidateQuotation:async function(req, res, next){
    let vContinue = true;
    let quoteId = req.params.quoteId;
    vContinue = quoteFunction.invalidateQuotation(quoteId);
    
  },
  getAvailableProductList:async function(req, res, next){
    let vContinue = true;
    let clientId = req.params.clientId;
    const fnaFormId = req.params.fnaFormId;
    if(clientId){
      const client = await dbHelper.dbHelpFindOne(ModelClientProfile,{"_id":clientId}).catch(error=>{vContinue = false});
      const productList = await dbHelper.dbHelpFindOne(ModelProductList,{}).catch(error=>{vContinue = false});
      const clientName = _.get(client,"CAFE.fullName")
      if(vContinue){
        let availableProductList = quoteFunction.getAvailableProductList(client,productList)
        var rtnProductList = {"fnaFormId":fnaFormId,"productList":availableProductList};
        _.set(rtnProductList,"fullName",clientName)
        res.json({"success":true,result:rtnProductList});
      }else{
        res.json({"success":false,message:"Internal server error!"});
      }
    }else{
      res.json({"success":false,message:"Parameter clientId missing!"});
    }
  },
  cloneQuote:async function(req, res, next){
    let vContinue = true;
    let quoteId = req.params.quoteId;
    let clientDocId = req.params.clientId;
      if(!quoteId){
        quoteId = "5e4c98399106f1fe73a01fba";
      }
      let reset = await searchAPI.initCreateMetaData("quotation",{}).catch(error=>{logger.debug("error = ",error);vContinue = false;});
      // logger.debug("reset = ",reset)
      if(vContinue){
        const newId = await utils.dbClone(ModelQuote,quoteId,clientDocId,reset).catch(error=>{logger.debug("error = ",error);vContinue = false;});;
        logger.debug("newId=",newId);
        if(vContinue && newId){
          if(!_.isEmpty(next))
            res.json({"success":true,result:"Clone Successfully"});
          else{
            _.set(req,"params.quoteId",_.toString(newId));
            next();
          }
        }else{
          res.json({"success":false,message:"Internal server error!"});
        }
      }
  },
  createClientQuote:async function(req, res, next){
    let quoteId = req.params.quoteId;
    let clientId = req.params.clientId;
    let errorMessage = "";
    let resResult = "";
    if(clientId && quoteId){
       let mappingParameter = {"relationType":"clientQuoteMapping"};
       let relationShip = {"oppositID":quoteId};
       let data = {"currentID":clientId};
       data["relationShip"]=relationShip;
       mappingParameter["data"]=data;

       await searchAPI.fnRelationCreate("clientProfile",mappingParameter).then((result)=>{
         logger.debug("result = ",result);
        if(result["success"]){
          resResult =  {
            "n": 1,
            "nModified": 1,
            "ok": 1
        };
        }else{
          errorMessage = result.message;
        }}).catch(error=>{errorMessage = error.message});
        if(errorMessage)
          res.json({"success":false,message:"Internal server error=>"+errorMessage});
        else
          res.json({"success":true,result:resResult});
    }else{
      res.json({"success":false,message:"Parameter fnaFormId/quoteId missing!"});
    }
    
  },
};