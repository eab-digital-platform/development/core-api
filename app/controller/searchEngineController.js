import systemConfig from '../../config/systemConfig';
import userConfig from '../../config/userConfig';
import logger from '../loaders/logger';
import mongoose from 'mongoose';
var ModelSeqMst = require('../models/ModelSeqMst');
var ModelRelationMapping = require('../models/ModelRelationMapping');
const _ = require('lodash');
const dbHelper = require('../utils/dbHelper');
const utils = require('../utils/utils');
var moment = require("moment");
// function getSystemCollectionRule(searchType){
//   //need to be implement
//   return systemConfig.collectionRules[searchType];
// }
const getRelatedObject = async (relationType,curId,isCurrent)=>{
  var criteria = {"EAB.type":relationType};
  let str = {$elemMatch:{oppositID:curId}};
  if(isCurrent){
    _.set(criteria,"currentID",curId);
  }else{
    _.set(criteria,"relationship",str);
  }
  let rtnValue = await dbHelper.searchAllResult(ModelRelationMapping,criteria);
  // logger.debug("getRelatedObject rtnValue = ",rtnValue);
  return rtnValue;
};
const  initCreateMetaData =(model,patameter) => new Promise((resolve,reject) =>{
  resolve(_initCreateMetaData(model,patameter));
});
const _initCreateMetaData = async (model,patameter)=>{
  var rt = {};
  const collectionRule = getUserCollectionRule(model);
  // logger.debug("collectionRule:",collectionRule);
  const createMetaDatas = collectionRule.createMetaDatas;
  // logger.debug("createMetaDatas:",createMetaDatas);
  if(createMetaDatas){
  for(var createDefaultField of createMetaDatas){  
    // logger.debug("createDefaultField:",createDefaultField);    
     await replaceFields(model,createDefaultField,patameter).then(result=>{       
      if(result.success){
        _.set(rt,createDefaultField.fieldName,result["result"]);
      }else{
        logger.debug("error message:",result["message"]);
        // _.set(rt,createDefaultField,result["message"]);
      }     
    });      
  };
  _.set(rt,"createDate",_.now());
  _.set(rt,"updateDate",_.now());
  }
  // logger.debug("rt:",rt);
  return rt;
}
function getUserCollectionRule(searchType){
  //need to be implement
  return userConfig.collectionRules[searchType];
}
function getUserCollectionRuleByModelName(modelName){
  //need to be implement
  return _.find(userConfig.collectionRules,_.set({},"model",modelName));
}
function getLoginInfo(){
   //need to be implement
  return {"fullName":"zeng.fanzhong","userCode":"088811","compCode":"01","compSubCode":"AG","agentCode":"000001"};
}
async function replaceFields(modelType,defindedField,parameter){
  const fieldName = defindedField["fieldName"];
  const fieldValue = defindedField["fieldValue"];
  const rule = defindedField["rule"];
  const seqRule = defindedField["seqRule"];
  const source = defindedField["source"];

  const internalInfo = getLoginInfo();

  if(fieldValue.indexOf("{")>=0 && fieldValue.indexOf("}")>=0){
       if(fieldValue=="{generate}"){
          finalFieldValue = rule;
          for(var logininfo in internalInfo){            
            if(finalFieldValue.indexOf("{"+logininfo+"}")>=0){
              finalFieldValue=finalFieldValue.replace("{"+logininfo+"}",internalInfo[logininfo]);
            }
          }
          if(finalFieldValue.indexOf("{seq}")>=0){
            const seqResult = await dbHelper.dbHelpFindOne(ModelSeqMst,{"_id":modelType});
            var seq = 1;
            if(!_.isEmpty(seqResult)){
              seq = seqResult["value"]+1;
            }else{
              var seqMst = new ModelSeqMst({
                 "_id":modelType,
                 "value":1
              });
              await dbHelper.dbHelpSave(seqMst);
            }
            let strSeq ="";
            await dbHelper.dbHelpUpdate(ModelSeqMst,{"_id":modelType},{value:seq});
            if(seqRule["paddingChar"] && seqRule["paddingCNT"]){
              for(let i=0;i<seqRule["paddingCNT"]-seq.toString().length;i++){
                strSeq = strSeq + seqRule["paddingChar"];
              }
            }
            strSeq = strSeq + seq;
            finalFieldValue = finalFieldValue.replace("{seq}",strSeq);
          }
          return {"success":true,result:finalFieldValue};
       }else{
        var finalFieldValue = fieldValue.replace("{","").replace("}","");
        if(source=="loginInfo"){
            if(internalInfo.hasOwnProperty(finalFieldValue)){
                return {"success":true,result:internalInfo[finalFieldValue]};
            }else{
                return {"success":false,message:fieldName+" : No "+finalFieldValue+" definded in Login information"};
            }
          
        }else if(source=="parameter"){
              if(parameter.data.hasOwnProperty(finalFieldValue)){
                return {"success":true,result:parameter.data[finalFieldValue]};
              }else{
                return {"success":false,message:fieldName+" : No "+finalFieldValue+" definded in input Parameter"};
              }

        }
      }
}else{
  return {"success":true,result:fieldValue};
}

};
const  fnSearch =(parameter,queryType) => new Promise((resolve,reject) =>{
  resolve(_fnSearch(parameter,queryType));
});
const fillValueFromObject = function(fieldNameList,fromObject,formatFields){
  // logger.debug("fromObject = ",fromObject);
  // logger.debug("formatFields = ",formatFields);
  var rtnObject = {};
  if(_.head(fieldNameList).indexOf("*")!=0){
    // logger.debug("fieldNameList=",fieldNameList);
    // logger.debug("fromObject=",fromObject["EAB"]);
    let dateFormat = systemConfig.dateFormat;
    fieldNameList.forEach(showField=>{
      let fieldValue = {};
      if(showField.indexOf(".*")>=0){
        showField = showField.replace(".*","");
        fieldValue = _.get(fromObject["_doc"],showField);   
      }
      else{
        fieldValue = _.get(fromObject,showField);
        if(showField.indexOf("{datetime}")>=0){
          showField = showField.replace("{datetime}","");
          fieldValue = _.get(fromObject,showField);
          fieldValue = moment(fieldValue).format(dateFormat);
        }else{
          if(_.isDate(fieldValue)){
            fieldValue = moment(fieldValue).format(dateFormat);
          }
        }        
      }
      rtnObject = _.set(rtnObject,showField,fieldValue);
    });
  }else{
    rtnObject = fromObject["_doc"];
    
  }
  // logger.debug("rtnObject 111 =",rtnObject);
  if(formatFields)
    rtnObject = formatField(formatFields,rtnObject);
  // logger.debug("rtnObject 222 =",rtnObject);
  return rtnObject;
}
const formatField = function(formatFieldsConfig,fromObject){
  var rtnObject = fromObject;
  // logger.debug("formatFieldsConfig 111 =",formatFieldsConfig);
  if(!_.isArray(formatFieldsConfig)){
    formatFieldsConfig = [formatFieldsConfig];
  }
  formatFieldsConfig.forEach(formatField=>{
    let fieldName = _.get(formatField,"fieldName");
    let formatType = _.get(formatField,"formatType");
    let as = _.get(formatField,"as");
    if(!as){
      as = fieldName;
    }
    let fieldValue = _.get(fromObject,fieldName);
    if(fieldValue){
      if(_.eq(formatType,"ccy")){
        fieldValue = utils.formatString(fieldValue,"ccy");
      }      
    }else{
      rtnObject = utils.handleArray(formatField["fieldName"],rtnObject,formatType,utils.formatString);
    }
    rtnObject = _.set(rtnObject,as,fieldValue);
  });
  // logger.debug("rtnObject = ",rtnObject)
  return rtnObject;
}

const prepareLookupDesc = async function(lookupDesc){
  let lookupDescs = {};
  // logger.debug("lookupDesc = ",lookupDesc);
  for(let i in lookupDesc){
    // logger.debug("lookupDesc = ",lookupDesc[i]);
    let lookupModelName = _.get(lookupDesc[i],"fromModel");
    // logger.debug("lookupModelName = ",lookupModelName);
    let lookupModel = require("../models/"+lookupModelName);
    let fromMatch = lookupDesc[i].fromMatch;
    let lookupCriteria = {};
    // logger.debug("lookupCriteria = ",lookupCriteria);
    if(fromMatch){
      lookupCriteria = _.set({},fromMatch.matchField,fromMatch.matchValue);
    }
    let loopResult = await dbHelper.searchAllResult(lookupModel,lookupCriteria);
    lookupDescs = _.set(lookupDescs,lookupDesc[i].localField,loopResult);   
  }
  return lookupDescs;
}
const getLookupDesc = function(paramStaffWithRelation,lookupDesc,lookupDescValue,language){
  let staffWithRelation = paramStaffWithRelation;
  // logger.debug("staffWithRelation = ",staffWithRelation);
  if(!language){
    language = "en";
  }
  for(let i in lookupDesc){
    let productDescs = [];
    let localField = lookupDesc[i].localField;
    let foreignPath = lookupDesc[i].foreignPath;
    let foreignField = lookupDesc[i].foreignField;
    let subForeignPath = lookupDesc[i].subForeignPath;
    // logger.debug("foreignPath = ",foreignPath);
    let lookupValues = _.head(_.get(lookupDescValue,localField));
    // logger.debug("lookupValues = ",lookupValues);
    lookupValues = _.get(lookupValues,foreignPath);
    // logger.debug("lookupValues = ",lookupValues);

    let isValueArray =true;
    if(subForeignPath){
      let localFieldValue =_.get(staffWithRelation,localField);
    if(!_.isArray(localFieldValue)){
      localFieldValue = [localFieldValue];
      isValueArray = false;
    }
    localFieldValue.forEach(item1 =>{
      let lookupValues2 ="";
    lookupValues.forEach(item=>{            
      let lookupValues1 = item[subForeignPath];                  
        let lookupValues3 = _.find(lookupValues1,_.set({},foreignField,item1))
        if(lookupValues3){
          lookupValues2 = lookupValues3[lookupDesc[i].lookup];
          lookupValues2 = _.get(lookupValues2,language);
        }          
      })
      if(!isValueArray){
        productDescs ={"itemValue":item1,"itemDesc":lookupValues2};
      }
      else{
        productDescs = _.concat(productDescs,{"itemValue":item1,"itemDesc":lookupValues2});
      }               
      });
      // logger.debug("productDescs = ",productDescs)
      staffWithRelation = _.set(staffWithRelation,localField,productDescs);
    }
    else{
      let localFieldValue =_.get(staffWithRelation,localField);
      
      if(!_.isArray(localFieldValue)){
        localFieldValue = [localFieldValue];
        isValueArray = false;
      }
      localFieldValue.forEach(item1=>{
        let lookupValues2 ="";
        let lookupValue = _.find(lookupValues,_.set({},foreignField,item1));
          lookupValue = _.get(lookupValue,lookupDesc[i].lookup);
          lookupValue = _.get(lookupValue,language);
          // logger.debug("lookupValues2 = ",lookupValues2);
          if(!isValueArray){
            productDescs ={"itemValue":item1,"itemDesc":lookupValue};
          }
          else{
            productDescs = _.concat(productDescs,{"itemValue":item1,"itemDesc":lookupValue});
          }
      })
      // lookupValue = _.get(lookupValue,lookupDesc[i].lookup);
      // lookupValue = _.get(lookupValue,language);
      // logger.debug("lookupValue = ",lookupValue);
      staffWithRelation = _.set(staffWithRelation,localField,productDescs);     
    }
  }
  //  logger.debug("staffWithRelation = ",staffWithRelation);
  return staffWithRelation;
}

async function _fnSearch(parameter,queryType){
  logger.debug("_fnSearch = ",parameter);
  var searchType = parameter.docType;
  var searchKey = parameter.searchKey;
  var showSelf = parameter.showSelf;
  var showRelation = parameter.showRelation;
  let queryParams = {};
  let queryAndOrParams= {};
  let orderBy = {};
  let finalResult = [];
  let resultIndex = 0;
  let rpResultSuccess = true;
  
  const searchRule = getUserCollectionRule(searchType);
  const indexFields = searchRule.indexFileds;
  const showFieldsList = _.get(_.get(searchRule,"showFieldsList"),_.get(parameter,"showFieldsList","default"));
  let lookupDesc = _.get(searchRule,"lookupDesc",[]);
  let sortBy = _.get(searchRule,"sortBy",[]);
  const exactMatchFields = searchRule.dependency;
  const orderByConfig = searchRule.orderBy;
  let intexItems = []; 
  let intexItemIndex = 0;
  
  if(indexFields && searchKey){   
    indexFields.forEach((indexField) => {
      let intexItem = {};
      intexItem[indexField] = {'$regex': searchKey, $options: '$i'};
      intexItems[intexItemIndex++]=intexItem;
    })
    queryAndOrParams={$or:intexItems};
  }
  let notInArray = _.get(parameter,"notInArray",[]);
  if(notInArray.length>0){
    queryAndOrParams = _.assign(queryAndOrParams,{"_id":{$nin:notInArray}})
  }
  if (exactMatchFields) {
    for(var exactMatchField of exactMatchFields){
      await replaceFields(searchType,exactMatchField,parameter).then(result =>{
        if(result["success"]){
          queryParams[exactMatchField["fieldName"]]=result["result"];
        }else{          
          finalResult[resultIndex++] = resultIndex+". "+result["message"];
          rpResultSuccess = false;
        }
      });
    }
  }
   if(rpResultSuccess){
      if(parameter.searchStr){
        let searchString =parameter.searchStr;
        if(queryType=="get"){
          searchString = JSON.parse(parameter.searchStr);
        }
        for(var str in searchString){
          queryParams[str] = searchString[str];
        }
      }
      if(orderByConfig){
        orderByConfig.forEach(order=>{
        orderBy[order["orderName"]]=order["order"];
        })
      }
      logger.info("orderBy =",orderBy);
      logger.info("queryParams =",queryParams);
      logger.info("queryAndOrParams =",queryAndOrParams);
      var Model = require('../models/'+searchRule.model);    
      var newStaffs = await dbHelper.searchQuery(Model,queryParams,queryAndOrParams,
        {},
        orderBy).catch(error=>{
          finalResult[resultIndex++]= ""+error;
        });
        _.remove(newStaffs,(newStatff)=>{return _.get(newStatff,"EAB.isDeleted")});
        if(_.get(newStaffs,"length",0)>0){
          let retrunStaffs = [];
          let lookupDescs = await prepareLookupDesc(lookupDesc);
          // logger.debug("lookupDescs = ",lookupDescs);
            for(let newStaff of newStaffs){
              if(showSelf){
                _.set(newStaff,"isSelf",true);
              }
              let staffWithRelation =fillValueFromObject(showFieldsList,newStaff,_.get(searchRule,"formatFields"));
              // logger.debug("showFieldsList = ",showFieldsList);          
              staffWithRelation = getLookupDesc(staffWithRelation,lookupDesc,lookupDescs,"en");
              if(showRelation){
                let relationSearchParameter = {};
                relationSearchParameter["docType"]=searchType;
                relationSearchParameter["key"]=""+newStaff._id;
                relationSearchParameter["lang"]=_.get(parameter,"lang","en");
                relationSearchParameter["relationType"]=_.get(parameter,"relationType","");
                let relation = await _fnRelationalSearch(relationSearchParameter,queryType);
                // logger.debug("relationSearchParameter = ",relationSearchParameter)
                if(_.get(relation,"success")){
                  let familyMember = relation["result"];
                  // logger.debug("familyMember result = ",relation);
                  if(showSelf){
                    familyMember.forEach(item=>{
                      staffWithRelation =_.concat(staffWithRelation,fillValueFromObject(showFieldsList,item,_.get(searchRule,"formatFields")));
                    });
                  }else{
                    staffWithRelation[_.get(parameter,"relationType","")] = familyMember;
                  }
                }
              }
              retrunStaffs = _.concat(retrunStaffs,staffWithRelation);
          //  logger.debug("retrunStaffs = ",retrunStaffs);

          }
          if(sortBy){
            let orderName = [];
            let sortType = [];
            sortBy.forEach(item=>{
              orderName = _.concat(orderName,item.sortName);
              sortType = _.concat(sortType,item.sortType);
            });
            retrunStaffs = _.orderBy(retrunStaffs,orderName,sortType);
          }

          finalResult= {"success":true,result:retrunStaffs};
          // logger.info("search count = ",newStaffs.length);
          // logger.info("finalResult = ",finalResult);
        }else{
          finalResult= "No record found!";
        }
    }
  return finalResult;
  };
//delete
async function fnDelete(deleteType,deleteCriteria,patameter){
  let queryParams = {};
  if(deleteCriteria){
    for(var criteria in deleteCriteria){     
        queryParams[criteria]=deleteCriteria[criteria];
    }
    const searchRule = getUserCollectionRule(deleteType);
    const isMarkDelete = _.get(searchRule,isMarkDelete,true);
    const deleteBefore = searchRule.deleteBefore;
    const deleteAfter = searchRule.deleteAfter;
    const exactMatchFields = searchRule.deleteDependency;
    let rpResultSuccess = true;
    let finalResult = [];
    let resultIndex = 0;
    if (exactMatchFields) {
        for(var exactMatchField of exactMatchFields){
        await replaceFields(deleteType,exactMatchField,patameter).then(result =>{
          if(result.success){
            queryParams[exactMatchField["fieldName"]]=result["result"];
          }else{
            finalResult[resultIndex++] = resultIndex+". "+result["message"];
            rpResultSuccess = false;
          }
        });
      };
    }
    if(rpResultSuccess && deleteBefore){
      const deleteFunctionPath = deleteBefore.path;
      const deleteFunction = deleteBefore.method;
      let caller = require(deleteFunctionPath);
      rpResultSuccess = await caller[deleteFunction](patameter);
    }
    logger.log("delete Criteria =",queryParams);
    logger.log("deleteBefore =",rpResultSuccess);
    if(rpResultSuccess){
        var Model = require('../models/'+searchRule.model);
        if(isMarkDelete){
          let updString ={"EAB.isDeleted":true};
          await dbHelper.dbHelpFindOneAndUpdate(Model,queryParams,updString).catch(error=>{
            finalResult[resultIndex++]= ""+error;
          });
        }else{
          await dbHelper.dbHelpRemove(Model,queryParams).then(result=>{
            // finalResult= {"success":true,result:result};
          }).catch(error=>{
            finalResult[resultIndex++]= ""+error;
          });
      }
    }
    if(rpResultSuccess && deleteAfter){
      const deleteAfterFunctionPath = deleteAfter.path;
      const deleteAfterFunction = deleteAfter.method;
      let caller = require(deleteAfterFunctionPath);
      rpResultSuccess = await caller[deleteAfterFunction](patameter);
    }
    if(rpResultSuccess){
      finalResult= {"success":true,result:1};
    }else{
      finalResult= "No record have been deleted";
    }
    return finalResult;
  }
};
//create
async function _fnCreate(createType,body){
    const collectionRule = getUserCollectionRule(createType);
    const Model = require('../models/'+collectionRule.model);
    const createMetaDatas = collectionRule.createMetaDatas;
    const model = new Model();
    var EAB = {};
    var finalResult = [];
    let rpResultSuccess = true;
    let resultIndex = 0;
    let subFiled ="";
    if(createMetaDatas){
    for(var createDefaultField of createMetaDatas){      
      await replaceFields(createType,createDefaultField,body).then(result=>{ 
         
        if(result.success){
           if(createDefaultField["fieldName"].indexOf(".")>=0){ 
              subFiled = createDefaultField["fieldName"].split(".")[0];
              EAB[createDefaultField["fieldName"].split(".")[1]]=result["result"];
           }
           else{
              model[createDefaultField["fieldName"]]=result["result"];
          }
        }else{
          finalResult[resultIndex++] = resultIndex+". "+result["message"];
          rpResultSuccess = false;
        }
       
      });      
    };
    }
    if(rpResultSuccess){
        model[subFiled]=EAB;
        model["CAFE"]=body.data;
        // delete model["CAFE"].responseID;
        // delete model["CAFE"].formID;
        let param = body;
        await dbHelper.dbHelpSave(model).then(result=>{
          finalResult= {"success":true,result:result};
          // logger.debug("result = ",result);
          param = _.set(param,"oppositID",_.get(result,"_id").toString());
        }).catch(error=>{
          finalResult[resultIndex++]= ""+error;
          rpResultSuccess = false;
        });
        if(rpResultSuccess){          
          if(_.has(body,"nextOfCreate")){
            let next=body.nextOfCreate;        
            let controller = require("./"+next.split(".")[0]);            
            await controller[next.split(".")[1]](param);
          }
        }
    }
    return finalResult;
};
const  fnCreate =(createType,body) => new Promise((resolve,reject) =>{
  resolve(_fnCreate(createType,body));
});

const  fnUpdate =(createType,updateCriteria,body) => new Promise((resolve,reject) =>{
  resolve(_fnUpdate(createType,updateCriteria,body));
});
//update
async function _fnUpdate(updateType,updateCriteria,patameter){
  let queryParams = {};
    const searchRule = getUserCollectionRule(updateType);
    const updateMetaDatas = searchRule.updateMetaDatas;
    const updateKeys = searchRule.updateKeys;
    let rpResultSuccess = true;
    let defaultUpdateKey = "responseID";
    let finalResult = [];
    let resultIndex = 0;
    let subFiled ="";
    let updateStr = {};
    if(!(updateKeys || patameter.data[defaultUpdateKey])){
        rpResultSuccess = false;
        finalResult[resultIndex++] = resultIndex+". No Update Key provided!";
    }
    if(rpResultSuccess){
        if(patameter.data[defaultUpdateKey]){
           queryParams[_.join(["EAB",defaultUpdateKey],".")]=patameter.data[defaultUpdateKey];
        }
        if(updateKeys){
          updateKeys.forEach(updateKey =>{
            if(patameter.data[updateKey]){
               queryParams[updateKey] = patameter.data[updateKey];
            }else{
              rpResultSuccess = false;
              finalResult[resultIndex++] = resultIndex+". No "+updateKey+" in parameter";
            }
          });
        }
        if(updateCriteria){
          for(var criteria in updateCriteria){       
              queryParams[criteria]=updateCriteria[criteria];
          }
        }    
        if(updateMetaDatas){
          for(var updateMetaData of updateMetaDatas){  
            await replaceFields(updateType,updateMetaData,patameter).then(result=>{          
              if(result.success){ 
                if(updateMetaData["fieldName"].indexOf(".")>=0){ 
                    subFiled = updateMetaData["fieldName"].split(".")[0];
                    let test={};
                    test[_.join([subFiled,updateMetaData["fieldName"].split(".")[1]],".")]=result["result"];
                    _.assign(updateStr,test);
                }
                else{
                    let test={};
                    test[updateMetaData["fieldName"]]=result["result"];  
                    _.assign(updateStr,test);
                    
                }
              }else{
                finalResult[resultIndex++] = resultIndex+". "+result["message"];
                rpResultSuccess = false;
              }
            
            });      
          };
          }
          _.assign(updateStr,{updateDate:_.now()});
          _.assign(updateStr,{"CAFE":patameter.data});
    }
    logger.error("in _fnUpdate queryParams = ",queryParams);
    if(rpResultSuccess){
        var Model = require('../models/'+searchRule.model);
            const newStaffs =await dbHelper.searchAllResult(Model,queryParams);
            // logger.error("in _fnUpdate  Model = ",Model);
              if(newStaffs.length===1){
                const newStaff = await dbHelper.dbHelpFindOneAndUpdate(Model,
                  queryParams,
                  updateStr
                  ).catch(error=>{
                    finalResult[resultIndex++] = resultIndex+". "+error;
                    rpResultSuccess = false;
                  });
                  if(rpResultSuccess){
                    if(_.has(patameter,"nextOfUpdate")){
                      let next=patameter.nextOfUpdate;
                      let controller = require("./"+next.split(".")[0]);
                      await controller[next.split(".")[1]](newStaff);
                    }
                  }
                if(newStaff){
                  finalResult= {"success":true,result:"Update Success"};
                }
              }else{
                finalResult[resultIndex++] = resultIndex+". No record have been updated!";
              }
      }
  return finalResult;
};

async function _fnUpdateSingle(updateType,updateKeys,updateString){
  let queryParams = {};
    const searchRule = getUserCollectionRule(updateType);
    const updateMetaDatas = searchRule.updateMetaDatas;
    let rpResultSuccess = true;
    let defaultUpdateKey = "responseID";
    let finalResult = [];
    let resultIndex = 0;
    if(!(updateKeys || patameter.data[defaultUpdateKey])){
        rpResultSuccess = false;
        finalResult[resultIndex++] = resultIndex+". No Update Key provided!";
    }
    if(rpResultSuccess){
        if(patameter.data[defaultUpdateKey]){
           queryParams[_.join(["EAB",defaultUpdateKey],".")]=patameter.data[defaultUpdateKey];
        }   
        if(updateMetaDatas){
          for(var updateMetaData of updateMetaDatas){  
            await replaceFields(updateType,updateMetaData,patameter).then(result=>{          
              if(result.success){
              }else{
                finalResult[resultIndex++] = resultIndex+". "+result["message"];
                rpResultSuccess = false;
              }
            
            });      
          };
          }
          _.assign(updateStr,{updateDate:_.now()});
    }
    logger.error("in _fnUpdate queryParams = ",queryParams);
    if(rpResultSuccess){
        var Model = require('../models/'+searchRule.model);
            const newStaffs =await dbHelper.searchAllResult(Model,queryParams);
            // logger.error("in _fnUpdate  Model = ",Model);
              if(newStaffs.length===1){
                const newStaff = await dbHelper.dbHelpFindOneAndUpdate(Model,
                  queryParams,
                  updateStr
                  ).catch(error=>{
                    finalResult[resultIndex++] = resultIndex+". "+error;
                    rpResultSuccess = false;
                  });
                  if(rpResultSuccess){
                    if(_.has(patameter,"nextOfUpdate")){
                      let next=patameter.nextOfUpdate;
                      let controller = require("./"+next.split(".")[0]);
                      await controller[next.split(".")[1]](newStaff);
                    }
                  }
                if(newStaff){
                  finalResult= {"success":true,result:"Update Success"};
                }
              }else{
                finalResult[resultIndex++] = resultIndex+". No record have been updated!";
              }
      }
  return finalResult;
};

const  fnRelationalSearch =(parameter,queryType) => new Promise((resolve,reject) =>{
  resolve(_fnRelationalSearch(parameter,queryType));
});
async function _fnRelationalSearch(parameter,queryType){
  var showSelf = parameter.showSelf;
  var searchType = parameter.docType;
  var grouping = parameter.grouping;
  var lang = _.lowerCase(_.get(parameter,"lang","en"));  
  // logger.info("lang = ",lang);
  let queryParams = {};
  let finalResult = [];
  let resultIndex = 0;
  let rpResultSuccess = true;  
  const searchRule = getUserCollectionRule(searchType);


  if(rpResultSuccess){
    let relationConfig = searchRule.relationMappingConfig;
    if(relationConfig){
      let relationType = _.get(parameter,"relationType");
      relationConfig = _.find(relationConfig,_.set({},"relationType",relationType))
      let lookupDesc = _.get(relationConfig,"lookupDesc",[]);
      let sortBy = _.get(relationConfig,"sortBy",[]);
      const relationRule = getUserCollectionRule(relationConfig.relationModel);
      const typeDefindedField = _.get(relationConfig,"typeDefindedField");
      const typeDefindedFieldValue = _.get(relationRule,"type");
      queryParams = {};
      let relateModel = require("../models/"+relationRule.model);
      queryParams[relationConfig.refKey]=parameter.key;
      queryParams[typeDefindedField]=typeDefindedFieldValue;
      logger.debug("match = ",queryParams);
      var lookups = relationRule.lookups;
      // logger.debug("lookups = ",lookups);
      let returnResult = [];
      let mappingKey = _.get(relationConfig,"mappingKey","");
      if(mappingKey.indexOf(".")>=0){
        mappingKey = mappingKey.split(".")[1];
      }
      if(lookups){
          var localField = lookups[0].localField;
          var subLocalField = lookups[0].subLocalField;
          var finalLocalField = localField;
          var foreignField = lookups[0].foreignField;
          if(subLocalField){
            finalLocalField = _.join([localField,subLocalField],".");
          }
          var as = lookups[0].as;
          var lookup = {

                from: lookups[0].from, // 需要关联的表
                localField: finalLocalField, // product 表需要关联的键
                foreignField: foreignField, // orders 的 matching key
                as: as // 对应的外键集合的数据
              
          };
          // logger.debug("lookup = ",lookup);
          logger.debug("queryParams = ",queryParams);
      const newStaffs = await dbHelper.dbHelpAggregate(relateModel,lookup,queryParams).catch(error=>{
          finalResult[resultIndex++]= ""+error;
          rpResultSuccess = false;
        });
        let displayFields = _.get(_.head(lookups),"returnFields",[]);
        // logger.debug("newStaffs = ",newStaffs);
        if(newStaffs.length>0){
          var relationships =_.get(_.head(newStaffs),localField,[]);
          var descriptions =_.get(_.head(newStaffs),as,[]);
          queryParams=[];
            relationships.forEach(relation=>{
              let temp = {};
              temp =_.assign(temp,displayFields.forEach(displayField=>{
                let lable = displayField;               
                if(lable.indexOf("{localField.}")>=0){
                  lable = lable.replace("{localField.}","");
                  temp[lable] =relation[lable];                 
                }else if(lable.indexOf("{as.}")>=0){
                  lable = lable.replace("{as.}","");
                  let role = _.get(relation,subLocalField);
                  let option = {};
                  if(foreignField.indexOf(".")>=0){
                   
                    let optionsKey = foreignField.split(".")[0];
                     let options = _.get(_.head(descriptions),optionsKey);
                     let foreignKey = foreignField.split(".")[1];
                     option = _.find(options,{[foreignKey]:role});
                  }else{
                     option = _.find(options,{[foreignField]:role});
                  }
                  temp[lable] =_.get(_.get(option,lable,""),lang,"");
                }
                 return temp;
              }));
              returnResult = _.concat(returnResult,temp);

              if(relationConfig.showOpposit){               
                 queryParams=_.concat(queryParams, _.get(relation,mappingKey));               
              }
            });
          }else{
            rpResultSuccess = false;
          }
      }else{

        const newStaffs =await dbHelper.searchAllResult(relateModel,queryParams);
        if(newStaffs.length>0){
          queryParams=[];
          var relationships =_.get(_.head(newStaffs),"relationship",[]);
            relationships.forEach(relation=>{
              returnResult = _.concat(returnResult,relation);
              if(relationConfig.showOpposit){               
                 queryParams=_.concat(queryParams, _.get(relation,mappingKey));
              }
            });
          }else{
            finalResult= {success:false,message:"No record found!"};
            rpResultSuccess = false;
          }

      }
      if(showSelf){
        queryParams=_.concat(queryParams, parameter.key);      
      }
      if(rpResultSuccess){
            let queryParamsTmp={};
            let finalRnResult = [];
            if(relationConfig.showOpposit){
              queryParamsTmp[_.get(relationConfig,"oppositKey")] = {$in:queryParams};
              // logger.debug("queryParamsTmp 1111=",queryParamsTmp);
              let modelOpposit = require("../models/"+relationConfig.oppositModel);
              // logger.info("modelOpposit = ",modelOpposit);
              const opposits =await dbHelper.searchAllResult(modelOpposit,queryParamsTmp).catch(error=>{});
              // if(_.get(parameter,"relationalSearchStr")){

              // }
              // logger.info("opposits = ",opposits);
              // logger.info("returnResult = ",returnResult);
              _.remove(opposits,(opposit)=>{return _.get(opposit,"EAB.isDeleted")});
              if(opposits.length>0){
                let lookupDescs = await prepareLookupDesc(lookupDesc);
                // logger.debug("lookupDescs = ",lookupDescs);
                let showOppositFields = _.get(relationConfig,"showOppositFields");
                opposits.forEach(opposit =>{ 
                  // logger.info("opposit = ",opposit);                          
                  let temp = fillValueFromObject(showOppositFields,opposit,_.get(relationConfig,"formatFields"));
                  let opkey = opposit[_.get(relationConfig,"oppositKey")];
                  opkey = _.toString(opkey);
                  let mpkey = _.get(relationConfig,"mappingKey");
                  if(mpkey.indexOf(".")>0){
                    mpkey = _.takeRight(mpkey.split("."));
                    mpkey = _.head(mpkey);
                  }
                  let oppositMappings = _.find(returnResult,_.set({},mpkey,opkey));
                  if(oppositMappings){
                     for(let oppositMapping in oppositMappings){
                       temp[oppositMapping] = oppositMappings[oppositMapping];
                     }
                  }else{
                    temp["self"] = true;
                  }
                  // item = _.assign(item,temp);
                  temp = getLookupDesc(temp,lookupDesc,lookupDescs,"en");
                  finalRnResult = _.concat(finalRnResult,temp);
              });  
              logger.debug("relation search count = ",opposits.length);
              }
              if(showSelf){

              }
            }
          if(sortBy){
            let orderName = [];
            let sortType = [];
            sortBy.forEach(item=>{
              orderName = _.concat(orderName,item.sortName);
              sortType = _.concat(sortType,item.sortType);
            });
            finalRnResult = _.orderBy(finalRnResult,orderName,sortType);
          }
          // logger.debug("finalRnResult = ",finalRnResult);
          if(grouping){
            finalRnResult = _.groupBy(finalRnResult,grouping.groupBy);
            finalRnResult = _(finalRnResult).map((items, iCid) => {
              return {
                groupBy:iCid,
                groupName: _.get(_.head(items),grouping.groupName),
                items: items
              }
            });
          }
          finalResult= {"success":true,result:finalRnResult};
        }
        // else{
        //   finalResult= "No record found!";
        //   rpResultSuccess = false;
        // }
  
    }
}
  return finalResult;
  };


  async function fnRelationCreate(createType,body){
    const systemCollectionRule = getUserCollectionRule(createType);
    var relationConfig = systemCollectionRule.relationMappingConfig;
    const relationType = _.get(body,"relationType");
    // logger.info("relationConfig",relationConfig);
    if(relationConfig){
      relationConfig = _.find(relationConfig,_.set({},"relationType",relationType))
    }
    const  mappingKey = _.get(relationConfig,"mappingKey");
    const  refKey = _.get(relationConfig,"refKey");
    
    const collectionRule = getUserCollectionRule(relationConfig.relationModel);
    const typeDefindedField = _.get(relationConfig,"typeDefindedField");
    const typeDefindedFieldValue = _.get(collectionRule,"type");
    // logger.info("collectionRule",collectionRule);
    const Model = require('../models/'+collectionRule.model);    
    const model = new Model();
    const actionType = _.get(body,"actionType","");
    const createMetaDatas = collectionRule.createMetaDatas;
    const createBefore = collectionRule.createBefore;
    const createAfter = collectionRule.createAfter;
    const deleteBefore = collectionRule.deleteBefore;
    var EAB = {};
    var finalResult = [];
    let rpResultSuccess = true;
    let resultIndex = 0;
    if(createMetaDatas){
    for(var createDefaultField of createMetaDatas){
      // logger.info("createDefaultField",createDefaultField);
      await replaceFields(createType,createDefaultField,body).then(result=>{
        // logger.info("createDefaultField result = ",result);
        if(result.success){
           if(createDefaultField["fieldName"].indexOf(".")>=0){
              EAB[createDefaultField["fieldName"].split(".")[1]]=result["result"];
            }
           else{
            model[createDefaultField["fieldName"]]=result["result"];
          }
        }else{
          finalResult[resultIndex++] = resultIndex+". "+result["message"];
          rpResultSuccess = false;
        }
      });
    };
    }
    let recordExists = false;
    if(rpResultSuccess){
      if(_.eq(actionType,"delete")){
        if(deleteBefore){
          const functionPath = deleteBefore.path;
          const vfunction = deleteBefore.method;
          let caller = require(functionPath);
          try{
            rpResultSuccess = await caller[vfunction](body);
            if(!rpResultSuccess)finalResult[resultIndex++]= vfunction+" return false";
          }catch(error){
            finalResult[resultIndex++]= error+"";
          }          
       }
      }else{
        if(createBefore){
          const functionPath = createBefore.path;
          const vfunction = createBefore.method;
          let caller = require(functionPath);
          try{
            rpResultSuccess = await caller[vfunction](body);
            if(!rpResultSuccess)finalResult[resultIndex++]= vfunction+" return false";
          }catch(error){
          finalResult[resultIndex++]= error+"";
        }
       }
    }
    }
    if(rpResultSuccess){
      let paramRelationShip = [body.data.relationShip];
      let currentID1 = body.data[refKey];
      logger.info("body.data[refKey] = ",currentID1);
      logger.info("refKey = ",refKey);
      let condition = _.set({},refKey,currentID1)//{"currentID":"5e27edc985de0900321920ea1"}
      condition[typeDefindedField]=typeDefindedFieldValue;//{"currentID":"5e27edc985de0900321920ea1","EAB.type":"familyMember"}
      // logger.debug("in fnRelationCreate condition = ",condition);
      const newStaffs = await dbHelper.searchAllResult(Model,condition,
        {});
        logger.debug("condition = ",condition)
      let relationShip =[];
      if(newStaffs.length > 0){
        relationShip=_.union(relationShip,newStaffs[0].relationship);
        recordExists = true;
      }
      logger.debug("in fnRelationCreate recordExists = ",recordExists);
      // logger.debug("relationShip in DB = ",relationShip); //[ { oppositID: '5e27ed8396ddf20032cf4b56', oppositRole: 'PGM' } ]
      let tmpMappingKey = mappingKey.indexOf(".")>=0?mappingKey.split(".")[1]:mappingKey;
      // logger.info("paramRelationShip = ",paramRelationShip[0][tmpMappingKey]);
         _.remove(relationShip,o=>{
        if (_.isEqual(_.get(o, tmpMappingKey), paramRelationShip[0][tmpMappingKey])) {
        return true;
      }})
      // logger.debug("relationShip after remove equals pass in relationship = ",relationShip)
      if(rpResultSuccess){
        if(actionType!=="delete"){
            relationShip = _.union(relationShip, paramRelationShip);
        }
        // logger.debug("relationShip after add back pass in relationship = ",relationShip);
            model["EAB"]=EAB;
            model["currentID"] = currentID1;
            model["relationship"] = relationShip;
            // logger.debug("model = ",model);   
            if(recordExists){
              // delete model["_id"];
              logger.debug("typeDefindedFieldValue = ",typeDefindedFieldValue);   
              await dbHelper.dbHelpUpdate(Model,{"currentID":currentID1,"EAB.type":typeDefindedFieldValue},{"relationship":relationShip}).then(result=>{
                finalResult= {"success":true,result:result};
              }).catch(error=>{
                finalResult[resultIndex++]= ""+error;
              });
            }else{
              if(actionType!=="delete"){
              await dbHelper.dbHelpSave(model).then(result=>{
                finalResult= {"success":true,result:result};
              }).catch(error=>{
                finalResult[resultIndex++]= ""+error;
              });
            }
          }
    }
    }
    logger.debug("in finalResult = ",finalResult);
    return finalResult;
};
exports.api = {
  //search
  search: function (req, res, next) {
    if (req.query.docType || req.body.docType) {
      let queryType = "";
      let parameter = {};
      if(req.query.docType){
         queryType ="get";
         parameter = req.query;
      }
      else{
          queryType ="post";
          parameter = req.body;
      }
      fnSearch(parameter,queryType).then(vResult =>{ 
         
        if(vResult["success"]){
          res.json({success:true, result: vResult["result"] });
        }else{
          res.json({ success: false, message: vResult});
        }        
      }).catch(error=>{
        res.json({ success: false, message: ""+error});
      });
  }else{
    res.json({ success: false,message: "required parameter:docType Missed"});
  }
  },
  //delete
  delete:function(req, res, next){
    if(req.body.docType && req.body.deleteCriteria){
    fnDelete(req.body.docType,req.body.deleteCriteria,req.body).then((result)=>{
      if(result["success"]){
        res.json({ success: true, result: result["result"] });
     }else{
        res.json({ success: false, message: result});
     }
    }).catch((error) => {
      res.json({ success: false,message:"" +error});
    });
  }else{
    res.json({ success: false,message: "required parameter:docType or deleteCriteria Missed"});
  }

},
  //create
  create: function(req, res, next){
    if(req.body.docType && req.body.data){
       fnCreate(req.body.docType,req.body).then((result)=>{
      if(result["success"]){
        res.json({ success: true, result: result["result"] });
     }else{
        res.json({ success: false, message: result});
     }
    }).catch((error) => {
      res.json({ success: false,message:"" +error});
    });
  }else{
    res.json({ success: false,message: "required parameter:docType or data Missed"});
  }
},
//update
update:function(req, res, next){
  if(req.body.docType && req.body.data){
    fnUpdate(req.body.docType,req.body.updateCriteria,req.body).then((result)=>{
    if(result["success"]){
      res.json({ success: true, result: result["result"] });
   }else{
      res.json({ success: false, message: result});
   }
  }).catch((error) => {
    res.json({ success: false,message:"" +error});
  });
}else{
  res.json({ success: false,message: "required parameter:docType or data Missed"});
}

},
//get document by docId
getDoc:async function(req, res, next){
  
  let docId = req.params.docId;
  let queryParams = {"_id":docId};
  // const query = new mongoose.Query();
  let keepGO = true;

  logger.info("docId = ",docId);
  for (let model in mongoose.models){
    logger.info("model = ",model);
    let Model = require('../models/'+model);
    if(keepGO){
      await dbHelper.dbHelpFindOne(Model,queryParams).then(result=>{
        
        if(result){
          keepGO = false;
          if(Model.modelName==='ModelClientProfile'){
            let parameter ={"relationType":"familyMember","docType":"clientProfile","searchStr":{"_id":docId},"showRelation":true,"showFieldsList":"default"};
            fnSearch(parameter,"post").then(vResult =>{
              logger.info("vResult = ",vResult);
              _.set(_.head(vResult.result),"EAB.clientDocId",docId);
              res.json ({"success":true,result:vResult["result"][0]});

            }).catch(error=>{
              res.json({"success":false,message:""+error});
            });


          }else{
            res.json ({"success":true,result:result});
          }
        }
        }).catch(error=>{
          res.json({"success":false,message:""+error});
        });
    }
  }
  if(keepGO){
    res.json({"success":false,message:"No record been found!"});
  }
},
userProfile:function(req, res, next){
  let userId = req.params.userId;
  let queryParams = {};

  if(userId){
      queryParams["searchStr"] = {"_id":userId};
      queryParams["docType"]="userProfile";
  }
  var searchResult = fnSearch(queryParams,"post");
  searchResult.then((result) => {
    if(!_.isEmpty(result))
      res.json({success: true, result: _.head(result["result"])});
    else
      res.json({ success: true, message: 'No Record Found!' });
})
  .catch((error) => {
    res.json({ success: false,message: ''+ error});
  });
},
 //relationalSearch
 relationalSearch: function (req, res, next) {
  // if (req.body.docType) {
  if (req.query.docType || req.body.docType) {
    var vResult;
    let key = "";
    if(req.query.docType){
      key = req.query.key;
       if(key){
          vResult = _fnRelationalSearch(req.query,"get");
       }
    }
    else{
       key = req.body.key;
       if(key){
        vResult = _fnRelationalSearch(req.body,"post");
       }
    }
    if(key){
    vResult.then((result) => {
      if(result["success"]){
        // logger.debug("result = ",result);
        res.json({ success: true, result: result["result"] });
      }else{
        res.json({ success: false, message: "Internal server error:"+result});
      }
        })
          .catch((error) => {
            res.json({ success: false,message: 'Internal server error:'+ error.message});
          });
    }else{
      res.json({ success: false,message: "required parameter:key Missed!"});
    }
}else{
  res.json({ success: false,message: "required parameter:docType Missed!"});
}
},
  //relationCreate
  relationCreate: function(req, res, next){
    if(req.body.docType,req.body.data){
      if(req.body.data.relationShip && req.body.data.currentID){
        fnRelationCreate(req.body.docType,req.body).then((result)=>{
        if(result["success"]){
          res.json({ success: true, result: result["result"] });
      }else{
          res.json({ success: false, message: result});
      }
      }).catch((error) => {
        res.json({ success: false,message:"" +error});
      });
    }else{
      res.json({ success: false,message: "required parameter:data.currentID or data.relationShip Missed!"});
    }
  }else{
    res.json({ success: false,message: "required parameter:docType or data Missed!"});
  }
},
//clientSearchExcludeFamilyMember
// clientSearchExcludeFamilyMember: function (req, res, next) {
//   if (req.query.docType || req.body.docType) {
//     let queryType = "";
//     let parameter = {};
//     if(req.query.docType){
//        queryType ="get";
//        parameter = req.query;
//     }
//     else{
//         queryType ="post";
//         parameter = req.body;
//     }
//     var notInArray = [];
//     _fnRelationalSearch(parameter,queryType).then(result =>{
        
//          if(_.get(result,"success",false)){
          
//           result["result"].forEach(opposit=>{
//             notInArray=_.concat(notInArray,opposit["oppositID"]);
//          })
//          }else{
//           logger.info("opposit",result);
//          }
//          notInArray=_.concat(notInArray,_.get(parameter,"key",""));
//          logger.info("notInArray",notInArray);         
//          parameter["notInArray"] = notInArray;
//          _fnSearch(parameter,queryType).then(vClients=>{
//            if(_.get(vClients,"success"))
//               res.json({success:true, result: vClients["result"] });
//             else
//               res.json({success:false, message: vClients });
//          })         
//       }).catch(error=>{
//         res.json({ success: false, message: ""}+error);
//       });
// }else{
//   res.json({ success: false,message: "required parameter:docType Missed"});
// }

// },
 //delete
 relationDelete:function(req, res, next){
  if(req.body.docType,req.body.data){
    if(req.body.data.relationShip && req.body.data.currentID){
      let parameter = _.set(req.body,"actionType","delete");
      logger.info("parameter = ",parameter);
      fnRelationCreate(req.body.docType,parameter).then((result)=>{
      if(result["success"]){
        res.json({ success: true, result: result["result"] });
    }else{
        res.json({ success: false, message: result});
    }
    }).catch((error) => {
      res.json({ success: false,message:"" +error});
    });
  }else{
    res.json({ success: false,message: "required parameter:data.currentID or data.relationShip Missed!"});
  }
}else{
  res.json({ success: false,message: "required parameter:docType or data Missed!"});
}

}
};
export {
  fnDelete,fnCreate,fnSearch,fnUpdate,fnRelationCreate,fillValueFromObject,getUserCollectionRuleByModelName,_fnUpdate
  ,_fnRelationalSearch,initCreateMetaData,getUserCollectionRule,getRelatedObject
};