const setting = {
    en:{
    quoteStages:"Stages",
    fnaRequirement:"FNA reqiurement",
    productType:"Product types",
    fnReqiuredMapping:[{"key":"true","value":"Compulsory"},{"key":"false","value":"Optional"}],
    insured:"Insured",
    eAppStatus:"EAPP Status",
    planNameAndQuoteNumber:"Plan name,quote number",
    keyWord:"Keywords",
    planNameAndEappNumber:"Plan name,eApp number",
    }
};
export {
    setting
  };