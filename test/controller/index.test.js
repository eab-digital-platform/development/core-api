import request from 'supertest';
import faker from 'faker';

import app from '../../app/index';

describe('Router', () => {

  it('[router][GET] / [cause] Success', (done) => {
    request(app)
      .get('/')
      .then((response) => {
        expect(response.status).toBe(200);
        expect(response.text)
          .toEqual('Hello World');
        done();
      });
  });

  it('[router][GET] /test [cause] Success', (done) => {
    request(app)
      .get('/test')
      .then((response) => {
        expect(response.status).toBe(200);
        expect(response.text)
          .toEqual('Hello World 2');
        done();
      });
  });

  it('[router][GET] /{random path} [cause] 404 error', (done) => {
    const randomPath = faker.random.words();
    request(app)
      .get(`/${randomPath}`)
      .then((response) => {
        expect(response.status).toBe(404);
        done();
      });
  });
})
